
cd pods
for entry in "$(pwd)"/*
do
  if [[ -d $entry ]]; then
  cd "$entry"
  for uflfile in *.ufl
  do
  	if [ -f "${uflfile}" ]; then
    echo "Running ffc on ${uflfile}"
  	ffc -l dolfin "$uflfile"
    fi
  done
  cd ../
  fi
done
cd ../

mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$HOME/local ..
make
make install
source $HOME/local/share/PODS/PODS.conf