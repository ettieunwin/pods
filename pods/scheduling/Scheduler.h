// Copyright (C) 2017 Hugo Hadfield
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-04-25
// Last changed: 2017-04-25

#ifndef __POD_Scheduler_H
#define __POD_Scheduler_H

#include <mpi.h>
#include <vector>
#include <functional>
#include "../pod/ParallelController.h"
#include "CostModel.h"

namespace pods
{
  // Where do these get used?  They are part of QOI?
  struct convergence_parameters
  {
    double alpha;
    double beta;
    double gamma;
  };
  
  class Scheduler
  /// What do I do Hugo?
  {
  public:
    Scheduler(MPI_Comm comm, std::size_t n_levels,
	      std::vector<SolverFunction> level_solvers);

    ~Scheduler()
    {
    };

    // What do I do?
    virtual std::vector<ParallelBatch> schedule(std::vector<int> level_repeats);

    // Are these all needed/ private?
    //? etc
    std::size_t n_levels;

    MPI_Comm comm;
    
    std::vector<SolverFunction> level_solvers;
    
    CostModel* p_cost_model;
    
    convergence_parameters convergence_params;
  };
  
}
#endif
