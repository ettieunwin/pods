// Copyright (C) 2017 Hugo Hadfield
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-04-25
// Last changed: 2017-04-25

#include <mpi.h>
#include "Scheduler.h"

using namespace pods;
//------------------------------------------------------------------------------
Scheduler::Scheduler(MPI_Comm comm, std::size_t n_levels,
		     std::vector<SolverFunction> level_solvers)
{
  this->n_levels = n_levels;
  this->comm = comm;
  this->level_solvers = level_solvers;
}
//------------------------------------------------------------------------------
std::vector<ParallelBatch> Scheduler::schedule(std::vector<int> level_repeats)
{
  std::vector<ParallelBatch> empty_vec;
  return empty_vec;
} 
//------------------------------------------------------------------------------

