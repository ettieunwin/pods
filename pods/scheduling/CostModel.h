// Copyright (C) 2017 Hugo Hadfield
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-03-29
// Last changed: 2017-03-29

#include <mpi.h>
#include <vector>
#include <math.h>
#include <functional>

#ifndef __POD_CostModel_H
#define __POD_CostModel_H

namespace pods
{
  struct  amdahl_parameters 
  {
    double p;
    double CF;
    double K;
    double gamma;
  };
 
  class CostModel
  {
  // What do I do?
  public:

    CostModel(amdahl_parameters cost_params,
	      std::vector<int> min_cores_at_level,
	      std::size_t total_cores);

    bool update_params(amdahl_parameters params);

    // One letter variable names
    static double amdahl_cost_function(std::size_t l,
				     std::size_t n ,
				     amdahl_parameters amdahl_params);
    
    double heuristic_problem_cost(std::vector<int> repeats_at_level,
				  std::vector<int> min_cores_at_level,
				  std::size_t total_cores,
				  std::size_t n_levels,
				  amdahl_parameters amdahl_params) const;

    double evaluate(std::vector<int> x) const;

    static bool target_variance_constraint(std::vector<int> repeat_list,
					   std::vector<double> variances,
					   double max_MSE);

    static bool min_repeat_constraint(std::vector<int> repeat_list,
				      std::vector<int> min_repeats);
    
    static std::vector<int> classic_MLMC(std::vector<double> variances,
					 amdahl_parameters amdahl_params,
					 double target_variance);


    
    // Are these public or private?
    amdahl_parameters cost_params;
    std::vector<int> min_cores_at_level;
    std::size_t total_cores;
    std::size_t n_levels;
  };
}
#endif
