// Copyright (C) 2017 Hugo Hadfield
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-04-25
// Last changed: 2017-04-25

#ifndef __POD_HeuristicScheduler_H
#define __POD_HeuristicScheduler_H

#include <mpi.h>
#include <vector>
#include "Scheduler.h"
#include "../pod/ParallelController.h"

namespace pods
{
  class HeuristicScheduler: public Scheduler
  /// What do I do?
  {
  public:
    HeuristicScheduler(MPI_Comm comm,
                       std::vector<SolverFunction> level_solvers,
                       std::vector<int> min_cores_per_level,
                       std::size_t total_cores) ;
    
    ~HeuristicScheduler()
    {
    };

    std::vector<ParallelBatch> schedule(std::vector<int> level_repeats);

    std::vector<int> generate_equal_split(std::size_t world_size,
					  std::size_t n_jobs);

    bool constraint_function(std::vector<double> x);

    // private?
    std::vector<int> min_cores_per_level;

    std::size_t total_cores;
  };
}
#endif
