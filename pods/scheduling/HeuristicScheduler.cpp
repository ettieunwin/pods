// Copyright (C) 2017 Hugo Hadfield
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-04-25
// Last changed: 2017-04-25

#include "HeuristicScheduler.h"
#include "../log/PodsLog.h"

using namespace pods;
//------------------------------------------------------------------------------
HeuristicScheduler::HeuristicScheduler(MPI_Comm comm,
				       std::vector<SolverFunction> level_solvers,
				       std::vector<int> min_cores_per_level,
				       std::size_t total_cores)
  : Scheduler(comm, level_solvers.size(), level_solvers)
{
	this->total_cores = total_cores;
	this->min_cores_per_level = min_cores_per_level;
}
//------------------------------------------------------------------------------
std::vector<int>
HeuristicScheduler::generate_equal_split(std::size_t world_size,
					 std::size_t n_jobs)
{
  if (world_size == n_jobs)
  {
    std::vector<int> output(n_jobs, 1);
    std::string msg = "batch output ";
    for (int i=0; i< n_jobs; ++i)
      msg+= std::to_string(output[i]) +  ", ";
    pods_log(0, msg); 
    return output;
  }
  else
  {
    if (world_size > n_jobs)
    {
      std::size_t cores_per_job = world_size / n_jobs;
      std::size_t remaining_cores = world_size - cores_per_job*n_jobs;

      // Set up the ones that fit
      std::vector<int> output(n_jobs, (int) cores_per_job);

      // Distribute the remaining cores
      for (std::size_t core_iterator = 0; core_iterator<remaining_cores;
           core_iterator++)
      {
        output[core_iterator] = output[core_iterator] + 1;
      }
      std::string msg = "batch output ";
      for (int i=0; i< n_jobs; ++i)
	msg+= std::to_string(output[i]) +  ", ";
      pods_log(0, msg); 
      
      return output;
    }
    else
    {
      //*******************MAKE THIS THROW A PODS ERROR WITH WHAT PROBLEM IS
      // Should throw a pods error
      throw 20;
    }
  }
}
//------------------------------------------------------------------------------
std::vector<ParallelBatch>
HeuristicScheduler::schedule(std::vector<int> level_repeats)
{
  std::vector<ParallelBatch> output_batch_list;
  for(int level_iterator = 0; level_iterator<level_repeats.size();
      level_iterator++)
  {
    pods_log(20, "scheduling level " + std::to_string(level_iterator));

    int n_cores = min_cores_per_level[level_iterator];
    if(total_cores < n_cores)
    {
      pods_error("HeuristicScheduler.cpp",
		 "schedule",
		 "total cores less than minimum cores");
    }
    std::size_t max_jobs_per_batch = total_cores/n_cores;

    // Get which specific solver we use for this one
    SolverFunction this_solver_function = level_solvers[level_iterator];

    pods_log(0, "got solver function " + std::to_string(level_iterator));

    // How many filled sync points we need
    int this_many_repeats = level_repeats[level_iterator];
    std::size_t n_batches = this_many_repeats/max_jobs_per_batch;
    std::size_t remaining_jobs
      = this_many_repeats - n_batches*max_jobs_per_batch;

    ///**********************MAKE COMMENT NOT SAY STUFF
    pods_log(0, "calulated some stuff " + std::to_string(level_iterator)
	    + " remainingJobs " +  std::to_string(remaining_jobs) +
	    " nbatches " + std::to_string(n_batches));

    // Put all the filled sync points in place
    for (int sync_point_iterator = 0; sync_point_iterator<n_batches;
	 sync_point_iterator++)
    {
      ParallelBatch my_batch = ParallelBatch();
      // Fill with the maximum number of jobs possible
      my_batch.core_allocation = generate_equal_split(total_cores,
						      max_jobs_per_batch);
      std::vector<SolverFunction>
          this_solver_list(max_jobs_per_batch, this_solver_function);
      my_batch.solver_allocation = this_solver_list;
      std::vector<int> this_level_list(max_jobs_per_batch,level_iterator);
      my_batch.level_allocation = this_level_list;
      output_batch_list.push_back(my_batch);
    }
    pods_log(0, "done all full batches" + std::to_string(level_iterator));

    // This is the remaining jobs
    pods_log(0, "remaining jobs: " + std::to_string(remaining_jobs) +
	    " total cores " + std::to_string(total_cores));
    if (remaining_jobs != 0)
    {
      ParallelBatch my_batch = ParallelBatch();
      my_batch.core_allocation = generate_equal_split(total_cores,
						      remaining_jobs);
      std::vector<SolverFunction> this_solver_list(remaining_jobs,
						   this_solver_function);
      my_batch.solver_allocation = this_solver_list;
      std::vector<int> this_level_list(remaining_jobs,level_iterator);
      my_batch.level_allocation = this_level_list;
      output_batch_list.push_back(my_batch);
    }
  }
  return output_batch_list;
}
//------------------------------------------------------------------------------
bool HeuristicScheduler::constraint_function(std::vector<double> x)
{
  for (int i = 0; i<x.size(); i++)
  {
    if (x[i] < min_cores_per_level[i])
    {
      return false;
    }
  }
  // Todo add in variance constraint based on beta
  return true;
}
//------------------------------------------------------------------------------
