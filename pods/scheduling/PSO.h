// Copyright (C) 2017 Hugo Hadfield
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-04-25
// Last changed: 2017-04-25

#ifndef PSO_H
#define PSO_H

#include <functional>
#include <vector>
#include <limits>
#include <random>

namespace pods
{
struct PSO_State{
    std::vector<double> swarmOptimumPosition;
    double swarmOptimumCost;
    std::vector<std::vector<double>> particlePosition;
    std::vector<std::vector<double>> particleVelocity;
    std::vector<std::vector<double>> particlePositionBest;
    std::vector<double> particleCostBest;
  };

struct PSO_Functions{
  std::function<double(std::vector<double>)> costFunc;
  std::function<bool(std::vector<double>)> constraintFunc;
  std::function<std::vector<double>(int)> initialPointGenerator;
  std::function<bool(PSO_State *, int)> terminationCriteria;
};

 struct PSO_Params{
  int xDimension;
  int NParticles;
  int maxEvaluations;
  double omega;
  double PhiP;
  double PhiG;
  double PhiR;
  double initialSpeed;
};

enum BoundaryTechnique { REJECT, BACKTRACKING, BINARY_SEARCH };

struct PSO_Flags{
  BoundaryTechnique boundaryHandling = BACKTRACKING;
};
 

PSO_State initialiseState(PSO_Functions psoFuncs, PSO_Params psoParams);

std::vector<double> generateNewVelocity(PSO_State *pPsoState, PSO_Params *pPsoParams, int particleIndex);

PSO_State runPSO(PSO_Functions psoFuncs, PSO_Params psoParams, PSO_Flags flags);

}
  
#endif
