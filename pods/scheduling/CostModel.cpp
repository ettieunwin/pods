// Copyright (C) 2017 Hugo Hadfield
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-03-29
// Last changed: 2017-03-29

#include "CostModel.h"
#include <iostream>
#include <assert.h>
#include <math.h>
  
using namespace pods;
//-----------------------------------------------------------------------------
CostModel::CostModel(amdahl_parameters cost_params,
		     std::vector<int> min_cores_at_level,
		     std::size_t total_cores)
{
  this->cost_params = cost_params;
  this->min_cores_at_level = min_cores_at_level;
  this->n_levels = min_cores_at_level.size();
  this->total_cores = total_cores;
}
//-----------------------------------------------------------------------------
bool CostModel::update_params(amdahl_parameters cost_params)
{
  this->cost_params = cost_params;
  return true;
}
//-----------------------------------------------------------------------------
double CostModel::amdahl_cost_function(std::size_t l, std::size_t n,
				       amdahl_parameters amdahl_params)
{
  double output;
  if (l == 0)
  {
    output = (amdahl_params.p/n + 1.0 - amdahl_params.p)*(amdahl_params.CF + amdahl_params.K*pow(2.0,(l*amdahl_params.gamma)));
  }
  else
  {
    output = (amdahl_params.p/n + 1.0 - amdahl_params.p)*( 2*amdahl_params.CF + amdahl_params.K*pow(2.0,((l-1.0)*amdahl_params.gamma))  + amdahl_params.K*pow(2.0,(l*amdahl_params.gamma))); 
  }
  //std::cout << "calculated cost func " << l << "  "<< n << "  "<< output <<std::endl;
  return output;
}
//-----------------------------------------------------------------------------
double CostModel::heuristic_problem_cost(std::vector<int> repeats_at_level,
					 std::vector<int> min_cores_at_level,
					 std::size_t total_cores,
					 std::size_t n_levels,
					 amdahl_parameters amdahl_params) const
{
  double total_cost = 0;
  for (std::size_t i=0; i<n_levels; ++i)
  {
   // Some helpful calcs and definitions
    std::size_t max_jobs_per_batch_cores = total_cores/min_cores_at_level[i];
    std::size_t max_jobs_per_batch = max_jobs_per_batch_cores;

    // How many filled sync points we need
    std::size_t n_batches = repeats_at_level[i]/max_jobs_per_batch;
    std::size_t remaining_jobs = repeats_at_level[i]
      - n_batches*max_jobs_per_batch;

    // Now the cost calc
    total_cost = total_cost + n_batches*amdahl_cost_function(i,total_cores/max_jobs_per_batch, amdahl_params);
    if (remaining_jobs > 0)
    {
      total_cost = total_cost
	+ amdahl_cost_function(i, total_cores/remaining_jobs, amdahl_params);
    }
  }
  return total_cost;
}
//-----------------------------------------------------------------------------
double CostModel::evaluate(std::vector<int> x) const
{
  return heuristic_problem_cost(x, min_cores_at_level, total_cores,
				n_levels, cost_params);
}
//-----------------------------------------------------------------------------
bool CostModel::target_variance_constraint(std::vector<int> repeat_list,
					   std::vector<double> variances,
					   double max_MSE )
{
  double total_sum = 0.0;
  for (int i =0; i <repeat_list.size(); i++)
  {
    if (repeat_list[i] == 0)
    {
      std::cout << "rejecting due to zero member" << std::endl;
      return false;
    }
    total_sum += variances[i]/((double) repeat_list[i]);
  }
  if (total_sum > 0.5*max_MSE)
  {
    return false;
  }
  return true;
}
//-----------------------------------------------------------------------------
bool CostModel::min_repeat_constraint(std::vector<int> repeat_list,
				      std::vector<int> min_repeats)
{
  for (int i = 0; i < repeat_list.size(); i++)
  {
    if (repeat_list[i] < min_repeats[i])
      return false;
  }
  return true;
}
//-----------------------------------------------------------------------------
std::vector<int> CostModel::classic_MLMC(std::vector<double> variances,
					 amdahl_parameters amdahl_params,
					 double target_variance )
{
  double mu  = 0.000;
  std::vector<int> Nl(variances.size(),0);
  for (std::size_t i=0; i<variances.size(); ++i)
  {
    mu += (1.0/target_variance) *sqrt(variances[i]*CostModel::amdahl_cost_function(i, 1 , amdahl_params))  ;
  }
  for (std::size_t i =0; i <variances.size(); ++i)
  {
    Nl[i] = (int) ceil(mu * sqrt(variances[i] / CostModel::amdahl_cost_function(i, 1 , amdahl_params)));
  }
  return Nl;
}
//-----------------------------------------------------------------------------
