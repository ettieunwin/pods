// Copyright (C) 2017 Hugo Hadfield
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-04-25
// Last changed: 2017-04-25
#include <vector>
#include <random>
#include <cassert>
#include <limits>
#include <algorithm>
#include <iostream>
#include "PSO.h"

namespace pods
{


template <typename T>
std::vector<T> operator+(const std::vector<T>& a, const std::vector<T>& b)
{
    assert(a.size() == b.size());

    std::vector<T> result;
    result.reserve(a.size());

    std::transform(a.begin(), a.end(), b.begin(),
                   std::back_inserter(result), std::plus<T>());
    return result;
}

template <typename T>
std::vector<T> operator-(const std::vector<T>& a, const std::vector<T>& b)
{
    assert(a.size() == b.size());

    std::vector<T> result;
    result.reserve(a.size());

    std::transform(a.begin(), a.end(), b.begin(),
                   std::back_inserter(result), std::minus<T>());
    return result;
}

template <typename T>
std::vector<T> operator/(const std::vector<T>& a, const std::vector<T>& b)
{
    assert(a.size() == b.size());

    std::vector<T> result;
    result.reserve(a.size());

    std::transform(a.begin(), a.end(), b.begin(),
                   std::back_inserter(result), std::divides<T>());
    return result;
}

template <typename T>
std::vector<T> operator*(const std::vector<T>& a, const std::vector<T>& b)
{
    assert(a.size() == b.size());

    std::vector<T> result;
    result.reserve(a.size());

    std::transform(a.begin(), a.end(), b.begin(),
                   std::back_inserter(result), std::multiplies<T>());
    return result;
}


PSO_State initialiseState(PSO_Functions psoFuncs, PSO_Params psoParams){

  PSO_State psoState{
    std::vector<double> (psoParams.xDimension,0.0),
     std::numeric_limits<double>::max(),
    std::vector<std::vector<double>> (psoParams.NParticles, std::vector<double> (psoParams.xDimension, 0.0)),
    std::vector<std::vector<double>> (psoParams.NParticles, std::vector<double> (psoParams.xDimension, 0.0)),
    std::vector<std::vector<double>> (psoParams.NParticles, std::vector<double> (psoParams.xDimension, 0.0)),
    std::vector<double> (psoParams.NParticles,  std::numeric_limits<double>::max())
  };

  std::default_random_engine generator;

  for (int particleIterator = 0; particleIterator < psoParams.NParticles; particleIterator++){

    std::cout << "Initialising particle position: " << particleIterator << std::endl;

    // Find somewhere valid to put each particle to begin with
    psoState.particlePosition[particleIterator] = psoFuncs.initialPointGenerator( psoParams.xDimension );
    std::cout << "finished getting valid points : " << particleIterator << std::endl;
    // Initialize the particle's best known position to its initial position
    psoState.particlePositionBest[particleIterator] = psoState.particlePosition[particleIterator];

     std::cout << "about to calculate cost : " << particleIterator << std::endl;

    for (auto i = psoState.particlePosition[particleIterator].begin(); i != psoState.particlePosition[particleIterator].end(); ++i)
        std::cout << *i << ' ';
      std::cout <<std::endl;

    psoState.particleCostBest[particleIterator] = psoFuncs.costFunc( psoState.particlePosition[particleIterator] );
    std::cout << "finished cost : " << particleIterator << std::endl;
    if  (psoState.particleCostBest[particleIterator] < psoState.swarmOptimumCost){
        // update the swarm's best known  position
        psoState.swarmOptimumPosition = psoState.particlePosition[particleIterator];
        psoState.swarmOptimumCost = psoFuncs.costFunc(psoState.swarmOptimumPosition);
    }
    std::cout << "Initialising particle velocity: " << particleIterator << std::endl;
    // Initialize the particle's velocity
    std::normal_distribution<double> distribution(0.0,psoParams.initialSpeed);
    for (int i =0; i < psoParams.xDimension; i++){
      psoState.particleVelocity[particleIterator][i] = distribution(generator);
    }
  }
  return psoState;
}


std::vector<double> generateNewVelocity(PSO_State *pPsoState, PSO_Params *pPsoParams, int particleIndex){

  std::default_random_engine generator;

  std::uniform_real_distribution<double> uniformDistribution(0.0,1.0);
  double rp = uniformDistribution(generator);
  double rg = uniformDistribution(generator);

  std::normal_distribution<double> gaussDistribution(0.0,pPsoParams->PhiR);  

  std::vector<double> totalNewVelocity(pPsoParams->xDimension,0.0);

  for (int i =0; i < pPsoParams->xDimension; i++){
      double randomnessInjection = gaussDistribution(generator);
      double particleSpecificVelocityAdjust = pPsoParams->PhiP * rp
                                                          * ( pPsoState->particlePositionBest[particleIndex][i]
                                                              -  pPsoState->particlePosition[particleIndex][i] );

      double globalVelocityAdjust = pPsoParams->PhiG * rg *( pPsoState->swarmOptimumPosition[i] 
                                                            - pPsoState->particlePosition[particleIndex][i] );

      totalNewVelocity[i] = pPsoParams->omega * pPsoState->particleVelocity[particleIndex][i] 
                                            + particleSpecificVelocityAdjust
                                            + globalVelocityAdjust
                                            + randomnessInjection;
    }
  return totalNewVelocity;
}


PSO_State runPSO(PSO_Functions psoFuncs, PSO_Params psoParams, PSO_Flags flags){

  std::cout << "Running PSO" << std::endl;

  PSO_State psoState = initialiseState(psoFuncs, psoParams);

  std::cout << "Finished initialising state" << std::endl;

  int nEvaluations = 0;
  int sweep = 0;
  while (!psoFuncs.terminationCriteria(&psoState,nEvaluations)){
     for (int i = 0; i < psoParams.NParticles; i++){
           
           // Update the particle's velocity: 
           // vi,d ? ? vi,d + ?p rp (pi,d-xi,d) + ?g rg (gd-xi,d)
           std::vector<double> totalNewVelocity = generateNewVelocity(&psoState, &psoParams, i);

            for (int backtracks = 0; backtracks < 50; backtracks++){
                if (0.0 < psoFuncs.constraintFunc(psoState.particlePosition[i] + totalNewVelocity)){
                   
                   psoState.particleVelocity[i] = totalNewVelocity;

                   // Update the particle's position: xi ? xi + vi
                   psoState.particlePosition[i] = psoState.particlePosition[i] + totalNewVelocity;

                   // Check if we have got any personal bests...
                   nEvaluations = nEvaluations + 1;
                   double thisCost = psoFuncs.costFunc(psoState.particlePosition[i]);
                   if (thisCost < psoState.particleCostBest[i]){
                      // Update the particle's best known position: pi ? xi
                      psoState.particleCostBest[i] = thisCost;
                      psoState.particlePositionBest[i] = psoState.particlePosition[i];
                      if (thisCost < psoState.swarmOptimumCost){
                         // Update the swarm's best known position: g ? pi
                         psoState.swarmOptimumPosition = psoState.particlePosition[i];
                         psoState.swarmOptimumCost = thisCost;
                      }
                   }
                   break;
                 }
                else{
                  for (int j =0; j < totalNewVelocity.size(); j++){
                    totalNewVelocity[j] = totalNewVelocity[j]*0.8;
                  }
                }
          }
   }
    std::cout << "Evaluation sweep, " << sweep << " , " << psoState.swarmOptimumCost << std::endl;
    sweep = sweep + 1;
 }
   return psoState;
}


}
