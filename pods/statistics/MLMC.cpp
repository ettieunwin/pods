// Copyright (C) 2017 Ettie Unwin and Hugo Hadfield
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-03-29
// Last changed: 2017-04-10

#include "MLMC.h"
#include "../solvers/GenericSolver.h"
#include "../mesh/PodsMesh.h"
#include "../pod/ParallelController.h"
#include "../log/PodsLog.h"
#include "../uncertainty/RandomNumberGenerator.h"
#include "../scheduling/HeuristicScheduler.h"
#include "../pod/ParallelController.h"
#include <math.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <functional>
#include <numeric>
#include <random>
#include <boost/filesystem.hpp>
#include <dolfin.h>

template <typename T>
std::vector<T> operator+(const std::vector<T>& a, const std::vector<T>& b)
{
    assert(a.size() == b.size());

    std::vector<T> result;
    result.reserve(a.size());

    std::transform(a.begin(), a.end(), b.begin(),
                   std::back_inserter(result), std::plus<T>());
    return result;
}

template <typename T>
std::vector<T> operator-(const std::vector<T>& a, const std::vector<T>& b)
{
    assert(a.size() == b.size());

    std::vector<T> result;
    result.reserve(a.size());

    std::transform(a.begin(), a.end(), b.begin(),
                   std::back_inserter(result), std::minus<T>());
    return result;
}

template <typename T>
std::vector<T> operator/(const std::vector<T>& a, const std::vector<T>& b)
{
    assert(a.size() == b.size());

    std::vector<T> result;
    result.reserve(a.size());

    std::transform(a.begin(), a.end(), b.begin(),
                   std::back_inserter(result), std::divides<T>());
    return result;
}

template <typename T>
std::vector<T> operator*(const std::vector<T>& a, const std::vector<T>& b)
{
    assert(a.size() == b.size());

    std::vector<T> result;
    result.reserve(a.size());

    std::transform(a.begin(), a.end(), b.begin(),
                   std::back_inserter(result), std::multiplies<T>());
    return result;
}

using namespace pods;
//-----------------------------------------------------------------------------
MLMC::MLMC(std::shared_ptr<GenericSolver> solver_class, double tol,
	   int initial_samples, int num_levels, int max_levels)
  : _solver_class(solver_class), _tol(tol), _num_levels(num_levels),
    _max_levels(max_levels)
{

  // Parameter to increase number of samples required at each level to
  // help meet error tolerance.
  double theta = 1.0;
  _theta = theta;
  double theta_max = 10.0;
  _theta_max = theta_max;

  // Gets number of quantities of interest
  int num_qois = solver_class->get_num_qoi();
  _num_qois = num_qois;

  // Creates a vector for number of samples for each level
  std::vector<int> samples_per_level;
  for (int i=0; i<_num_levels; ++i)
    samples_per_level.push_back(initial_samples);
  _samples_per_level = samples_per_level;

  // Sets save path to none
  std::string save_path = "no_path";
  _save_path = save_path;

  // Sets top level communicator
  MPI_Comm top_level_communicator = MPI_COMM_WORLD;
  _top_level_communicator = top_level_communicator;
}
//-----------------------------------------------------------------------------
MLMC::MLMC(std::shared_ptr<GenericSolver> solver_class, std::string save_path,
           double tol, int initial_samples, int num_levels, int max_levels)
  : _solver_class(solver_class), _tol(tol), _num_levels(num_levels),
    _max_levels(max_levels), _save_path(save_path)
{
 // Parameter to increase number of samples required at each level to
  // help meet error tolerance.
  double theta = 1.0;
  _theta = theta;
  double theta_max = 10.0;
  _theta_max = theta_max;

  // Gets number of quantities of interest
  int num_qois = solver_class->get_num_qoi();
  _num_qois = num_qois;

  // Creates a vector for number of samples for each level
  std::vector<int> samples_per_level;
  for (int i=0; i<_num_levels; ++i)
    samples_per_level.push_back(initial_samples);
  _samples_per_level = samples_per_level;

  // Makes sure save path directory exists
  boost::filesystem::path dir(save_path);
  boost::filesystem::create_directory(dir);

  // Sets top level communicator
  MPI_Comm top_level_communicator = MPI_COMM_WORLD;
  _top_level_communicator = top_level_communicator;
}
//-----------------------------------------------------------------------------
std::vector<QOI> MLMC::routine(double initial_seed)
{
 //Check if MPI has been initialised
   int flag;
   MPI_Initialized(&flag);
   if (!flag){
      MPI_Init(NULL, NULL);
    }

  // Start timer
  double start_time;
  double end_time;
  double total_time;
  start_time = MPI_Wtime();

  // Finds total number of cores and current rank
  int total_cores;
  int rank;
  MPI_Comm_size(_top_level_communicator, &total_cores);
  MPI_Comm_rank(_top_level_communicator, &rank);

  // Intialises data holders
  double theta = _theta;
  std::vector<int> samples_to_do = _samples_per_level;
  std::vector<int> all_samples(_num_levels, 0);

  // Force initialisation to zero for QoI holder
  std::vector<QOI> QOI_holder;
  std::vector<double> zeros(_num_levels, 0.0);
  std::vector<int> zeros_i(_num_levels, 0);
  for(int i = 0; i<_num_qois; i++)
  {
    QOI specific_QOI;
    specific_QOI.num_samples = zeros_i;
    specific_QOI.samples_to_do = _samples_per_level;
    specific_QOI.sum_estimations = zeros;
    specific_QOI.sum_estimations_sq = zeros;
    specific_QOI.expectation_per_level = zeros;
    specific_QOI.variance_per_level = zeros;
    specific_QOI.cost_per_level = zeros;
    specific_QOI.total_cost = zeros;
    specific_QOI.expectation = 0.0;
    specific_QOI.variance = 0.0;
    QOI_holder.push_back(specific_QOI);
  }

  // Calculates the number of samples to be run on each level
  int sum_samples_to_do = std::accumulate(QOI_holder[0].samples_to_do.begin(),
				    QOI_holder[0].samples_to_do.end(), 0);
  int sum_all_samples = sum_samples_to_do;
  int num_levels = _num_levels;

  pods_log(20, "starting routine");

  while (sum_samples_to_do > 0)
  {
    _get_expectations(QOI_holder, initial_seed);
    num_levels = QOI_holder[0].sum_estimations.size();

    // Update all_N
    for (int q=0; q<_num_qois; ++q)
    {
      std::transform(QOI_holder[q].num_samples.begin(),
		     QOI_holder[q].num_samples.end(),
		     QOI_holder[q].samples_to_do.begin(),
		     QOI_holder[q].num_samples.begin(),
		     std::plus<double>());
    }

    // Calculate expectation, variance and cost for each level
    _calculate_level_expectation(QOI_holder);
    _calculate_level_variance(QOI_holder);
    _calculate_level_cost(QOI_holder);

    for (int q=0; q<<_num_qois; ++q)
    {
      pods_log(50, "E[QoI] " + std::to_string(q)  + ": ");
      for(int i=0; i<num_levels; ++i)
        pods_log(50, std::to_string(QOI_holder[q].expectation_per_level[i]));

      pods_log(50, "V[QoI] " + std::to_string(q) +  ": ");
      for(int i=0; i<num_levels; ++i)
	pods_log(50, std::to_string(QOI_holder[q].variance_per_level[i]));

      pods_log(50, "Cost[QoI] " + std::to_string(q) +  ": ");
      for(int i=0; i<num_levels; ++i)
        pods_log(50, std::to_string(QOI_holder[q].cost_per_level[i]));
    }

    // Calculate alpha, beta, gamma parameters
    _calculate_convergence_constants(QOI_holder);

    // Calculate number of optimum samples
    _calculate_num_extra_samples(theta, QOI_holder);

    // Calculates number of samples to decide if need to check for convergence
    sum_samples_to_do = std::accumulate(QOI_holder[0].samples_to_do.begin(),
					QOI_holder[0].samples_to_do.end(), 0);
    sum_all_samples = std::accumulate(QOI_holder[0].num_samples.begin(),
				      QOI_holder[0].num_samples.end(),
				      0);

    if (sum_samples_to_do<0.01*sum_all_samples)
    {
      // Check for convergence
      _check_convergence(theta, QOI_holder);

      // Resum number of samples to see if need to carry on.
      sum_samples_to_do = std::accumulate(QOI_holder[0].samples_to_do.begin(),
					  QOI_holder[0].samples_to_do.end(), 0);
    }
  }

  // Calculate expectation and variance to return
  _calculate_expectation(QOI_holder);
  _calculate_variance(QOI_holder);

  // Calculates time for routine and communicates maximum time across
  // processors
  end_time = MPI_Wtime();
  total_time = end_time-start_time;
  double new_total_time;
  MPI_Allreduce(&total_time, &new_total_time, 1, MPI_DOUBLE,
		MPI_MAX, _top_level_communicator);
  total_time = new_total_time;

  for (int q=0; q<QOI_holder.size(); ++q)
    QOI_holder[q].total_time = total_time;

  if (_save_path != "no_path")
    pods_save_mlmc(initial_seed, _solver_class, QOI_holder, _tol, _save_path);

  dolfin::SubSystemsManager::finalize();
  MPI_Finalize();

  return QOI_holder;
}
//-----------------------------------------------------------------------------
void MLMC::_get_expectations(std::vector<QOI>& QOI_holder,
			     double initial_seed)
{
  int num_levels = QOI_holder[0].samples_to_do.size();

  int total_cores;
  int rank;
  MPI_Comm_size(_top_level_communicator, &total_cores);
  MPI_Comm_rank(_top_level_communicator, &rank);

  // Finds minimum number of cores per level
  std::vector<int>
    min_cores_at_level = _solver_class->get_min_num_cores_per_level(num_levels);

  // Create a scheduler
  std::vector<pods::SolverFunction> level_solvers;
  for (int level_index = 0; level_index < num_levels; level_index++)
  {
    SolverFunction estimate_this_level = [=](MPI_Comm comm, double processor_seed)
    {
      return MLMC::_estimate_level(comm, _solver_class,
				   level_index,
				   processor_seed);
    };
    level_solvers.push_back(estimate_this_level);
  }
  HeuristicScheduler basicScheduler(_top_level_communicator, level_solvers,
				    min_cores_at_level, total_cores);
  pods_log(20, "built scheduler");

  // Generate a schedule
  std::vector<ParallelBatch>
    batch_vector = basicScheduler.schedule(QOI_holder[0].samples_to_do);
  pods_log(20, "finished scheduling");

  for (int q=0; q<_num_qois; ++q)
  {
    QOI_holder[q].Cb.clear();
    QOI_holder[q].cost.clear();
  }

  // Initialise time variables
  double start_time;
  double end_time;
  double time;

  // Information about the number of seeds needed for the problem
  int batch_size = batch_vector.size();
  int number_cores;
  MPI_Comm_size(_top_level_communicator, &number_cores);
  std::vector<std::vector<double>> all_seeds;
  std::vector<double> row_of_seeds(number_cores);

  // Generate a random number generator for seeds for the distributions
  std::mt19937 gen;
  std::vector<double> range_seed = {0.0, 100000.0};
  RandomNumberGenerator rng_seeds(initial_seed, range_seed);
  std::function<double()>
    seed_number_generator = rng_seeds.uniform_distribution(gen);
  for (int r=0; r<batch_size; r++)
    {
      for (int c=0; c<number_cores; c++)
	row_of_seeds[c] = seed_number_generator();
      all_seeds.push_back(row_of_seeds);
    }

  // Solves the problem
  for (int i = 0; i<batch_size; i++)
  {
    pods_log(50,  "solving batch " + std::to_string(i) + " of "
	    + std::to_string(batch_size));

    start_time = MPI_Wtime();

    // Gets quantities of interest and sends them to all cores.
    std::vector<QOI> output_QOIs =
      ParallelController::solve_and_reduce(_top_level_communicator,
					   batch_vector[i].core_allocation,
					   batch_vector[i].solver_allocation,
					   batch_vector[i].level_allocation,
					   level_solvers, all_seeds[i]);

    // Ends timer - time for batch is taken as maximum time for batch
    // across cores.
    end_time = MPI_Wtime();
    time = end_time-start_time;
    double tmp_time;
    MPI_Allreduce(&time, &tmp_time, 1, MPI_DOUBLE, MPI_MAX, _top_level_communicator);
    time = tmp_time;

    // Calculates sum of estimations and estimations squared.
    for (int q=0; q<_num_qois; q++)
    {
      QOI_holder[q].sum_estimations = QOI_holder[q].sum_estimations
	+ output_QOIs[q].sum_estimations;
      QOI_holder[q].sum_estimations_sq = QOI_holder[q].sum_estimations_sq
	+ output_QOIs[q].sum_estimations_sq;
      QOI_holder[q].Cb.push_back(time);
    }
  }

  pods_log(20, "finished solving");

  // Calculate the cost data based on contents of batch vector
  std::vector<cost_datum> cost_data
    = _generate_cost_data(QOI_holder, batch_vector);
  pods_log(20, "finished generating cost data");

  std::vector<double> level_costs = _sum_level_costs(cost_data, num_levels);
  pods_log(20, "finished summing level costs");

  // Repackage it all into the original stuff passed in
  for (int l=0; l<num_levels; ++l)
  {
    for (int q=0; q<_num_qois; ++q)
      QOI_holder[q].total_cost[l]
        = QOI_holder[q].total_cost[l] + level_costs[l];
  }
  return;
}
//-----------------------------------------------------------------------------
std::vector<double>
MLMC::_estimate_level(MPI_Comm parent_comm,
		      std::shared_ptr<GenericSolver> solver_class,
		      int level,
		      double process_seed)
{
  // Generate meshes
  PodsMesh pods_mesh;
  solver_class->get_meshes(parent_comm, pods_mesh, level);

  // Solve fine problem
  std::vector<double> E_f = solver_class->pods_solve(pods_mesh, process_seed,
						     true);
  pods_log(20, "solved the fine problem");

  // If L0, diff = E_f
  std::vector<double> diff_E;
  if (level == 0)
    diff_E = E_f;
  else
  {
    // Solve coarse problem
    std::vector<double> E_c = solver_class->pods_solve(pods_mesh, process_seed,
						       false);
    pods_log(20, "solved the coarse problem");

    // Calculate difference between fine and coarse problem
    std::transform(E_f.begin(),E_f.end(),E_c.begin(),
                   std::back_inserter(diff_E),std::minus<double>());
  }
  std::string msg = "calculated difference: ";
  for (int i=0; i<diff_E.size(); i++)
    msg += std::to_string(diff_E[i]) + " ";
  pods_log(0, msg);
  return diff_E;
}
//-----------------------------------------------------------------------------
void MLMC::_calculate_level_expectation(std::vector<QOI>& QOI_holder)
{
  int num_levels = QOI_holder[0].num_samples.size();
  for (int l=0; l<num_levels; ++l)
  {
    // Divides Q/N to calculate expectation
    for (int q=0; q<_num_qois; ++q)
      QOI_holder[q].expectation_per_level[l]
	= QOI_holder[q].sum_estimations[l]/QOI_holder[q].num_samples[l];
  }
  return;
}
//-----------------------------------------------------------------------------
void MLMC::_calculate_level_variance(std::vector<QOI>& QOI_holder)
{
  int num_levels = QOI_holder[0].sum_estimations.size();

  // Make temporary vector for Q**2
  std::vector<double> Q2(_num_qois);

  for (int l=0; l<num_levels; ++l)
  {
    for (int q=0; q<_num_qois; ++q)
      QOI_holder[q].variance_per_level[l]
	= QOI_holder[q].sum_estimations_sq[l]/QOI_holder[q].num_samples[l]
	- pow(QOI_holder[q].expectation_per_level[l], 2);
  }

  // Take into account spurious variance caused from few samples.
  // This only occurs if have added an extra level.
  double Vl_;
  if (_num_levels<num_levels)
    {
    for (int l=_num_levels-1; l<num_levels; ++l)
    {
      for (int q=0; q<_num_qois; ++q)
      {
	if (std::abs(QOI_holder[q].variance_per_level[l]) == 0)
        {
	  pods_log(100,
		  "To few samples on l - need to estimate varaince at level l");
	  Vl_
            = QOI_holder[q].variance_per_level[l-1]/pow(2, QOI_holder[q].beta);
	  QOI_holder[q].variance_per_level[l]
	    = std::max(QOI_holder[q].variance_per_level[l], Vl_);
	}
      }
    }
  }
  return;
}
//-----------------------------------------------------------------------------
void MLMC::_calculate_level_cost(std::vector<QOI>& QOI_holder)
{
  int num_levels = QOI_holder[0].num_samples.size();
  for (int q=0; q<_num_qois; ++q)
  {
    // Divides cost by number of samples to get a value for cost of level
    for (int l=0; l<num_levels; ++l)
      QOI_holder[q].cost_per_level[l]
	= QOI_holder[q].total_cost[l]/QOI_holder[q].num_samples[l];
  }
  return;
}
//-----------------------------------------------------------------------------
void MLMC::_calculate_expectation(std::vector<QOI>& QOI_holder)
{
  int num_levels = QOI_holder[0].expectation_per_level.size();
  for (int q=0; q<_num_qois; ++q)
  {
    QOI_holder[q].expectation = 0.0;
    // Sums expectation over levels
    for (int l=0; l<num_levels; ++l)
      QOI_holder[q].expectation
	= QOI_holder[q].expectation + QOI_holder[q].expectation_per_level[l];
  }
  return;
}
//-----------------------------------------------------------------------------
void MLMC::_calculate_variance(std::vector<QOI>& QOI_holder)
{
  int num_levels = QOI_holder[0].variance_per_level.size();
  for (int q=0; q<_num_qois; ++q)
  {
    QOI_holder[q].variance = 0.0;
    // Sums variance over levels
    for (int l=0; l<num_levels; ++l)
    {
      QOI_holder[q].variance
	= QOI_holder[q].variance + QOI_holder[q].variance_per_level[l];
    }
  }
  return;
}
//-----------------------------------------------------------------------------
void MLMC::_calculate_convergence_constants(std::vector<QOI>& QOI_holder)
{
  int num_levels = QOI_holder[0].expectation_per_level.size();
  // Don't consider level 0 as not calculated in same way
  int level = 0.4*num_levels;

  std::vector<double> levels(num_levels-level);
  std::iota(levels.begin(), levels.end(), level);

  std::vector<double> expectation_per_level_log2(num_levels-level);
  std::vector<double> variance_per_level_log2(num_levels-level);
  std::vector<double> cost_per_level_log2(num_levels-level);

  for (int q=0; q<_num_qois; ++q)
  {
    //Make vectors to calculate constants from
    for (int i=0; i<num_levels-level; ++i)
    {
      expectation_per_level_log2[i]
	= std::log2(std::abs(QOI_holder[q].expectation_per_level[level+i]));
      variance_per_level_log2[i]
	= std::log2(QOI_holder[q].variance_per_level[level+i]);
      if (q==0)
	cost_per_level_log2[i]
	  = std::log2(QOI_holder[q].cost_per_level[level+i]);
    }
    // Calculates alpha, beta and gamma.
    QOI_holder[q].alpha
      = -1*_linear_regression(levels, expectation_per_level_log2);
    QOI_holder[q].beta = -1*_linear_regression(levels, variance_per_level_log2);
    QOI_holder[q].gamma = _linear_regression(levels, cost_per_level_log2);
    pods_log(80, "alpha qoi "  + std::to_string(q) +  " : "
	    + std::to_string(QOI_holder[q].alpha));
    pods_log(80, "beta qoi "  + std::to_string(q) +  " : "
	    + std::to_string(QOI_holder[q].beta));
    pods_log(80, "gamma qoi "  + std::to_string(q) +  " : "
	    + std::to_string(QOI_holder[q].gamma));
  }
  return;
}
//-----------------------------------------------------------------------------
double MLMC::_linear_regression(std::vector<double>& x_data,
                                std::vector<double>& y_data)
{
  // Performs linear regression on x_data and y_data to return value
  // of slope.
  double xsum=0;
  double x2sum=0;
  double ysum=0;
  double xysum=0;
  double slope;
  double intercept;
  int n = x_data.size();

  for (int l=0; l<n; ++l)
  {
    xsum += x_data[l];
    x2sum += pow(x_data[l], 2);
    ysum += y_data[l];
    xysum += x_data[l]*y_data[l];
  }

  slope = (n*xysum-xsum*ysum)/(n*x2sum-xsum*xsum);
  return slope;
}
//-----------------------------------------------------------------------------
void MLMC::_calculate_num_extra_samples(double theta,
                                        std::vector<QOI>& QOI_holder)
{
  // Get number of levles - same for all levels
  int num_levels = QOI_holder[0].variance_per_level.size();

  std::vector<double> opt_cost_per_level(num_levels);
  double sum_variance_cost;
  std::vector<std::vector<double>>
    Ns_(num_levels,std::vector<double>(_num_qois));
  std::vector<int> samples_opt(num_levels);

  // Calculate a cost model based on gamma - same for all levels
  for (int l=0; l<num_levels; ++l)
    opt_cost_per_level[l] = pow(2, QOI_holder[0].gamma*l);

  // For each QOI...
  for (int q=0; q<_num_qois; ++q)
  {
    sum_variance_cost = 0;

    // Sum variance*cost for all
    for (int l=0; l<num_levels; ++l)
       sum_variance_cost
	+= std::sqrt(opt_cost_per_level[l]*QOI_holder[q].variance_per_level[l]);

    // Calculate optimum number of samples for each qoi at each level
    for (int l=0; l<num_levels; ++l)
      Ns_[l][q]
	= theta*std::sqrt(QOI_holder[q].variance_per_level[l]/opt_cost_per_level[l])*sum_variance_cost/pow(_tol,2);
  }


  // Choosing optimum number of samples considering all qois
  for (int l=0; l<num_levels; ++l)
  {
    samples_opt[l] = ceil(Ns_[l][0]);

    std::string message = "Samples: " + std::to_string(samples_opt[l]) + " ";
    for (int q=1; q<_num_qois; ++q)
    {
      message += std::to_string(ceil(Ns_[l][q])) + " ";
      if (samples_opt[l] < ceil(Ns_[l][q]))
	samples_opt[l] = ceil(Ns_[l][q]);
    }
    pods_log(50, message);
    pods_log(80, "Level " + std::to_string(l) + ": "
	    + std::to_string(QOI_holder[0].num_samples[l])
	    + " samples collected, "
	    + std::to_string( samples_opt[l]) + " samples are optimum");
    for (int q=0; q<_num_qois; ++q)
      QOI_holder[q].samples_to_do[l]
	= std::max(0, samples_opt[l] - QOI_holder[q].num_samples[l]);
  }
}
//-----------------------------------------------------------------------------
void MLMC::_check_convergence(double& theta,
				  std::vector<QOI>& QOI_holder)
{
  pods_log(100, "Checking convergence...");

  double expectation_level_plus1;
  int num_levels = QOI_holder[0].expectation_per_level.size();
  int l_idx = num_levels-1;

  // Temporary vectors for adding samples
  int tmp = 0;
  double tmp_d = 0.0;

  for (int q=0; q<_num_qois; ++q)
  {
    // Estimating value of Q[L+1] - Q[l]
    expectation_level_plus1
      = QOI_holder[q].expectation_per_level[l_idx]/pow(QOI_holder[q].alpha,2);

    pods_log(20, "Comparing " + std::to_string(std::abs(expectation_level_plus1*std::sqrt(2))) + " to " + std::to_string(_tol));

    if (std::abs(expectation_level_plus1*std::sqrt(2)) > _tol)
    {
      if (num_levels == _max_levels-1)
      {
        pods_log(100, "Failed to reach weak convergence.  L_max reached.");
	for (int q=0; q<_num_qois; ++q)
	  std::transform(QOI_holder[q].samples_to_do.begin(),
			 QOI_holder[q].samples_to_do.end(),
			 QOI_holder[q].samples_to_do.begin(),
			 std::bind1st(std::multiplies<double>(), 0.0));
	return;
      }

      pods_log(100, "****Introducing a new level");

      // Approximate Vl on L+1 and resize QOI for new level
      for (int q_=0; q_<_num_qois; ++q_)
      {
	QOI_holder[q_].variance_per_level.push_back(tmp_d);
	QOI_holder[q_].variance_per_level[num_levels]
	  = QOI_holder[q_].variance_per_level[l_idx]/pow(2, QOI_holder[q_].beta);
	QOI_holder[q_].sum_estimations.push_back(tmp_d);
	QOI_holder[q_].sum_estimations_sq.push_back(tmp_d);
	QOI_holder[q_].expectation_per_level.push_back(tmp_d);
	QOI_holder[q_].cost_per_level.push_back(tmp_d);
        QOI_holder[q_].total_cost.push_back(tmp_d);
	QOI_holder[q_].samples_to_do.push_back(tmp);
	QOI_holder[q_].num_samples.push_back(tmp);
      }

      _calculate_num_extra_samples(theta, QOI_holder);

      return;
    }

    // Calculate variance of estimator
    double variance_estimator = 0.0;
    for (int l=0; l<num_levels; ++l)
      variance_estimator = variance_estimator +
	QOI_holder[q].variance_per_level[l]/QOI_holder[q].num_samples[l];

    if (std::sqrt(2*variance_estimator) > _tol)
    {
      if (theta > _theta_max)
      {
        pods_log(100, "Failed to reach weak convergence.  Theata_max reached");
	for (int q=0; q<_num_qois; ++q)
	  std::transform(QOI_holder[q].samples_to_do.begin(),
			 QOI_holder[q].samples_to_do.end(),
			 QOI_holder[q].samples_to_do.begin(),
			 std::bind1st(std::multiplies<double>(), 0.0));
	return;
      }
      else
      {
	theta *= 2;
        pods_log(100, "****Increasing the value of theta to "
		+ std::to_string(theta));
	_calculate_num_extra_samples(theta, QOI_holder);
	return;
      }
    }
    else
    {
      pods_log(20, "ERROR: " + std::to_string(variance_estimator +
					      abs(expectation_level_plus1)));
      pods_log(100, "Solution has converged for a tolerance of "
	      + std::to_string(_tol) +  "!" );
      for (int q=0; q<_num_qois; ++q)
	std::transform(QOI_holder[q].samples_to_do.begin(),
		       QOI_holder[q].samples_to_do.end(),
		       QOI_holder[q].samples_to_do.begin(),
		       std::bind1st(std::multiplies<double>(), 0.0));
      return;
    }
  }
}
//-----------------------------------------------------------------------------
std::vector<cost_datum>
MLMC::_generate_cost_data(std::vector<QOI> QOI_holder,
                          std::vector<ParallelBatch> batch_vector)
{
  std::vector<cost_datum> cost_data;

  int n_batches = batch_vector.size();
  for(int i = 0; i <n_batches; i++){

    // We will only use homogenous batches
    std::vector<int> level_alloc = batch_vector[i].level_allocation;
    if (std::adjacent_find(level_alloc.begin(), level_alloc.end(),
                           std::not_equal_to<int>() ) == level_alloc.end())
    {
      // We assume that the job allocated minimum cores takes the longest
      int min_cores = *min_element(batch_vector[i].core_allocation.begin(),
                                   batch_vector[i].core_allocation.end());
      // Push back a cost datum for every job in the batch
      for (int j=0; j<batch_vector[i].core_allocation.size(); j++)
      {
	cost_datum this_datum
          = {level_alloc[0], min_cores, QOI_holder[0].Cb[i]};
        cost_data.push_back(this_datum);
      }
    }
  }
  return cost_data;
}
//-----------------------------------------------------------------------------
std::vector<double> MLMC::_sum_level_costs(std::vector<cost_datum> cost_data,
                                           int n_levels)
{
  int n_data = cost_data.size();
  std::vector<double> level_costs(n_levels, 0.0);
  for (int i = 0; i < n_data; i++)
    level_costs[cost_data[i].l] = level_costs[cost_data[i].l] + cost_data[i].c;
  return level_costs;
}
//-----------------------------------------------------------------------------
