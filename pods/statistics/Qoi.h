#ifndef __QOI_H
#define __QOI_H

#include <vector>

struct QOI
{
  double expectation;
  double variance;
  std::vector<int> num_samples;
  std::vector<int> samples_to_do;
  std::vector<double> all_estimations;
  double batch_estimations;
  std::vector<double> sum_estimations;
  std::vector<double> sum_estimations_sq;
  std::vector<double> variance_per_level;
  std::vector<double> expectation_per_level;
  double alpha;
  double beta;
  double gamma;
  double total_time;
  
  // Can remove cost model if want
  //  double C;
  std::vector<double> Cb;
  std::vector<double> cost_per_level;
  std::vector<double> cost;
  std::vector<double> total_cost;
} ;

#endif
