// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-03-29
// Last changed: 2017-03-29

#include "MC.h"
#include "../solvers/GenericSolver.h"
#include "../mesh/PodsMesh.h"
#include "../log/PodsLog.h"
#include "../uncertainty/RandomNumberGenerator.h"
#include "../pod/ParallelController.h"
#include "../scheduling/HeuristicScheduler.h"
#include "../scheduling/Scheduler.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <functional>
#include <numeric>
#include <random>
#include <boost/filesystem.hpp>
#include <mpi.h>

template <typename T>
std::vector<T> operator+(const std::vector<T>& a, const std::vector<T>& b)
{
    assert(a.size() == b.size());

    std::vector<T> result;
    result.reserve(a.size());

    std::transform(a.begin(), a.end(), b.begin(),
                   std::back_inserter(result), std::plus<T>());
    return result;
}

template <typename T>
std::vector<T> operator-(const std::vector<T>& a, const std::vector<T>& b)
{
    assert(a.size() == b.size());

    std::vector<T> result;
    result.reserve(a.size());

    std::transform(a.begin(), a.end(), b.begin(),
                   std::back_inserter(result), std::minus<T>());
    return result;
}

template <typename T>
std::vector<T> operator/(const std::vector<T>& a, const std::vector<T>& b)
{
    assert(a.size() == b.size());

    std::vector<T> result;
    result.reserve(a.size());

    std::transform(a.begin(), a.end(), b.begin(),
                   std::back_inserter(result), std::divides<T>());
    return result;
}

template <typename T>
std::vector<T> operator*(const std::vector<T>& a, const std::vector<T>& b)
{
    assert(a.size() == b.size());

    std::vector<T> result;
    result.reserve(a.size());

    std::transform(a.begin(), a.end(), b.begin(),
                   std::back_inserter(result), std::multiplies<T>());
    return result;
}

using namespace pods;
//-----------------------------------------------------------------------------
MC::MC(std::shared_ptr<GenericSolver> solver_class, double tol)
  : _solver_class(solver_class), _tol(tol)
{
  // Calculates number of samples to take
  int num_samples =  std::ceil(1/(tol*tol));
  _num_samples = num_samples;

  // Corrects level - indexes from 0 not 1
  int level = 0;
  _level = level;

  int num_qois = solver_class->get_num_qoi();
  _num_qois = num_qois;

  std::string save_path = "no_path";
  _save_path = save_path;

  MPI_Comm top_level_communicator = MPI_COMM_WORLD;
  _top_level_communicator = top_level_communicator;
}
//-----------------------------------------------------------------------------
MC::MC(std::shared_ptr<GenericSolver> solver_class,
       std::string save_path, double tol)
  : _solver_class(solver_class),  _tol(tol),
    _save_path(save_path)
{
  // Calculates number of samples to take
  int num_samples =  10;
  _num_samples = num_samples;

  // Sets level to 1 so know where data is stored
  int level = 0;
  _level = level;

  int num_qois = solver_class->get_num_qoi();
  _num_qois = num_qois;

  // Make sure save path directory exists
  boost::filesystem::path dir(save_path);
  boost::filesystem::create_directory(dir);

  MPI_Comm top_level_communicator = MPI_COMM_WORLD;
  _top_level_communicator = top_level_communicator;
}
//-----------------------------------------------------------------------------
std::vector<QOI> MC::routine(double initial_seed)
{
  // Check if MPI has been initialised
   int flag;
   MPI_Initialized(&flag);
   if (!flag)
   {
     MPI_Init(NULL, NULL);
   }

  // Start timer
  double start_time;
  double end_time;
  double total_time;
  start_time = MPI_Wtime();

  int total_cores;
  int rank;
  MPI_Comm_size(_top_level_communicator, &total_cores);
  MPI_Comm_rank(_top_level_communicator, &rank);

  // Force initialisation to zero for QoI holder
  std::vector<QOI> QOI_holder;
  std::vector<double> zeros(_level+1, 0);
  std::vector<int> zeros_i(_level+1, 0);
  std::vector<int> num_samples = zeros_i;
  num_samples[_level] = _num_samples;
  for(int i = 0; i<_num_qois; i++)
  {
    QOI specific_QOI;
    specific_QOI.samples_to_do = num_samples;
    specific_QOI.num_samples = zeros_i;
    specific_QOI.sum_estimations = zeros;
    specific_QOI.sum_estimations_sq = zeros;
    specific_QOI.all_estimations = {};
    specific_QOI.expectation = 0.0;
    specific_QOI.variance = 0.0;
    specific_QOI.total_time = 0.0;
    QOI_holder.push_back(specific_QOI);
  }

  pods_log(50, "starting routine ");

  int num_samples_to_do = _num_samples;
  while (num_samples_to_do > 0)
  {
    // Update number of samples
    for (int q=0; q<_num_qois; ++q)
      QOI_holder[q].num_samples[_level] += QOI_holder[q].samples_to_do[_level];

    // Get all expectations
    _get_expectations(QOI_holder, initial_seed);

    // Calulates expectation and variance
    _calculate_expectation(QOI_holder);
    _calculate_variance(QOI_holder);

    // Checks convergence.
    _check_convergence(QOI_holder);
    num_samples_to_do = QOI_holder[0].samples_to_do[_level];
  }

  // Calculates time for routine
  end_time = MPI_Wtime();
  total_time = end_time-start_time;
  MPI_Allreduce(&total_time, &total_time, 1, MPI_DOUBLE,
		MPI_MAX, _top_level_communicator);

  for (int q=0; q<QOI_holder.size(); ++q)
    QOI_holder[q].total_time = total_time;

  // Saves data for analysis
  if (_save_path != "no_path")
    pods_save_mc(initial_seed, _solver_class, QOI_holder, _tol, _save_path);

  dolfin::SubSystemsManager::finalize();
  MPI_Finalize();

  return QOI_holder;
}
//-----------------------------------------------------------------------------
void MC::_get_expectations(std::vector<QOI>& QOI_holder,
			   double initial_seed)
{
  int total_cores;
  MPI_Comm_size(_top_level_communicator, &total_cores);
  int num_levels = _level+1;

  // Finds minimum number of cores per level
  std::vector<int>
    min_cores_at_level = _solver_class->get_min_num_cores_per_level(num_levels);

  // Create a scheduler
  std::vector<pods::SolverFunction> level_solvers;
  for (int level_index = 0; level_index < num_levels; level_index++)
  {
    SolverFunction estimate_this_level = [=](MPI_Comm comm, double processor_seed)
      { return MC::_estimate_level(comm, _solver_class,
				   level_index, processor_seed);
      };
    level_solvers.push_back(estimate_this_level);
  }

  HeuristicScheduler basic_scheduler(_top_level_communicator, level_solvers,
                                     min_cores_at_level, total_cores);
  pods_log(20, "built scheduler");

   // Generate a schedule
  std::vector<ParallelBatch>
    batch_vector = basic_scheduler.schedule(QOI_holder[0].samples_to_do);
  pods_log(20, "finished scheduling");

  // Information about the number of seeds needed for the problem
  int batch_size = batch_vector.size();
  int number_cores;
  MPI_Comm_size(_top_level_communicator, &number_cores);
  std::vector<std::vector<double>> all_seeds;
  std::vector<double> row_of_seeds(number_cores);

  // Generate a random number generator for seeds for the distributions
  std::mt19937 gen;
  std::vector<double> range = {0, 100000};
  RandomNumberGenerator rng(initial_seed, range);
  std::function<double()> seed_number_generator = rng.uniform_distribution(gen);

  for (int r=0; r<batch_size; r++)
  {
    for (int c=0; c<number_cores; c++)
      row_of_seeds[c] = seed_number_generator();
    all_seeds.push_back(row_of_seeds);
  }

  // Solves the problem
  for (int i = 0; i<batch_size; i++)
  {
    pods_log(50, "solving batch " + std::to_string(i) +  " of "
	    + std::to_string (batch_size));

    // Pass in a batch Vector
    std::vector<QOI> output_QOIs =
      ParallelController::solve_and_reduce(_top_level_communicator,
					   batch_vector[i].core_allocation,
					   batch_vector[i].solver_allocation,
					   batch_vector[i].level_allocation,
					   level_solvers,
					   all_seeds[i]);

    pods_log(20, "finished solve and reduce");

    for (int q=0; q<_num_qois; q++)
    {
      QOI_holder[q].sum_estimations
        = QOI_holder[q].sum_estimations + output_QOIs[q].sum_estimations;
      QOI_holder[q].sum_estimations_sq
        = QOI_holder[q].sum_estimations_sq
        + output_QOIs[q].sum_estimations_sq;
      for (int i=0; i<(output_QOIs[q].all_estimations).size(); ++i)
        QOI_holder[q].all_estimations.push_back(output_QOIs[q].all_estimations[i]);
    }
  }
  pods_log(20, "finished getting estimations");

  return;
}
//------------------------------------------------------------------------------
std::vector<double>
MC::_estimate_level(MPI_Comm parent_comm,
		    std::shared_ptr<GenericSolver> solver_class,
		    int level,
		    double processor_seed)
{
  int n_procs;
  MPI_Comm_size(parent_comm, &n_procs);

  // Generate mesh
  PodsMesh pods_mesh;;
  solver_class->get_meshes(parent_comm, pods_mesh, level);
  pods_log(20, "generated mesh");

  // Solve the problem
  std::vector<double> E = solver_class->pods_solve(pods_mesh, processor_seed,
						   true);
  pods_log(20, "found an expectation");

  return E;
}
//------------------------------------------------------------------------------
void MC::_calculate_expectation(std::vector<QOI>& QOI_holder)
{
  // Divides Q/N to calculate expectation
  for (int q=0; q<_num_qois; ++q)
    QOI_holder[q].expectation
      = QOI_holder[q].sum_estimations[_level]/QOI_holder[q].num_samples[_level];
  return;
}
//-----------------------------------------------------------------------------
void MC::_calculate_variance(std::vector<QOI>& QOI_holder)
{
 for (int q=0; q<_num_qois; ++q)
 {
   QOI_holder[q].variance
    = QOI_holder[q].sum_estimations_sq[_level]/QOI_holder[q].num_samples[_level]
    - pow(QOI_holder[q].expectation, 2);
 }
}
//-----------------------------------------------------------------------------
void MC::_check_convergence(std::vector<QOI>& QOI_holder)
{
  pods_log(100, "Checking convergence...");

  double V_N;

  // Calculate variance of estimator
  for (int q=0; q<_num_qois; ++q)
  {
    V_N = QOI_holder[q].variance/QOI_holder[q].num_samples[_level];

    if (_tol < std::sqrt(2*V_N))
    {
      pods_log(100, "Solution has not converged for tolerance "
	      + std::to_string(_tol));

      int max_num_samples = 0;
      int num_samples_qois = 0;
      for (int qoi=0; qoi<_num_qois; ++qoi)
      {
	num_samples_qois = std::ceil(2*QOI_holder[qoi].variance/pow(_tol,2));
	if (num_samples_qois > max_num_samples)
	  max_num_samples = num_samples_qois;
      }
      for (int qoi=0; qoi<_num_qois; ++qoi)
	QOI_holder[qoi].samples_to_do[_level]
	  = max_num_samples - QOI_holder[qoi].num_samples[_level] ;

      return;
    }
  }
  pods_log(100, "Solution has converged for tolerance " + std::to_string( _tol)
	  + "!");
  for (int qoi=0; qoi<_num_qois; qoi++)
	QOI_holder[qoi].samples_to_do[_level] = 0;
  return;
}
//-----------------------------------------------------------------------------
