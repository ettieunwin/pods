// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-03-29
// Last changed: 2017-03-29

#ifndef MC_H
#define MC_H

#include "Qoi.h"
#include <memory>
#include <mpi.h>
#include <string>
#include <memory>
#include <vector>
#include <functional>

namespace pods
{
  class GenericSolver;

  class MC
  /// Uses the Monte Carlo (MC) method to calculate expectation
  /// and variance for quantity of interests (QOIs) to a specified tolerance.
  /// Requires user to write a solver in C++ for the QOIs that adhears to
  /// the GenericSolver class. Includes scheduling when run in parallel.
  {
  public:
    // Constructor which requires the solver class and level of problem to match
    // MLMC.
    MC(std::shared_ptr<GenericSolver> solver_class, double tol=1e-1);

    // Constructor which requires solver and level of problem to match MLMC
    // but will save data.
    MC(std::shared_ptr<GenericSolver> solver_class, std::string solver_path,
       double tol=1e-1);

    // Destructor
    ~MC()
    {
    }

    // Calculates expectation and variance for QOI.  Requires a seed.
    std::vector<QOI> routine(double initial_seed);

  private:
    // Get estimations
    void _get_expectations(std::vector<QOI>& QOIholder,
			   double initial_seed);

    // Estimate level
    static std::vector<double>
      _estimate_level(MPI_Comm parentComm,
		      std::shared_ptr<GenericSolver> solver_class,
		      int level,
		      double processor_seed);

    // Calculate expectation
    void _calculate_expectation(std::vector<QOI>& QOIholder);

    // Calculate variance
    void _calculate_variance(std::vector<QOI>& QOIholder);

    // Checks convergence
    void _check_convergence(std::vector<QOI>& QOIholder);

    // Solver class
    std::shared_ptr<GenericSolver> _solver_class;

    // Tolerance
    double _tol;

    // Variance
    double _variance;

    // Level to match finest level of MLMC
    int _level;

    // Number of samples
    int _num_samples;

    // Number of quanitites of interest
    int _num_qois;

    // Save path
    std::string _save_path;

    // MPI top level communicator
    MPI_Comm _top_level_communicator;
  };
}
#endif
