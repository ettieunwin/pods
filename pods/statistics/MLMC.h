// Copyright (C) 2017 Ettie Unwin and Hugo Hadfield
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-03-29
// Last changed: 2017-03-29

#ifndef MLMC_H
#define MLMC_H

#include "Qoi.h"
#include "../pod/ParallelController.h"
#include <memory>
#include <mpi.h>

namespace pods
{
  struct cost_datum
  {
    // Cost data structure.
    int l;
    int n;
    double c;
  };

  class GenericSolver;

  class MLMC
  /// Uses the Multi-level Monte Carlo (MLMC) method to calculate expectation
  /// and variance for quantity of interests (QOIs) to a specified tolerance.
  /// Requires user to write a solver in C++ for the QOIs that adhears to
  /// the GenericSolver class. Includes scheduling when run in parallel.
  /// For more information about the method, see Mike Giles's paper
  /// https://people.maths.ox.ac.uk/gilesm/files/acta15.pdf.
  {
  public:

    // Constructor requiring solver to be specified.
    MLMC(std::shared_ptr<GenericSolver> solver_class, double tol = 1e-1,
         int initial_samples = 10, int num_levels = 4, int max_levels = 8);

    // Constructor requiring solver to be specified and saves data.
    MLMC(std::shared_ptr<GenericSolver> solver_class, std::string save_path,
         double tol = 1e-1, int initial_samples = 10, int num_levels = 4,
         int max_levels = 8);

    // Destructor
    ~MLMC()
    {
    }

    // Routine that generates the expectation and variance of QOI.
    // Requires an initial seed.
    std::vector<QOI> routine(double initial_seed);

  private:

    // Get all expectations
    void _get_expectations(std::vector<QOI>& QOI_holder,
			   double process_seed);

    // Estimate expectation at each levels.  Need to ensure have parent
    // communicator so correct scheduling occurs.
    static std::vector<double>
      _estimate_level(MPI_Comm parent_comm,
		      std::shared_ptr<pods::GenericSolver> _solver_class,
		      int level,
		      double process_seed);

    // Calculate expectation on each level
    void _calculate_level_expectation(std::vector<QOI>& QOI_holder);

    // Calculate variance on each level
    void _calculate_level_variance(std::vector<QOI>& QOI_holder);

    // Calculate cost of each level
    void _calculate_level_cost(std::vector<QOI>& QOI_holder);

    // Calculate expectation
    void _calculate_expectation(std::vector<QOI>& QOI_holder);

    // Calculate variance
    void _calculate_variance(std::vector<QOI>& QOI_holder);

    // Calculate convergence constants
    void _calculate_convergence_constants(std::vector<QOI>& QOI_holder);

    // Linear regression code
    double _linear_regression(std::vector<double>& x_data,
			      std::vector<double>& y_data);

    // Calculate number of extra samples needed per level
    void _calculate_num_extra_samples(double theta,
				      std::vector<QOI>& QOI_holder);

    // Check convergence
    void _check_convergence(double& theta,
			    std::vector<QOI>& QOI_holder);

    // Generate cost data
    std::vector<cost_datum>
      _generate_cost_data(std::vector<QOI> QOI_holder,
			  std::vector<pods::ParallelBatch> batch_vector);

    // Sum level costs
    std::vector<double> _sum_level_costs(std::vector<cost_datum> cost_data,
					 int n_levels);

    // Solver class
    std::shared_ptr<GenericSolver> _solver_class;

    // Tolerance
    double _tol;

    // Initial number of samples
    std::vector<int> _samples_per_level;

    // Initial number of levels
    int _num_levels;

    // Maximum number of levels
    int _max_levels;

    // Initial value of theta
    double _theta;

    // Maximum value of theta
    int _theta_max;

    // Number of quanitites of interest
    int _num_qois;

    // Save path
    std::string _save_path;

    // Top level communicator
    MPI_Comm _top_level_communicator;
  };
}
#endif
