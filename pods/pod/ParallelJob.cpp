// Copyright (C) 2017 Hugo Hadfield
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// The logger is based on the FEniCS libraries logger, see
// <https://fenicsproject.org>.
//
// First added:  2017-03-30
// Last changed: 2017-03-30

#include <mpi.h>
#include "../log/PodsLog.h"
#include "ParallelJob.h"

using namespace pods;
//-----------------------------------------------------------------------------
ParallelJob::ParallelJob(SolverFunction function_pointer,
			 int solver_level, MPI_Comm input_comm,
			 double processor_seed)
  : _processor_seed(processor_seed)
{
  split_comm = input_comm;
  job_solver_function = function_pointer;
  level = solver_level;
  
  int new_rank;
  MPI_Comm_rank(split_comm, &new_rank);
  is_broadcaster = new_rank == 0;
}
//-----------------------------------------------------------------------------
bool ParallelJob::get_is_broadcaster()
{
  return is_broadcaster;
}
//-----------------------------------------------------------------------------
std::vector<double> ParallelJob::solve()
{
  // This solves the job with the split communicator
  pods_log(20, "about to solve");
  std::vector<double> output = job_solver_function(split_comm, _processor_seed);
  pods_log(20, "solved");
  return output;
}
//-----------------------------------------------------------------------------
int ParallelJob::n_processors()
{
  // Returns how many processors on the assigned communicator are dedicated
  // to this job
  int n_procs = -1;
  MPI_Comm_size(split_comm, &n_procs);
  return n_procs;
}
//-----------------------------------------------------------------------------


