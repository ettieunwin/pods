// Copyright (C) 2017 Hugo Hadfield
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// The logger is based on the FEniCS libraries logger, see
// <https://fenicsproject.org>.
//
// First added:  2017-03-30
// Last changed: 2017-03-30

#ifndef __POD_PARALLEL_JOB_H
#define __POD_PARALLEL_JOB_H

#include <mpi.h>
#include <vector>
#include <functional>

namespace pods
{
  typedef std::function<std::vector<double>(MPI_Comm split_comm, double processor_seed)>
    SolverFunction;
	
  class ParallelJob
  /// Write me Hugo!! Add some documentation.
  {
    MPI_Comm split_comm;
    
    SolverFunction job_solver_function;
    
  public:
    // Constructor
    ParallelJob(SolverFunction function_pointer, int solver_level,
		MPI_Comm input_comm, double processor_seeds);

    // Destructor
    ~ParallelJob()
    {
    }

    // Solves the job with the constructor
    std::vector<double> solve();

    // Returns how many processors on the assigned communicator are dedicated
    //to this job
    int n_processors();

    // Returns if the processor is broadcaster
    bool get_is_broadcaster();

    //TODO:: what do I do?
    int level;

  private:
    // TODO: Why is the function private if it is called outside the function??
    bool is_broadcaster;

    double _processor_seed;
  };

}

#endif
