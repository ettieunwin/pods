// Copyright (C) 2017 Hugo Hadfield
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// The logger is based on the FEniCS libraries logger, see
// <https://fenicsproject.org>.
//
// First added:  2017-04-06
// Last changed: 2017-04-06

#ifndef __POD_PARALLEL_CONTROLLER_H
#define __POD_PARALLEL_CONTROLLER_H

#include "ParallelJob.h"
#include "../statistics/Qoi.h"
#include <vector>
#include <mpi.h>
#include <functional>
#include <iostream>

namespace pods
{
  typedef std::function<std::vector<double>(MPI_Comm split_comm, double processor_seed)>
    SolverFunction;

  struct ParallelBatch
  {
    std::vector<int> core_allocation;
    std::vector<SolverFunction> solver_allocation;
    std::vector<int> level_allocation;
  };

  class ParallelController
  /// Hugo what do I do?
  {
  public:
    // What do each of these do?  Are some of them only called within this
    // class?  If so can we make them private?
    // Put some kind of constant communicator names.  Should they be comm_world
    // instead of top level?

    //?
    static std::vector<QOI>
      solve_and_reduce(MPI_Comm top_level_vommunicator,
		       std::vector<int> core_allocation,
		       std::vector<SolverFunction> solver_allocation,
		       std::vector<int> level_allocation,
		       std::vector<SolverFunction> level_solvers,
		       std::vector<double> seeds);

  private:
    // Can we note these all as private member functions?
    static MPI_Comm generate_job_comm(MPI_Comm old_comm,
				      std::vector<int> color_List);  
    //?
    static std::vector<int>
      generate_color_list(std::vector<int> core_allocation);

    //?
    static MPI_Comm split_comm_to_cores(MPI_Comm parent_comm,
				     std::vector<int> core_allocation);
    //?
    static std::vector<SolverFunction>
      generate_solver_list(std::vector<int> core_allocation,
			   std::vector<SolverFunction> solver_allocation);
    //?
    static std::vector<int>
      generate_level_list(std::vector<int> core_allocation,
			  std::vector<int> level_allocation);
    //?
    static ParallelJob
      generate_parallel_job(MPI_Comm top_level_communicator,
			    MPI_Comm new_comm,
			    std::vector<SolverFunction> solver_list,
			    std::vector<int> level_list,
			    std::vector<double> seeds);
 

    //?
    static std::vector<QOI>
      generate_QOI_pay_load(std::vector<double> problem_output,
			    ParallelJob this_job,
			    std::size_t n_levels);

    //?
    static std::vector<QOI> reduce_all_QOIs(MPI_Comm reduction_comm,
					    MPI_Comm new_comm,
					    std::vector<QOI> local_contrib);
  }; 
}
#endif
