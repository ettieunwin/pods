// Copyright (C) 2017 Hugo Hadfield and Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// The logger is based on the FEniCS libraries logger, see
// <https://fenicsproject.org>.
//
// First added:  2017-04-06
// Last changed: 2017-04-06

#include "ParallelJob.h"
#include "ParallelController.h"
#include "../log/PodsLog.h"

using namespace pods;
// Document me!
//-----------------------------------------------------------------------------
MPI_Comm ParallelController::generate_job_comm(MPI_Comm old_comm,
					       std::vector<int> color_list)
{
  MPI_Comm new_comm;
  int old_rank;
  MPI_Comm_rank(old_comm, &old_rank);
  int color = color_list[old_rank];
  MPI_Comm_split(old_comm, color, old_rank, &new_comm);
  return new_comm;
}
//-----------------------------------------------------------------------------
std::vector<int>
ParallelController::generate_color_list(std::vector<int> core_allocation)
{
  std::vector<int> color_list;
  for (int i =0; i<core_allocation.size(); i++)
  {
    for (int j=0; j<core_allocation[i]; j++)
      color_list.push_back(i);
  }
  return color_list;
}
//-----------------------------------------------------------------------------
std::vector<SolverFunction>
ParallelController::generate_solver_list(std::vector<int> core_allocation,
					 std::vector<SolverFunction> solver_allocation)
{
  std::vector<SolverFunction> solver_list;
  for (int i =0; i<core_allocation.size(); i++)
  {
    for (int j=0; j<core_allocation[i];j++)
    {
      SolverFunction this_func_pointer = solver_allocation[i];
      solver_list.push_back(this_func_pointer);
    }
  }
  return solver_list;
}
//-----------------------------------------------------------------------------
std::vector<int>
ParallelController::generate_level_list(std::vector<int> core_allocation,
					std::vector<int> level_allocation)
{
  std::vector<int> level_list;
  for (int i =0; i<core_allocation.size(); i++)
  {
    for (int j=0; j<core_allocation[i];j++)
    {
      int this_level = level_allocation[i];
      level_list.push_back(this_level);
    }
  }
  return level_list;
}
//-----------------------------------------------------------------------------
ParallelJob
ParallelController::generate_parallel_job(MPI_Comm top_level_communicator,
					  MPI_Comm new_comm,
					  std::vector<SolverFunction> solver_list,
					  std::vector<int> level_list,
					  std::vector<double> seeds)
{
  // Gives world rank
  int world_rank;
  MPI_Comm_rank(top_level_communicator, &world_rank);

  // Finds group size of new communicator
  int new_comm_group_size;
  MPI_Comm_size(new_comm, &new_comm_group_size);

  // Broadcast seeds[0] from each group to all other processors within that group
  double processor_seed = seeds[world_rank];
  if (new_comm_group_size > 1)
  {
    MPI_Bcast(&processor_seed, 1, MPI_DOUBLE, 0, new_comm);
  }

  int local_rank;
  MPI_Comm_rank(new_comm, &local_rank);
  std::string msg = "Global processor rank: " + std::to_string(world_rank)
    + " , Local processor rank: " + std::to_string(local_rank) + ", seed: "
    + std::to_string(processor_seed);
  pods_log(20, "Seeding from within Parallel Controller");
  pods_log(20, msg);
  
  ParallelJob this_job(solver_list[world_rank],
		       level_list[world_rank], new_comm, processor_seed);
  return this_job;
}
//-----------------------------------------------------------------------------
MPI_Comm ParallelController::split_comm_to_cores(MPI_Comm parent_comm,
						 std::vector<int> core_allocation)
{
  // Turn core_allocation into color_list
  std::vector<int> color_list = generate_color_list(core_allocation);
  // Create a new MPI communicator!
  MPI_Comm new_comm = generate_job_comm(parent_comm, color_list);
  return new_comm;
}
//-----------------------------------------------------------------------------
std::vector<QOI>
ParallelController::generate_QOI_pay_load(std::vector<double> problem_output,
					  ParallelJob this_job,
					  std::size_t n_levels)
{
  std::size_t n_QOIs = problem_output.size();
  int level_index = this_job.level;
  std::vector<QOI> QOI_holder;
  for (std::size_t i=0; i<n_QOIs; ++i)
  {
    // Generate local vectors
    std::vector<double> local_sum_vector(n_levels, 0.0);
    std::vector<double> local_sum_sq_vector(n_levels, 0.0);
    double local_batch_estimations;// = 0.0;
    QOI temp_QOI;

    // Only broadcast a non zero value if it is rank zero on the new
    //communicator
    if (this_job.get_is_broadcaster())
    {
      local_sum_vector[level_index] = problem_output[i];
      local_sum_sq_vector[level_index] = problem_output[i]*problem_output[i];;
      local_batch_estimations = problem_output[i];
    }
    
    temp_QOI.sum_estimations = local_sum_vector;
    temp_QOI.sum_estimations_sq = local_sum_sq_vector;
    temp_QOI.batch_estimations = local_batch_estimations;

    QOI_holder.push_back(temp_QOI);
  }
  return QOI_holder;
}
//-----------------------------------------------------------------------------
std::vector<QOI> ParallelController::reduce_all_QOIs(MPI_Comm top_level_comm,
						     MPI_Comm local_comm,
						     std::vector<QOI> local_QOIs)
{
  // Calculates local number of QOIs
  std::size_t n_QOIs = local_QOIs.size();
  
  // Calculates number of levels
  std::size_t n_levels = local_QOIs[0].sum_estimations.size();

  int g_rank;
  int l_rank;
  int l_size;
  MPI_Comm_rank(top_level_comm, &g_rank);
  MPI_Comm_rank(local_comm, &l_rank);
  MPI_Comm_size(local_comm, &l_size);
  
  std::vector<QOI> QOI_holder;
  pods_log(20, "entering for loop over qois");
  for (std::size_t q=0; q<n_QOIs; ++q)
  {
    // Does MPI all reduce so each processor has a sum of estimations from that
    // batch.
    std::vector<double> global_sum_vector(n_levels, 0.0);
    MPI_Allreduce(&(local_QOIs[q]).sum_estimations.front(),
		  &global_sum_vector.front(), (int) n_levels, MPI_DOUBLE,
		  MPI_SUM, top_level_comm);
    pods_log(20, "all reduce over sum estimations");

    // Does MPI all reduce so each processor has a sum of estimations squared from
    // batch.
    std::vector<double> global_sum_sq_vector(n_levels, 0.0);
    MPI_Allreduce(&(local_QOIs[q]).sum_estimations_sq.front(),
		  &global_sum_sq_vector.front(), (int) n_levels, MPI_DOUBLE,
		  MPI_SUM, top_level_comm);
    pods_log(20, "all reduce over sum estimations sq");
      
    // Gather the results of all estimations from that batch on global processor
    // with rank 0. 
    int top_level_size;
    MPI_Comm_size(top_level_comm, &top_level_size);
    std::vector<double> global_batch_estimations((std::size_t) top_level_size);
    MPI_Gather(&(local_QOIs[q].batch_estimations), 1, MPI_DOUBLE,
   	       &global_batch_estimations.front(), 1, MPI_DOUBLE,
    	       0, top_level_comm);
    pods_log(20, "gather estimations on global rank 0 processor");
 
    // Sends to all the size of the MPI groups
    std::vector<int> rank_sizes(top_level_size);
    MPI_Allgather(&l_size, 1, MPI_INT, &rank_sizes.front(), 1, MPI_INT,
		      top_level_comm);
    
    // Ensure that data is split so remove the duplicate blank values when
    // running each job on more than one processor.
    int counter = 0;
    std::vector<double> tmp_values;
    for (int i=0; counter<top_level_size; ++i)
    {
      tmp_values.push_back(global_batch_estimations[counter]);
      counter += rank_sizes[counter];
    }
    std::string msg = "Non null global batch estimations: ";
    for (int j =0; j<tmp_values.size(); ++j)
      msg += std::to_string(tmp_values[j]) + " ";
    pods_log(0, msg);
    
    QOI temp_QOI;
    temp_QOI.all_estimations = {};
    temp_QOI.sum_estimations = global_sum_vector;
    temp_QOI.sum_estimations_sq = global_sum_sq_vector;    
    for (std::size_t j=0; j<tmp_values.size(); ++j)
      temp_QOI.all_estimations.push_back(tmp_values[j]);

    QOI_holder.push_back(temp_QOI);
  }
  pods_log(20, "create QOIholder");
  return QOI_holder;
}
//-----------------------------------------------------------------------------
std::vector<QOI>
ParallelController::solve_and_reduce(MPI_Comm top_level_communicator,
				     std::vector<int> core_allocation,
				     std::vector<SolverFunction> solver_allocation,
				     std::vector<int> level_allocation,
				     std::vector<SolverFunction> level_solvers,
				     std::vector<double> seeds)
{
  std::size_t n_levels = level_solvers.size();
  
  // Split communicator across cores
  MPI_Comm new_comm = split_comm_to_cores(top_level_communicator,
					  core_allocation);
  
  pods_log(20, "finished splitting comm", {new_comm});
  
  // Turn solver_allocation into solver_list
  std::vector<SolverFunction>
    solver_list = generate_solver_list(core_allocation, solver_allocation);
  
  pods_log(20, "finished generating solver list", {new_comm});
  
  // Turn levelAllocation into levelList
  std::vector<int> level_list = generate_level_list(core_allocation,
						    level_allocation);
  
  pods_log(20, "finished generating level list", {new_comm});
  
  // Create and solve problem
  ParallelJob this_job = generate_parallel_job(top_level_communicator,
					       new_comm, solver_list,
					       level_list, seeds);
  
  pods_log(20, "finished generating parallel job", {new_comm});
  
  std::vector<double> problem_output = this_job.solve();
    
  pods_log(20, "finished solving job", {new_comm});
  
  // Package and send answers
  std::vector<QOI>
    local_contrib = generate_QOI_pay_load(problem_output, this_job, n_levels);

  pods_log(20, "finished generating pay load", {new_comm});
  
  std::vector<QOI>
    global_merged_Qois = reduce_all_QOIs(top_level_communicator, new_comm,
					 local_contrib);
  
  pods_log(20, "finished sending answers", {new_comm});
  
  // Free this local communicator
  MPI_Comm_free(&new_comm);
  pods_log(20, "finished freeing communicator");
  
  // End the synchronisation point
  MPI_Barrier(top_level_communicator);
  pods_log(20, "finished barrier");
  
  return global_merged_Qois;
}
//-----------------------------------------------------------------------------
