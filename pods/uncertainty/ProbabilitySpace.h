// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-04-25
// Last changed: 2017-04-25

#ifndef PROBABILITYSPACE_H
#define PROBABILITYSPACE_H

#include <string>
#include <vector>
#include <functional>
#include <random>
#include <dolfin.h>

namespace pods
{
  class ProbabilitySpace
  {
  public:
    // Constructs a probability space
    ProbabilitySpace(std::string type, double seed);

    // Destructor
    ~ProbabilitySpace()
    {
    };

    // Generates a random number generator
    std::function<double()>
      generate_random_number_generator(std::mt19937& gen,
				       std::vector<double> stats);

  private:
    // Type of random number generator required
    std::string _type;

    // Seed
    double _seed;

    // Coarse mesh
    std::shared_ptr<const dolfin::Mesh> _coarse_mesh;
  };
}

#endif
