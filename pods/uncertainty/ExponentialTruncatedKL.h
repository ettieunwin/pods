// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-04-20
// Last changed: 2017-04-20

#ifndef EXPONENTIALTRUNCATEDKL
#define EXPONENTIALTRUNCATEDKL

#include <dolfin.h>
#include "FunctionHolder.h"
#include "StochasticFunction.h"
#include "TruncatedKL.h"

using namespace dolfin;
namespace pods
{
  class ExponentialTruncatedKL
    : public StochasticFunction<std::shared_ptr<dolfin::Function>>
  {
  public:
    // Constructor
    ExponentialTruncatedKL(std::shared_ptr<const dolfin::Mesh> mesh,
			   ProbabilitySpace& omega,
			   double mu, double sigma,
			   std::vector<double> lambda,
			   int truncation_number,
			   bool debug=false);

    // Destructor
    ~ExponentialTruncatedKL() {};

    // Samples the probability space to generate a random field realisation
    std::shared_ptr<dolfin::Function> sample();


  private:
   // Meshes
    std::shared_ptr<const dolfin::Mesh> _mesh, _mesh_full;

    // Probability space
    ProbabilitySpace& _omega;

    // Constants
    int _truncation_number;
    double _mu, _sigma;
    std::vector<double>  _lambda;

    // Debug switch
    bool _debug;

    // Communicator
    MPI_Comm _comm;

    std::shared_ptr<FunctionSpace> _V;
  };
}


#endif
