// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-03-29
// Last changed: 2017-03-29

#include "RandomNumberGenerator.h"
#include "../log/PodsLog.h"
#include <random>
#include <mpi.h>

using namespace pods;
//-----------------------------------------------------------------------------
std::function<double()>
RandomNumberGenerator::uniform_distribution(std::mt19937& gen)
{
  // Generates a different generator for each processor
  gen.seed(_processor_seed);

  // Creates a uniform number generator with _stats[0] as lowest
  // possible value and _stats[1] as highest possible value.
  std::uniform_real_distribution<> dis(_stats[0],_stats[1]);

  // Creates a thread safe random number generator.
  // Does not use reference to generator so if called multiple times
  // not in the same function will start again from the beginning with
  // initial seed.
  std::function<double()> rng = std::bind(dis, std::ref(gen));

  return rng;
}
//-----------------------------------------------------------------------------
std::function<double()>
RandomNumberGenerator::gaussian_distribution(std::mt19937& gen)
{
  // Generates a different generator for each processor
  gen.seed(_processor_seed);

  // Creates a normal distribution randon number generator with
  // _stats[0] as mean and _stats[1] as standard deviation.
  std::normal_distribution<> dis(_stats[0],_stats[1]);
  // Creates a thread safe random number generator.
  // Does not use reference to generator so if called multiple times
  // not in the same function will start again from the beginning with
  // initial seed.
  std::function<double()> rng = std::bind(dis, std::ref(gen));

  return rng;
}
//-----------------------------------------------------------------------------
