// Copyright (C) 2017 Nathan Sime and Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-07-27
// Last changed: 2017-07-27

#ifndef PODS_LPSMOOTHED_H
#define PODS_LPSMOOTHED_H

#include <dolfin.h>
#include "StochasticFunction.h"

namespace pods
{

  class MaternField :
    /// This class enables a matern random fields to be calculated by solving a
    /// Poisson equation with a random right hand side produced by a
    /// Gaussian distribution with mean mu and standard deviation sigma.
    public StochasticFunction<std::shared_ptr<dolfin::Function>>
  {
  public:
    // Constructor
    MaternField(std::shared_ptr<const dolfin::Mesh> mesh,
		ProbabilitySpace& omega,
		double mu, double sigma, double lambda)
      : _mesh(mesh), _omega(omega),
        _lambda(lambda), _mu(mu), _sigma(sigma)
    {
    }

    // Destructor
    ~MaternField(){};

    // Generates a random field
    virtual std::shared_ptr<dolfin::Function> sample();

  private:

    std::shared_ptr<const dolfin::Mesh> _mesh;

    ProbabilitySpace& _omega;

    double _lambda, _mu, _sigma;
  };

}


#endif
