// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-06-21
// Last changed: 2017-06-21

#include "TruncatedKL.h"
#include "../log/PodsLog.h"
#include <dolfin.h>
#include "kl1D.h"
#include "kl2D.h"
#include "kl3D.h"

using namespace pods;
using namespace dolfin;
//-----------------------------------------------------------------------------
class Kernel : public Expression
{
public:
  Kernel() {};

  Kernel(std::vector<double> lambda): _lambda(lambda) {};

  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = exp(-std::abs(x_i - x[0])/_lambda[0]);
  }

  double x_i;

private:
  std::vector<double> _lambda;
};
//-----------------------------------------------------------------------------
class Kernel2D : public Expression
{
public:
  Kernel2D() {};

  Kernel2D(std::vector<double> lambdas): _lambdas(lambdas) {};

  void eval(Array<double>& values, const Array<double>& x) const
  {
    double lambda_x = _lambdas[0];
    double lambda_y;
    if (_lambdas.size() == 2)
       lambda_y = _lambdas[1];
    else
       lambda_y = _lambdas[0];

    values[0] = exp(-(std::abs(point[0] - x[0])/lambda_x
		      + std::abs(point[1] - x[1])/lambda_y));
  }

  std::vector<double> point;

private:
  std::vector<double> _lambdas;
};
//-----------------------------------------------------------------------------
class Kernel3D : public Expression
{
public:
  Kernel3D() {};

  Kernel3D(std::vector<double> lambdas): _lambdas(lambdas) {};

  void eval(Array<double>& values, const Array<double>& x) const
  {
    double lambda_x, lambda_y, lambda_z;
    if (_lambdas.size() == 3)
    {
      lambda_x = _lambdas[0];
      lambda_y = _lambdas[1];
      lambda_z = _lambdas[2];
    }
    else
    {
      lambda_x = _lambdas[0];
      lambda_y = _lambdas[0];
      lambda_z = _lambdas[0];
    }
    values[0] = exp(-(std::abs(point[0] - x[0])/lambda_x
		      + std::abs(point[1] - x[1])/lambda_y
		      + std::abs(point[2] - x[2])/lambda_z));
  }

  std::vector<double> point;

private:
  std::vector<double> _lambdas;
};
//-----------------------------------------------------------------------------
TruncatedKL::TruncatedKL(std::shared_ptr<const dolfin::Mesh> mesh,
			 ProbabilitySpace& omega,
			 double mu, double sigma, std::vector<double> lambda,
			 int truncation_number, bool debug)
  : _omega(omega), _mu(mu), _sigma(sigma), _lambda(lambda),
    _truncation_number(truncation_number), _debug(debug)
{
  // Gets group communicator information
  MPI_Comm comm=mesh->mpi_comm();
  _comm = comm;
  int rank, size;
  MPI_Comm_rank(_comm, &rank);
  MPI_Comm_size(_comm, &size);
  _rank = rank;
  _size = size;

  // To avoid file writing/ reading conflicts
  int file_no;
  if (rank == 0)
    MPI_Comm_rank(MPI_COMM_WORLD, &file_no);
  MPI_Bcast(&file_no, 1, MPI_INT, 0, _comm);
  _file_no = file_no;

  // Gets dimensions of the problem
  int dim = mesh->geometry().dim();
  _dim = dim;

  // Creates function space for all the communicators together
  std::shared_ptr<FunctionSpace> V_all;
  if (_dim == 1)
    V_all = std::make_shared<kl1D::FunctionSpace>(mesh);
  else if (_dim == 2)
    V_all = std::make_shared<kl2D::FunctionSpace>(mesh);
  else if (_dim == 3)
    V_all = std::make_shared<kl3D::FunctionSpace>(mesh);

  // If size is equal to 1, mesh and function space are already only
  // on zeroth processor
  if (_size == 1)
  {
    // Assigns objects for just zeroth processor
    _V = V_all;
    _mesh = mesh;

     int mat_dim = mesh->num_vertices();
    _mat_dim = mat_dim;

  }
  else
  {
    pods_log(20, "Truncated KL: Finding random field on zeroth ranked processor");

    HDF5File tmp_mesh(_comm,
		      "tmp_mesh"+std::to_string(_file_no)+".h5", "w");
    tmp_mesh.write(*mesh, "mesh");
    tmp_mesh.close();

    if (_rank == 0)
    {

      pods_log(0, "Truncated KL: Reading mesh in on zeroth ranked processor");
      // All processors write to mesh - has to be to HDF5!!
      std::shared_ptr<Mesh> new_mesh = std::make_shared<Mesh>(MPI_COMM_SELF);
      HDF5File tmp_mesh_read(MPI_COMM_SELF,
			     "tmp_mesh"+std::to_string(_file_no)+".h5", "r");
      tmp_mesh_read.read(*new_mesh, "mesh", false);
      tmp_mesh_read.close();
      _mesh = new_mesh;

      int mat_dim = new_mesh->num_vertices();
      _mat_dim = mat_dim;

      if (_dim == 1)
	_V = std::make_shared<kl1D::FunctionSpace>(new_mesh);
      else if (_dim == 2)
	_V = std::make_shared<kl2D::FunctionSpace>(new_mesh);
      else if (_dim == 3)
	_V = std::make_shared<kl3D::FunctionSpace>(new_mesh);
    }
  }

  _V_all = V_all;

  // Barrier to make sure that the zeroth processor has finished
  MPI_Barrier(_comm);
}
//-----------------------------------------------------------------------------
std::shared_ptr<dolfin::Function> TruncatedKL::sample()
{
  // Generate random number generator
  pods_log(20, "Truncated KL: Resampling");
  Function random_field = Function(_V_all);

  if (_rank == 0)
  {
    MPI_Comm local_comm = MPI_COMM_SELF;

    // Compute assembled Covariance Kernel
    PETScMatrix C = _compute_basis(local_comm);
    pods_log(0, "Truncated KL: Computed basis");

    // Compute eigen pairs
    std::vector<KLEigenPair> eigenpairs = _compute_eigen_pairs(local_comm, C);
    pods_log(0, "Truncated KL: Computed eigenpairs");

    // Initialises functions
    Function eig_function = Function(_V);
    Function random_field_zero = Function(_V);
    random_field_zero = Constant(_mu);

    // Create random numbers generator using the probability space
    std::mt19937 gen;
    std::vector<double> stats = {0.0, 1.0};
    std::function<double()> random_number_generator
      = _omega.generate_random_number_generator(gen,stats);

    // Sample random numbers from the probability space
    std::vector<double> sample_numbers(eigenpairs.size());
    std::generate(sample_numbers.begin(),
		  sample_numbers.end(),
		  random_number_generator);
    pods_log(0, "Truncated KL: Generated random numbers");

    // Creates files
    XDMFFile random_field_file(local_comm, "random_field_numeric"
			       +std::to_string(_file_no)+".xdmf");
    XDMFFile eig_funcs_file(local_comm, "eigen_functions_numeric"
			    +std::to_string(_file_no)+".xdmf");

    std::string evs;
    evs += "Truncated KL: Eigenvalues: ";

    // Generate the realisation
    for (int k=0; k<eigenpairs.size(); ++k)
    {
      *eig_function.vector() = eigenpairs[k].eigenfunction;
      random_field_zero.vector()->axpy(pow(eigenpairs[k].eigenvalue, 0.5)
				       *sample_numbers[k],
				       *(eig_function).vector());

      // Saves ranom field and eigen functions if in debug mode
      if (_debug==true)
      {
      	random_field_file.write(random_field_zero, k);
      	eig_funcs_file.write(eig_function, k);
      }

      evs += std::to_string(eigenpairs[k].eigenvalue) + ", ";
    }
    pods_log(0, evs);

    // Checks properties of the eigenpairs if debug mode is chose
    if (_debug == true)
      _check_l2_norm(local_comm, eigenpairs);

    pods_log(0, "Truncated KL: Calculated truncated KL expansion");

    // If one one processor, random field is as calculated on zeroth processor
    if (_size == 1)
        random_field = random_field_zero;
    // Save the random field from one processor
    else
    {
      HDF5File h5(MPI_COMM_SELF, "tmp_rf"+std::to_string(_file_no)+".h5", "w");
      h5.write(random_field_zero, "rf");
      h5.close();
    }
  }

  // Holds all processors until zeroth one has calculated field
  MPI_Barrier(_comm);

  // If running using a group of processors, reads in random field
  if (_size >  1)
  {
    HDF5File file(MPI_COMM_WORLD, "tmp_rf"+std::to_string(_file_no)+".h5", "r");
    file.read(random_field, "rf");
    file.close();
  }

  return std::make_shared<Function>(random_field);
}
//-----------------------------------------------------------------------------
PETScMatrix TruncatedKL::_compute_basis(MPI_Comm local_comm)
{
  // Assembling the covarainace fredholm integral
  Mat C = _assemble_covariance_fredholm_integral(local_comm, _V);
  pods_log(0, "Truncated KL: Assembled covariance Fredholm integral");

  // Assembling Galerkin projection
  _assemble_galerkin_projection(local_comm, _V, C);
  pods_log(0, "Truncated KL: Assembled Galerkin projection");

  // Makes Mat into PETScMatrix.
  PETScMatrix C_mat = PETScMatrix(C);

  return C_mat;
}
//-----------------------------------------------------------------------------
Mat TruncatedKL::_assemble_covariance_fredholm_integral(MPI_Comm local_comm,
							std::shared_ptr<FunctionSpace> V)
{
  // Create Mat to assemble the fredholm integral
  Mat C;
  MatCreate(local_comm, &C);
  MatSetSizes(C, _mat_dim, _mat_dim, PETSC_DETERMINE, PETSC_DETERMINE);
  MatSetType(C, "dense");
  PetscOptionsSetFromOptions(NULL);
  MatSetUp(C);

  // Initialise vectors
  PETScVector tmp_vec(local_comm);

  // Get information about coordinates of mesh
  auto dof2v = dof_to_vertex_map(*_V);

  // Make covariance kernel
  if (_dim == 1)
  {
    pods_log(0, "Truncated KL: Making 1D covariance kernel");
    auto c = std::make_shared<Kernel>(_lambda);
    // Initialise the fredhold ufl form
    kl1D::Form_C_Fredholm c_form_fredholm(_V);

    // Evaluate \int C(x, x') \xi(x') dx'
    for (int j=0; j<dof2v.size(); j++)
    {
      // Choose point to evaluate the kernel at
      Point point = Vertex(*_mesh, dof2v[j]).midpoint();
      c->x_i = point[0];

      // Set coefficients for the fredhold ufl form
      c_form_fredholm.sigma = std::make_shared<Constant>(_sigma);
      c_form_fredholm.c = c;

      // Assemble the form
      assemble(tmp_vec, c_form_fredholm);

      // Insert vector into Mat
      for (int i=0; i<_mat_dim; i++)
	MatSetValue(C, j, i, tmp_vec[i], INSERT_VALUES);
    }
  }

  else if (_dim == 2)
  {
    pods_log(0, "Truncated KL: Making 2D covariance kernel");
    auto c = std::make_shared<Kernel2D>(_lambda);

    // Initialise the fredhold ufl form
    kl2D::Form_C_Fredholm c_form_fredholm(_V);
    pods_log(0, "Truncated KL: Initialised Fredholm form");

    // Evaluate \int C(x, x') \xi(x') dx'
    for (int j=0; j<dof2v.size(); j++)
    {
      // Choose point to evaluate the kernel at
      Point point = Vertex(*_mesh, dof2v[j]).midpoint();
      c->point = {point[0], point[1]};

      // Set coefficients for the fredhold ufl form
      c_form_fredholm.sigma = std::make_shared<Constant>(_sigma);
      c_form_fredholm.c = c;

      // Assemble to form
      assemble(tmp_vec, c_form_fredholm);

      // Insert vector into Mat
      for (int i=0; i<_mat_dim; i++)
	MatSetValue(C, j, i, tmp_vec[i], INSERT_VALUES);
    }
  }

  else if (_dim == 3)
  {
    pods_log(0, "Truncated KL: Making 3D covariance kernel");
    auto c = std::make_shared<Kernel3D>(_lambda);

    // Initialise the fredhold ufl form
    kl3D::Form_C_Fredholm c_form_fredholm(_V);
    pods_log(0, "Truncated KL: Initialised Fredholm form");

    // Evaluate \int C(x, x') \xi(x') dx'
    for (int j=0; j<dof2v.size(); j++)
    {
      // Choose point to evaluate the kernel at
      Point point = Vertex(*_mesh, dof2v[j]).midpoint();
      c->point = {point[0], point[1], point[2]};

      // Set coefficients for the fredhold ufl form
      c_form_fredholm.sigma = std::make_shared<Constant>(_sigma);
      c_form_fredholm.c = c;

      // Assemble to form
      assemble(tmp_vec, c_form_fredholm);

      // Insert vector into Mat
      for (int i=0; i<_mat_dim; i++)
	MatSetValue(C, j, i, tmp_vec[i], INSERT_VALUES);
    }
  }
  // Assemble to PETSc Mat so can be used.
  MatAssemblyBegin(C, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(C, MAT_FINAL_ASSEMBLY);

  return C;
}
//-----------------------------------------------------------------------------
void TruncatedKL::_assemble_galerkin_projection(MPI_Comm local_comm, std::shared_ptr<FunctionSpace> V,
						  Mat C)
{
  // Initialises a PETScVector
  Vec A;
  VecCreate(local_comm, &A);
  VecSetSizes(A, PETSC_DECIDE, _mat_dim);
  VecSetType(A, "dense");
  PetscOptionsSetFromOptions(NULL);
  VecSetUp(A);

  // Initialises vectors
  std::shared_ptr<Function> int_func = std::make_shared<Function>(_V);
  PETScVector vec(local_comm, _mat_dim);
  PETScVector assembled_c = PETScVector(local_comm, _mat_dim);

  // Initialises projection form
  if (_dim == 1)
  {
    kl1D::Form_C_Projection c_form_projection(_V, int_func);
    pods_log(0, "Truncated KL: Initialised projection form");
    for (int j=0; j<_mat_dim; j++)
    {
      // Gets column vector
      MatGetColumnVector(C, A, j);

      // Sets vector to the function int_func
      vec = PETScVector(A);
      *int_func->vector() = vec;

      // Assembles the form projection
      assemble(assembled_c, c_form_projection);

      for (int i=0; i<_mat_dim; i++)
	MatSetValue(C, i, j, assembled_c[i], INSERT_VALUES);

      // Assembles the Mat
      MatAssemblyBegin(C, MAT_FINAL_ASSEMBLY);
      MatAssemblyEnd(C, MAT_FINAL_ASSEMBLY);
    }
  }
  else if (_dim == 2)
  {
    kl2D::Form_C_Projection c_form_projection(_V, int_func);

    for (int j=0; j<_mat_dim; j++)
    {
      // Gets column vector
      MatGetColumnVector(C, A, j);

      // Sets vector to the function int_func
      vec = PETScVector(A);
      *int_func->vector() = vec;

      // Assembles the form projection
      assemble(assembled_c, c_form_projection);

      for (int i=0; i<_mat_dim; i++)
	MatSetValue(C, i, j, assembled_c[i], INSERT_VALUES);

      // Assembles the Mat
      MatAssemblyBegin(C, MAT_FINAL_ASSEMBLY);
      MatAssemblyEnd(C, MAT_FINAL_ASSEMBLY);
    }
  }
  else if (_dim == 3)
  {
    kl3D::Form_C_Projection c_form_projection(_V, int_func);

    for (int j=0; j<_mat_dim; j++)
    {
      // Gets column vector
      MatGetColumnVector(C, A, j);

      // Sets vector to the function int_func
      vec = PETScVector(A);
      *int_func->vector() = vec;

      // Assembles the form projection
      assemble(assembled_c, c_form_projection);

      for (int i=0; i<_mat_dim; i++)
	MatSetValue(C, i, j, assembled_c[i], INSERT_VALUES);

      // Assembles the Mat
      MatAssemblyBegin(C, MAT_FINAL_ASSEMBLY);
      MatAssemblyEnd(C, MAT_FINAL_ASSEMBLY);
    }
  }
  return;
}
//-----------------------------------------------------------------------------
std::vector<KLEigenPair>
TruncatedKL::_compute_eigen_pairs(MPI_Comm local_comm, dolfin::PETScMatrix C)
{
  // Assemble the mass matrix
  PETScMatrix M(local_comm);
  // RHS mass matrix \int \xi(x) v(x) dx
  if (_dim == 1)
  {
    kl1D::Form_Mass mass(_V, _V);
    assemble(M, mass);
    pods_log(0, "Truncated KL: Assembled 1D mass matrix");
  }
  else if (_dim == 2)
  {
    kl2D::Form_Mass mass(_V, _V);
    assemble(M, mass);
    pods_log(0, "Truncated KL: Assembled 2D mass matrix");
  }
  else if (_dim == 3)
  {
    kl3D::Form_Mass mass(_V, _V);
    assemble(M, mass);
    pods_log(0, "Truncated KL: Assembled 2D mass matrix");
  }

  // Calculate eigenpairs
  KLEigenPair pair(local_comm);
  std::vector<KLEigenPair> eigenpairs;

  // Initialise the eigen solver
  dolfin::SLEPcEigenSolver
    esolver = SLEPcEigenSolver(local_comm, std::make_shared<PETScMatrix>(C),
			       std::make_shared<PETScMatrix>(M));
  esolver.parameters["spectrum"] = "largest magnitude";
  esolver.parameters["problem_type"] = "gen_hermitian";
  esolver.parameters["solver"] = "lapack";

  // Check truncation number isn't bigger than dofs
  int n_evs = _truncation_number;
  if (n_evs > _mat_dim)
  {
    pods_error("TruncatedKL.cpp",
	       "calculate eigenpairs",
	       "Desired number of eigenvalues is bigger than matrix dimension");

  }
  esolver.solve(n_evs);

  // Initialise variables
  double lc;
  PETScVector c(local_comm);
  Vec tmp;
  VecCreate(local_comm, &tmp);
  VecSetSizes(tmp, PETSC_DECIDE, _mat_dim);
  VecSetType(tmp, "dense");
  PetscOptionsSetFromOptions(NULL);
  VecSetUp(tmp);

  for (int i=0; i<n_evs; ++i)
  {
    esolver.get_eigenpair(pair.eigenvalue, lc, pair.eigenfunction, c, i);

    if (pair.eigenfunction[0] < 0)
    {
      tmp = pair.eigenfunction.vec();
      VecScale(tmp, -1);
      pair.eigenfunction = PETScVector(tmp);
    }

    eigenpairs.push_back(pair);
  }
  return eigenpairs;
}
//-----------------------------------------------------------------------------
void TruncatedKL::_check_l2_norm(MPI_Comm local_comm,
				 std::vector<KLEigenPair> eigenpairs)
{
  Function eig_function = Function(_V);
  Function eig_function2 = Function(_V);

  Scalar s(local_comm);
  double integral;

  for (int i=0; i<eigenpairs.size(); i++)
  {
    for (int j=0; j<eigenpairs.size(); j++)
    {
      *eig_function.vector() = eigenpairs[i].eigenfunction;
      *eig_function2.vector() = eigenpairs[j].eigenfunction;

      if (_dim == 1)
      {
	kl1D::Form_KL eigenvector_integral(_mesh);
	eigenvector_integral.psi = std::make_shared<Function>(eig_function);
	eigenvector_integral.phi = std::make_shared<Function>(eig_function2);
	assemble(s, eigenvector_integral);
	integral = s.get_scalar_value();
      }
      else if(_dim == 2)
      {
	kl2D::Form_KL eigenvector_integral(_mesh);
	eigenvector_integral.psi = std::make_shared<Function>(eig_function);
	eigenvector_integral.phi = std::make_shared<Function>(eig_function2);
	assemble(s, eigenvector_integral);
	integral = s.get_scalar_value();
      }
      else if(_dim == 3)
      {
	kl3D::Form_KL eigenvector_integral(_mesh);
	eigenvector_integral.psi = std::make_shared<Function>(eig_function);
	eigenvector_integral.phi = std::make_shared<Function>(eig_function2);
	assemble(s, eigenvector_integral);
	integral = s.get_scalar_value();
      }

      std::string msg;
      msg = "phi_" + std::to_string(i) +  " phi_" + std::to_string(j)  + ": " + std::to_string(integral);
      pods_log(0, msg);

    }
  }
  return;
}
//-----------------------------------------------------------------------------
