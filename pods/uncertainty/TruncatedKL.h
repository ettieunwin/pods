// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-06-21
// Last changed: 2017-06-21

#ifndef TRUNCATED_KL_H
#define TRUNCATED_KL_H

#include <dolfin.h>
#include "StochasticFunction.h"

namespace pods
{

  struct KLEigenPair
  {
  KLEigenPair(MPI_Comm local_comm)
  : eigenfunction(dolfin::PETScVector(local_comm)) {}
    double eigenvalue;
    dolfin::PETScVector eigenfunction;
  };

  class TruncatedKL :
    /// This class enables a random field (X~N(\mu, \signa^2)
    /// to be realised from a probabilty space numerically using a
    /// truncated KL expansion with a separable exponential
    /// covariance kernel using Galerkin projection.
    /// This is computationally expensive,
    /// especially in high dimensions.
    public StochasticFunction<std::shared_ptr<dolfin::Function>>
  {
  public:
    // Constructor
    TruncatedKL(std::shared_ptr<const dolfin::Mesh> mesh,
		ProbabilitySpace& omega,
		double mu, double sigma, std::vector<double> lambda,
		int truncation_number,
		bool debug=false);

    // Destructor
    ~TruncatedKL() {};
    
    // Samples from probability space to generate a random field
    std::shared_ptr<dolfin::Function> sample();
    
  private:
    // Computes Mercer basis functions
    dolfin::PETScMatrix _compute_basis(MPI_Comm local_comm);

    // Assembles fredholm integral \int C(x, x') \xi(x') dx'
    Mat _assemble_covariance_fredholm_integral(MPI_Comm local_comm,
					       std::shared_ptr<dolfin::FunctionSpace> V);

    // Assembles galerkin projection \int C v dx
    void _assemble_galerkin_projection(MPI_Comm local_comm,
				       std::shared_ptr<dolfin::FunctionSpace> V,
				       Mat C);

    // Computes eigenpairs
    std::vector<KLEigenPair> _compute_eigen_pairs(MPI_Comm local_comm,
						  dolfin::PETScMatrix C);

    // Checks orthonormal properties of eigenfinctions
    void _check_l2_norm(MPI_Comm local_comm,
			std::vector<KLEigenPair> eigenpairs);

    // Meshes
    std::shared_ptr<const dolfin::Mesh> _mesh;

    // Probability space
    ProbabilitySpace& _omega;

    // Coefficients for properties of random field
    double  _mu, _sigma;
    std::vector<double> _lambda;
    int _truncation_number, _mat_dim, _dim, _size, _rank, _file_no;

    // Communicator
    MPI_Comm _comm;

    // Function space
    std::shared_ptr<dolfin::FunctionSpace> _V, _V_all;

    // Debug switch
    bool _debug;
  };
}
#endif
