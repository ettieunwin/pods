// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-06-22
// Last changed: 2017-06-22

#ifndef FUNCTIONHOLDER_H
#define FUNCTIONHOLDER_H

#include <functional>

namespace pods
{
  class FunctionHolder
  {
  public:
    // Constructs a function holder
    FunctionHolder(std::function<double(double)> function,
		   std::function<double(double)> derivative_function)
      : _function(function), _derivative_function(derivative_function)
    {
    };
    
    // Destructor
    ~FunctionHolder()
    {
    };

    std::function<double(double)> get_function()
    {
      return _function;
    };
    
    std::function<double(double)>
      get_derivative_function()
    {
      return _derivative_function;
    };
    
  private:
    std::function<double(double)> _function, _derivative_function;
  };
}

#endif
