// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-03-29
// Last changed: 2017-03-29

#ifndef RANDOMNUMBERGENERATOR_H
#define RANDOMNUMBERGENERATOR_H

#include <iostream>
#include <vector>
#include <memory>
#include <random>
#include <mpi.h>
#include <functional>

namespace pods
{
class RandomNumberGenerator
{
public:
  // Constructor where comm is the communication, seeds is a seed for
  // each processor and stats is range for uniform_distribution.
  RandomNumberGenerator(double processor_seed, std::vector<double> stats)
    : _processor_seed(processor_seed), _stats(stats)
  {
  }

  // Destructor
  ~RandomNumberGenerator()
  {
  }

  // Generates a uniform distribution generator
  std::function<double()> uniform_distribution(std::mt19937& gen);

  // Generates a Gaussian distribution generator
  std::function<double()> gaussian_distribution(std::mt19937& gen);

private:
  // Vector of seeds
  double _processor_seed;

  // Vector of stats for random field
  std::vector<double> _stats;
};
}
#endif
