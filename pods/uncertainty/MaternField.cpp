// Copyright (C) 2017 Nathan Sime and Ettie unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-04-20
// Last changed: 2017-04-20

#include "MaternField.h"
#include "Matern_1d.h"
#include "Matern_2d.h"
#include "Matern_3d.h"
#include "../log/PodsLog.h"
#include <dolfin.h>
#include <ctime>
#include <math.h>

using namespace pods;
using namespace dolfin;

// Sub domain for boundary
class Boundary : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary;
  }
};
//-----------------------------------------------------------------------------
std::shared_ptr<dolfin::Function> MaternField::sample()
{
 
  pods_log(20, "MaternField: Resampling");

  // Choose alpha = 2 so solve (\kappa - \nabla**{2})u = w
  int alpha = 2;
  int dim = _mesh->geometry().dim();
  double nu = alpha - dim/2.0;
  double kappa = pow(8*nu, 0.5)/_lambda;
  
  // Calculating sigma of computed equation
  double d_sigma = pow(tgamma(nu)/(tgamma(nu + dim/2.0)
  				   *pow(4*M_PI,dim/2.0)
				   *pow(kappa,2.0*nu)), 0.5);
  
  // Generate random number generator
  std::mt19937 gen;
  // Generates normally distributed random numbers N~(0,1^{2})
  std::function<double()> random_number_generator
    = _omega.generate_random_number_generator(gen, {0,1});

  std::shared_ptr<dolfin::FunctionSpace> V_M, W_M;
  std::shared_ptr<dolfin::Form> a, L;

  if (dim == 1)
  {  
    V_M = std::make_shared<Matern_1d::FunctionSpace>(_mesh);
    a = std::make_shared<Matern_1d::BilinearForm>(V_M, V_M);
    L = std::make_shared<Matern_1d::LinearForm>(V_M);
    W_M = std::make_shared<Matern_1d::CoefficientSpace_w>(_mesh);
  }
  else if (dim == 2)
  {  
    V_M = std::make_shared<Matern_2d::FunctionSpace>(_mesh);
    a = std::make_shared<Matern_2d::BilinearForm>(V_M, V_M);
    L = std::make_shared<Matern_2d::LinearForm>(V_M);
    W_M = std::make_shared<Matern_2d::CoefficientSpace_w>(_mesh);
  }
  else if (dim == 3)
  {  
    V_M = std::make_shared<Matern_3d::FunctionSpace>(_mesh);
    a = std::make_shared<Matern_3d::BilinearForm>(V_M, V_M);
    L = std::make_shared<Matern_3d::LinearForm>(V_M);
    W_M = std::make_shared<Matern_3d::CoefficientSpace_w>(_mesh);
  }
  
  auto kappa_const = std::make_shared<dolfin::Constant>(kappa);         
  std::map<std::string, std::shared_ptr<const dolfin::GenericFunction>> coefficients_a
    = {{"kappa", kappa_const}};
  a->set_coefficients(coefficients_a);

  auto w = std::make_shared<dolfin::Function>(W_M);          
  std::map<std::string, std::shared_ptr<const dolfin::GenericFunction>> coefficients_L
    = {{"w", w}};
  L->set_coefficients(coefficients_L);

  pods_log(20, "MaternField: Generating forcing function");
  std::vector<double> sample_numbers(w->vector()->local_size());
  std::generate(sample_numbers.begin(),
                sample_numbers.end(),
                random_number_generator);

  std::vector<dolfin::la_index> row_idx(w->vector()->local_size());
  std::iota(row_idx.begin(), row_idx.end(), 0);

  w->vector()->set_local(sample_numbers.data(),
                         w->vector()->local_size(),
                         row_idx.data());

  auto result = std::make_shared<dolfin::Function>(V_M);

  auto boundary = std::make_shared<Boundary>();
  auto bc = dolfin::DirichletBC(V_M, std::make_shared<dolfin::Constant>(0.0),
				boundary);
  
  pods_log(20, "MaternField: Solving Lp-Laplace problem");
  dolfin::solve(*a == *L, *result, bc);
  
  // Changes the mean and varaince of the function N~(mu, sigma^{2})
  auto random_field = std::make_shared<dolfin::Function>(V_M);
  random_field->interpolate(dolfin::Constant(_mu));
  double c = _sigma/(double) d_sigma;
  random_field->vector()->axpy(c, *result->vector());
  
  pods_log(20, "MaternField: generated the random field");
  return random_field;
}
