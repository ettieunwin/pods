// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// The logger is based on the FEniCS libraries logger, see
// <https://fenicsproject.org>.
//
// First added:  2017-04-06
// Last changed: 2017-04-06

#include <dolfin.h>
#include <assert.h>
#include <cmath>
#include <fstream>
#include <string>
#include <memory>
#include "kl1D.h"
#include "kl2D.h"
#include "kl3D.h"
#include "TruncatedKLAnalytic.h"
#include "../log/PodsLog.h"
#include "FunctionHolder.h"
#include "NewtonRhapson.h"

using namespace dolfin;
using namespace pods;
//------------------------------------------------------------------------------
class FunctionOdd
{
 public:

  FunctionOdd(double a, double lambda)
    : _a(a), _lambda(lambda)
  {
  }

  double function(double x)
  {
    return 1.0/_lambda - x*tan(_a*x);
  }
 private:
  double _a, _lambda;
};
//------------------------------------------------------------------------------
class DerivativeFunctionOdd
{
 public:
  DerivativeFunctionOdd(double a, double lambda)
    : _a(a), _lambda(lambda)
  {
  }

  double function(double x)
  {
    return -tan(_a*x) - _a*x*pow(1/cos(_a*x),2);
  }
 private:
  double _a, _lambda;
};
//------------------------------------------------------------------------------
class FunctionEven
{
 public:

  FunctionEven(double a, double lambda)
    : _a(a), _lambda(lambda)
  {
  }

  double function(double x)
  {
    return (1.0/_lambda)*tan(_a*x) + x;
  }
 private:
  double _a, _lambda;
};
//------------------------------------------------------------------------------
class DerivativeFunctionEven
{
 public:
  DerivativeFunctionEven(double a, double lambda)
    : _a(a), _lambda(lambda)
  {
  }

  double function(double x)
  {
    return 1 + (_a/_lambda)*pow(1/cos(_a*x),2);
  }
 private:
  double _a, _lambda;
};
//------------------------------------------------------------------------------
class FunctionZero
{
 public: FunctionZero(double a, double lambda)
    : _a(a), _lambda(lambda)
  {
  }

  double function(double x)
  {
    return tan(_a*x) - (2*_lambda*x)/(pow(_lambda,2)*pow(x,2)-1);
  }
 private:
  double _a, _lambda;
};
//------------------------------------------------------------------------------
class DerivativeFunctionZero
{
 public:
  DerivativeFunctionZero(double a, double lambda)
    : _a(a), _lambda(lambda)
  {
  }

  double function(double x)
  {
    return _a/(pow(cos(_a*x),2)) +
      (2*pow(x,2)*pow(_lambda,3) + 2*_lambda)/pow((pow(x,2)*pow(_lambda,2)-1),2);
  }
 private:
  double _a, _lambda;
};
//------------------------------------------------------------------------------
TruncatedKLAnalytic::TruncatedKLAnalytic(std::shared_ptr<const dolfin::Mesh> mesh,
					 ProbabilitySpace& omega,
					 double mu, double sigma,
					 int truncation_number,
					 std::vector<NRInfo> nr_info,
					 bool debug)
  :  _omega(omega), _mu(mu), _sigma(sigma), _nr_info(nr_info), _debug(debug)
{
  // Gets group communicator information
  MPI_Comm comm=mesh->mpi_comm();
  _comm = comm;
  int rank, size;
  MPI_Comm_rank(_comm, &rank);
  MPI_Comm_size(_comm, &size);
  _rank = rank;
  _size = size;

  // To avoid file writing/ reading conflicts
  int file_no;
  if (rank == 0)
    MPI_Comm_rank(MPI_COMM_WORLD, &file_no);
  MPI_Bcast(&file_no, 1, MPI_INT, 0, _comm);
  _file_no = file_no;

  // Ensures can have equal numbers of odd and even roots of trancscendental
  // equations
  truncation_number = std::floor(truncation_number/2.0)*2.0;
  _truncation_number = truncation_number;

  // Gets num dof
  int dim = mesh->geometry().dim();
  _dim = dim;
  assert(dim == nr_info.size());

  // Creates function space for all the communicators together
  std::shared_ptr<FunctionSpace> V_all;
  if (_dim == 1)
    V_all = std::make_shared<kl1D::FunctionSpace>(mesh);
  else if (_dim == 2)
    V_all = std::make_shared<kl2D::FunctionSpace>(mesh);
  else if (_dim == 3)
    V_all = std::make_shared<kl3D::FunctionSpace>(mesh);

  // If size is equal to 1, mesh and function space are already only
  // on zeroth processor
  if (_size == 1)
  {
    // Assigns objects for just zeroth processor
    _V = V_all;
    _mesh = mesh;
  }
  else
  {
    pods_log(0, "Analytic Truncated KL: Finding random field on zeroth ranked processor");
    HDF5File tmp_mesh(comm,
		      "tmp_mesh"+std::to_string(_file_no)+".h5", "w");
    tmp_mesh.write(*mesh, "mesh");
    tmp_mesh.close();
    if (_rank == 0)
    {

      pods_log(0, "Analytic Truncated KL: Reading mesh in on zeroth ranked processor");
      // All processors write to mesh - has to be to HDF5!!
      std::shared_ptr<Mesh> new_mesh = std::make_shared<Mesh>(MPI_COMM_SELF);
      HDF5File tmp_mesh_read(MPI_COMM_SELF,
			     "tmp_mesh"+std::to_string(_file_no)+".h5", "r");
      tmp_mesh_read.read(*new_mesh, "mesh", false);
      tmp_mesh_read.close();
      _mesh = new_mesh;

      if (_dim == 1)
	_V = std::make_shared<kl1D::FunctionSpace>(new_mesh);
      else if (_dim == 2)
	_V = std::make_shared<kl2D::FunctionSpace>(new_mesh);
      else if (_dim == 3)
	_V = std::make_shared<kl2D::FunctionSpace>(new_mesh);
    }
  }

  _V_all = V_all;

  // Barrier to make sure that the zeroth processor has finished
  MPI_Barrier(_comm);
}
//------------------------------------------------------------------------------
std::shared_ptr<Function> TruncatedKLAnalytic::sample()
{
  pods_log(20, "Analytic Truncated KL: Resampling");
  Function random_field = Function(_V_all);

  if (_rank == 0)
  {
    // Calculate analytic eigenpairs
    std::vector<EigenPair> eigen_pairs = calculate_eigen_pairs(MPI_COMM_SELF);
    pods_log(0, "Analytic Truncated KL: Calculated analytic eigen pairs");

    // Initialises functions
    Function eig_function = Function(_V);
    Function random_field_zero = Function(_V);
    random_field_zero = Constant(_mu);

    // Create random numbers generator using the probability space
    std::mt19937 gen;
    std::vector<double> stats = {0.0, 1.0};
    std::function<double()> random_number_generator
      = _omega.generate_random_number_generator(gen,stats);

    // Sample random numbers from the probability space
    std::vector<double> sample_numbers(eigen_pairs.size());
    std::generate(sample_numbers.begin(),
		  sample_numbers.end(),
		  random_number_generator);
    pods_log(0, "Analytic Truncated KL: Generated random numbers");
    for (int i=0; i<10; ++i)
      std::cout << sample_numbers[i] << ", ";
    std::cout << std::endl;

    // Creates files
    XDMFFile random_field_file(MPI_COMM_SELF,
			       "random_field_analytic"
			       +std::to_string(_file_no)+".xdmf");
    XDMFFile eig_funcs_file(MPI_COMM_SELF,
			    "eigen_functions_analytic"
			    +std::to_string(_file_no)+".xdmf");

    std::string evs;
    evs += "Analytic Truncated KL: Eigenvalues: ";

    // Generate the realisation
    for (int k=0; k<_truncation_number; ++k)
    {
      // Ensure that get comparable eigenvectors
      if (_dim == 1)
	eig_function.interpolate(eigen_pairs[k].eigenfunction.one_D);
      else if (_dim == 2)
	eig_function.interpolate(eigen_pairs[k].eigenfunction.two_D);
      else if (_dim == 3)
	eig_function.interpolate(eigen_pairs[k].eigenfunction.three_D);


      if ((*eig_function.vector())[0] < 0)
      {
	*eig_function.vector()*=-1.0;
      }
      random_field_zero.vector()->axpy(pow(eigen_pairs[k].eigenvalue, 0.5)
				       *sample_numbers[k],
				       *(eig_function).vector());

      if (_debug==true)
      {
	random_field_file.write(random_field_zero, k);
	eig_funcs_file.write(eig_function, k);
      }

      evs += std::to_string(eigen_pairs[k].eigenvalue) + ", ";
    }
    pods_log(0, evs);

    pods_log(0, "Analytic Tuncated KL: Calculated truncated KL expansion");

    // If one one processor, random field is as calculated on zeroth processor
    if (_size == 1)
        random_field = random_field_zero;
    // Save the random field from one processor
    else
    {
      HDF5File h5(MPI_COMM_SELF, "tmp_rf"+std::to_string(_file_no)+".h5", "w");
      h5.write(random_field_zero, "rf");
      h5.close();
    }
  }

  // Holds all processors until zeroth one has calculated field
  pods_log(0, "Analytic Tuncated KL: Being held");
  MPI_Barrier(_comm);

  // If running using a group of processors, reads in random field
  if (_size >  1)
  {
    HDF5File file(_comm, "tmp_rf"+std::to_string(_file_no)+".h5", "r");
    file.read(random_field, "rf");
    file.close();
  }

  pods_log(0, "Analytic Tuncated KL: Made truncated KL expansion");
  return std::make_shared<Function>(random_field);
}
//---------------------------------------------------------------------------
Roots TruncatedKLAnalytic::compute_roots_two(NRInfo nr_info)
{
  // Calculate function holder to calculate roots
  FunctionHolder fh_odd = _create_function_holder_odd(nr_info.b,
						      nr_info.lambda);
  FunctionHolder fh_even = _create_function_holder_even(nr_info.b,
							nr_info.lambda);

  // Period of repetition in roots
  double period = M_PI/nr_info.b;

  Roots roots;

  // Use Newton Rahpson method to find even and odd roots
  NewtonRhapson nr_odd = NewtonRhapson(fh_odd, _truncation_number/2,
				       nr_info.initial_guesses_odd,
				       period);
  roots.odd_roots = nr_odd.get_roots();

  NewtonRhapson nr_even = NewtonRhapson(fh_even, _truncation_number/2,
					nr_info.initial_guesses_even,
					period);
  roots.even_roots = nr_even.get_roots();

  return roots;
}
//---------------------------------------------------------------------------
std::vector<double> TruncatedKLAnalytic::compute_roots_one(NRInfo nr_info)
{
  // Calculate function holder to calculate roots
  FunctionHolder fh = _create_function_holder(nr_info.b,
					      nr_info.lambda);

  // Period of repetition in roots
  double period = M_PI/nr_info.b;

  // Use Newton Rahpson method to find even and odd roots
  NewtonRhapson nr = NewtonRhapson(fh, _truncation_number,
				   nr_info.initial_guesses,
				   period);
  std::vector<double> roots = nr.get_roots();

  return roots;
}
//------------------------------------------------------------------------------
std::vector<EigenPair>
TruncatedKLAnalytic::calculate_eigen_pairs(MPI_Comm comm)
{
  std::vector<DimensionInfo> all_dim_holders = _calculate_constants_and_roots();

  // Calculate eigenvalues
  std::vector<double> eigenvalues = _calculate_eigenvalues(all_dim_holders);
  pods_log(0, "Analytic Truncated KL: Calculated eigenvalues of covariance kernel");

  // Calculates eigenfunctions
  std::vector<EigenFunction> eigenfunctions
    = _calculate_eigenfunctions(all_dim_holders);
  pods_log(0, "Analytic Truncated KL: Calculated eigenfunctions of covariance kernel");

  // Checks orthonormal property of eigenfunctions if asked
  if (_debug == true)
    _check_l2_norm(comm, all_dim_holders);

  // Calculate eigen pairs
  EigenPair pair;
  std::vector<EigenPair> eigen_pairs;

  // Sort eigen values
  std::vector<size_t> indexes = _sort_indexes(eigenvalues);
  pods_log(0, "Analytic Truncated KL: Sorted eigenvalues");

  // Make eigenpairs based from indexes
  assert(eigenfunctions.size() == eigenvalues.size());

  for (int k=0; k<indexes.size() ; k++)
  {
    pair.eigenfunction = eigenfunctions[indexes[k]];
    pair.eigenvalue = eigenvalues[indexes[k]];
    eigen_pairs.push_back(pair);
  }
  return eigen_pairs;
}
//------------------------------------------------------------------------------
std::vector<DimensionInfo> TruncatedKLAnalytic::_calculate_constants_and_roots()
{
  // Define vectors
  DimensionInfo dim_holder;
  std::vector<DimensionInfo> all_dim_holders(_dim);

  // If domain is [-b, b]
  if ( _nr_info[0].a ==  -_nr_info[0].b)
  {
    pods_log(20, "Analytic Truncated KL: Have domain [-a, a].");
    Roots roots_holder;
    std::vector<double> roots(_truncation_number);
    std::vector<double> As_odd(_truncation_number/2);
    std::vector<double> Bs_even(_truncation_number/2);

    // Calculate the eigenfunctions and values for each dimension
    for (int i=0; i<_dim; ++i)
    {
      // Get roots
      roots_holder = compute_roots_two(_nr_info[i]);
      pods_log(0, "Analytic Truncated KL: Calculated roots of transcendental equations");

      // Calculate constants
      std::vector<std::vector<double>> constants(_truncation_number);
      As_odd = _calculate_As(roots_holder.odd_roots, _nr_info[i].b);
      Bs_even = _calculate_Bs(roots_holder.even_roots, _nr_info[i].b);
      pods_log(0, "Analytic Truncated KL: Calculated constants of eigenfunctions");

      // Group constants and roots
      for (int j=0; j<_truncation_number/2; ++j)
      {
	constants[2*j] = {As_odd[j], 0.0};
	constants[2*j+1] = {0.0, Bs_even[j]};

	roots[2*j] = roots_holder.odd_roots[j];
	roots[2*j+1] = roots_holder.even_roots[j];
      }

      dim_holder.roots = roots;
      dim_holder.constants = constants;
      dim_holder.lambda = _nr_info[i].lambda;

      all_dim_holders[i] = dim_holder;
    }
  }

  // If domain is [0, b]
  else if (_nr_info[0].a == 0.0)
  {
    pods_log(20, "Analytic Truncated KL: Have domain [0, b].");

    // Calculate the constants for each dimension
    // Cliffe et al
    for (int i=0; i<_dim; ++i)
    {
      // Compute roots
      dim_holder.roots = compute_roots_one(_nr_info[i]);
      pods_log(0, "Analytic Truncated KL: Calculated roots of transcendental equations");

      // Compute constants
      std::vector<std::vector<double>> constants(dim_holder.roots.size());
      std::vector<double> Cs= _calculate_Cs(dim_holder.roots, _nr_info[i].b,
					    _nr_info[i].lambda);
      // Calculate vector of vectors of constants
      for (int j=0; j<Cs.size(); j++)
	constants[j] = {_nr_info[i].lambda*dim_holder.roots[j]*Cs[j], Cs[j]};

      dim_holder.constants = constants;
      pods_log(0, "Analytic Truncated KL: Calculated constants of eigenfunctions");

      dim_holder.lambda = _nr_info[i].lambda;

      all_dim_holders[i] = dim_holder;
    }
  }

  else
  {
    pods_error("TruncatedKLAnalytic.cpp",
	       "calculating roots and constants for analytic expansion",
	       "domain is not [-a, a] or [0,b]");
  }

  return all_dim_holders;
}
//------------------------------------------------------------------------------
std::vector<double>
TruncatedKLAnalytic::_calculate_As(std::vector<double> roots, double a)
{
  std::vector<double> As(roots.size());
  for (int i=0; i<roots.size(); i++)
     As[i] = 1.0/pow(a + sin(2.0*roots[i]*a)/(2.0*roots[i]), 0.5);
  return As;
}
//------------------------------------------------------------------------------
std::vector<double>
TruncatedKLAnalytic::_calculate_Bs(std::vector<double> roots, double a)
{
  std::vector<double> Bs(roots.size());
  for (int i=0; i<roots.size(); i++)
    Bs[i] = 1.0/pow(a - sin(2.0*roots[i]*a)/(2.0*roots[i]), 0.5);
  return Bs;
}
//------------------------------------------------------------------------------
std::vector<double>
TruncatedKLAnalytic::_calculate_Cs(std::vector<double> roots,
				   double a,
				   double lambda)
{
  std::vector<double> Cs(roots.size());
  for (int i=0; i<roots.size(); i++)
    Cs[i] = pow((4.0*roots[i])/((pow(lambda,2)*pow(roots[i],2) - 1)*sin(2.0*a*roots[i]) + 2.0*roots[i]*a*(pow(lambda,2)*pow(roots[i],2) + 1)), 0.5);
  return Cs;
}
//------------------------------------------------------------------------------
FunctionHolder TruncatedKLAnalytic::_create_function_holder_odd(double a,
								double lambda)
{
  // Make FunctionHolder which holds the lambda function for the odd roots
  std::function<double(double)> function_odd
    = [nr_function = FunctionOdd(a, lambda)](double x) mutable
    {return nr_function.function(x);};
  std::function<double(double)> derivative_function_odd
    = [nr_derivative_function = DerivativeFunctionOdd(a, lambda)](double x)
    mutable {return nr_derivative_function.function(x);};
  FunctionHolder fh_odd = FunctionHolder(function_odd, derivative_function_odd);

  return fh_odd;
}
//------------------------------------------------------------------------------
FunctionHolder TruncatedKLAnalytic::_create_function_holder_even(double a,
								 double lambda)
{
 // Make FunctionHolder which holds the lambda function for the even roots
  std::function<double(double)> function_even
    = [nr_function = FunctionEven(a, lambda)](double x) mutable
    {return nr_function.function(x);};
  std::function<double(double)> derivative_function_even
    = [nr_derivative_function = DerivativeFunctionEven(a, lambda)](double x)
    mutable {return nr_derivative_function.function(x);};
  FunctionHolder fh_even
    = FunctionHolder(function_even, derivative_function_even);

  return fh_even;
}
//------------------------------------------------------------------------------
FunctionHolder TruncatedKLAnalytic::_create_function_holder(double a,
							    double lambda)
{
 // Make FunctionHolder which holds the lambda function for the even roots
  std::function<double(double)> function
    = [nr_function = FunctionZero(a, lambda)](double x) mutable
    {return nr_function.function(x);};
  std::function<double(double)> derivative_function
    = [nr_derivative_function = DerivativeFunctionZero(a, lambda)](double x)
    mutable {return nr_derivative_function.function(x);};
  FunctionHolder fh
    = FunctionHolder(function, derivative_function);

  return fh;
}
//------------------------------------------------------------------------------
std::vector<double>
TruncatedKLAnalytic::_calculate_eigenvalues(std::vector<DimensionInfo> all_dimension_holders)
{
  std::vector<double> eigenvalues;

  double eig;
  if (_dim == 1)
  {
    for (int i=0; i< all_dimension_holders[0].roots.size(); i++)
    {
      eig = (2.0*pow(_sigma,2)*all_dimension_holders[0].lambda)
	/(pow(all_dimension_holders[0].roots[i], 2)
	  *pow(all_dimension_holders[0].lambda, 2) + 1);

      eigenvalues.push_back(eig);

    }
  }

  if (_dim == 2)
  {
    for (int i=0; i<all_dimension_holders[0].roots.size(); i++)
    {
      for (int j=0; j<all_dimension_holders[1].roots.size(); j++)
      {
        eigenvalues.push_back(pow(_sigma,2)*((2.0*all_dimension_holders[0].lambda)/(pow(all_dimension_holders[0].roots[i], 2)*pow(all_dimension_holders[0].lambda, 2) + 1))*((2.0*all_dimension_holders[1].lambda)/(pow(all_dimension_holders[1].roots[j], 2)*pow(all_dimension_holders[1].lambda, 2) + 1)));

      }
    }
  }

  if (_dim == 3)
  {
    double eig_i, eig_j, eig_k;
    for (int i=0; i<all_dimension_holders[0].roots.size(); i++)
    {
      for (int j=0; j<all_dimension_holders[1].roots.size(); j++)
      {
	for (int k=0; k<all_dimension_holders[2].roots.size(); k++)
	{

	  eig_i = (2.0*all_dimension_holders[0].lambda)/
	    (pow(all_dimension_holders[0].roots[i], 2)
	     *pow(all_dimension_holders[0].lambda, 2) + 1);
	  eig_j = (2.0*all_dimension_holders[1].lambda)/
	    (pow(all_dimension_holders[1].roots[j], 2)
	     *pow(all_dimension_holders[1].lambda, 2) + 1);
	  eig_k = (2.0*all_dimension_holders[2].lambda)/
	    (pow(all_dimension_holders[2].roots[k], 2)
	     *pow(all_dimension_holders[2].lambda, 2) + 1);
	    eigenvalues.push_back(pow(_sigma, 2)*eig_i*eig_j*eig_k);
	}
      }
    }
  }
  return eigenvalues;
}
//------------------------------------------------------------------------------
std::vector<EigenFunction>
TruncatedKLAnalytic::_calculate_eigenfunctions(std::vector<DimensionInfo> all_dimension_holders)
{
  std::vector<EigenFunction> eigenfunctions;
  EigenFunction one_eigenfunction;

  if (_dim == 1)
  {
    std::vector<double> constants;
    double root;
    for (int i=0; i<all_dimension_holders[0].roots.size(); i++)
    {
      constants = all_dimension_holders[0].constants[i];
      root = all_dimension_holders[0].roots[i];
      one_eigenfunction.one_D = EigenFunction1D(constants, root);
      eigenfunctions.push_back(EigenFunction(one_eigenfunction));
    }
  }

  else if(_dim == 2)
  {
    std::vector<double> constantsx, constantsy;
    double rootx, rooty;

    for (int i=0; i<all_dimension_holders[0].roots.size(); i++)
    {
      for (int j=0; j<all_dimension_holders[1].roots.size(); j++)
      {
	constantsx = all_dimension_holders[0].constants[i];
	rootx = all_dimension_holders[0].roots[i];

	constantsy = all_dimension_holders[1].constants[j];
	rooty = all_dimension_holders[1].roots[j];

	one_eigenfunction.two_D = EigenFunction2D(constantsx,rootx,
						constantsy, rooty);
	eigenfunctions.push_back(EigenFunction(one_eigenfunction));
      }
    }
  }

  else if(_dim == 3)
  {
    std::vector<double> constantsx, constantsy, constantsz;
    double rootx, rooty, rootz;

    for (int i=0; i<all_dimension_holders[0].roots.size(); i++)
    {
      for (int j=0; j<all_dimension_holders[1].roots.size(); j++)
      {
	for (int k=0; k<all_dimension_holders[2].roots.size(); k++)
	{
	  constantsx = all_dimension_holders[0].constants[i];
	  rootx = all_dimension_holders[0].roots[i];

	  constantsy = all_dimension_holders[1].constants[j];
	  rooty = all_dimension_holders[1].roots[j];

	  constantsz = all_dimension_holders[2].constants[k];
	  rootz = all_dimension_holders[2].roots[k];

	  one_eigenfunction.three_D = EigenFunction3D(constantsx,rootx,
						      constantsy, rooty,
						      constantsz, rootz);
	  eigenfunctions.push_back(EigenFunction(one_eigenfunction));
	}
      }
    }
  }
  return eigenfunctions;
}
//------------------------------------------------------------------------------
std::vector<size_t> TruncatedKLAnalytic::_sort_indexes(std::vector<double> v)
{
  // initialize original index locations
  std::vector<size_t> idx(v.size());
  std::iota(idx.begin(), idx.end(), 0);

  // sort indexes based on comparing values in v
  std::sort(idx.begin(), idx.end(),
       [&v](size_t i1, size_t i2) {return v[i1] > v[i2];});

  return idx;
}
//------------------------------------------------------------------------------
void TruncatedKLAnalytic::_check_l2_norm(MPI_Comm comm, std::vector<DimensionInfo> all_dimension_holders)
{
  //Check integrals:
  Scalar s(comm);
  double integral;
  std::string msg;

  if (_dim == 1)
  {
    for (int i=0; i<all_dimension_holders[0].roots.size(); i++)
    {
      for (int j=0; j<all_dimension_holders[0].roots.size(); j++)
      {
	std::vector<double> constantsi = all_dimension_holders[0].constants[i];
	double rooti = all_dimension_holders[0].roots[i];
	std::vector<double> constantsj = all_dimension_holders[0].constants[j];
	double rootj = all_dimension_holders[0].roots[j];
	kl1D::Form_KL eigenvector_integral(_mesh, std::make_shared<EigenFunction1D>(constantsi, rooti), std::make_shared<EigenFunction1D>(constantsj, rootj));

	assemble(s, eigenvector_integral);
	integral = s.get_scalar_value();

	msg = "phi_" + std::to_string(i) +  " phi_" + std::to_string(j)  + ": " + std::to_string(integral);

	pods_log(0, msg);
      }
    }
  }

  else if (_dim == 2)
  {
    for (int i=0; i<all_dimension_holders[0].roots.size(); i++)
    {
      for (int j=0; j<all_dimension_holders[1].roots.size(); j++)
      {
	for (int k=0; k<all_dimension_holders[0].roots.size(); k++)
	{
	  for (int l=0; l<all_dimension_holders[1].roots.size(); l++)
	  {
	    std::vector<double>
	      constantsi = all_dimension_holders[0].constants[i];
	    double rooti = all_dimension_holders[0].roots[i];
	    std::vector<double>
	      constantsj = all_dimension_holders[1].constants[j];
	    double rootj = all_dimension_holders[1].roots[j];
	    std::vector<double>
	      constantsk = all_dimension_holders[0].constants[k];
	    double rootk = all_dimension_holders[0].roots[k];
	    std::vector<double>
	      constantsl = all_dimension_holders[1].constants[l];
	    double rootl = all_dimension_holders[1].roots[l];
	    kl2D::Form_KL eigenvector_integral(_mesh, std::make_shared<EigenFunction2D>(constantsi, rooti, constantsj, rootj), std::make_shared<EigenFunction2D>(constantsk, rootk, constantsl, rootl));

	    assemble(s, eigenvector_integral);
	    integral = s.get_scalar_value();

	    msg = "phi_" + std::to_string(i) + "*" + std::to_string(j)
	      + " phi_" + std::to_string(k) + "*" + std::to_string(l)
	      + ": " + std::to_string(integral);
	    pods_log(0, msg);
	  }
	}
      }
    }
  }

  else if (_dim == 3)
    {
    for (int i=0; i<all_dimension_holders[0].roots.size(); i++)
    {
      for (int j=0; j<all_dimension_holders[1].roots.size(); j++)
      {
	for (int k=0; k<all_dimension_holders[0].roots.size(); k++)
	{
	  for (int l=0; l<all_dimension_holders[1].roots.size(); l++)
	  {
	    for (int m=0; m<all_dimension_holders[0].roots.size(); m++)
	    {
	      for (int n=0; n<all_dimension_holders[1].roots.size(); n++)
	      {

		std::vector<double>
		  constantsi = all_dimension_holders[0].constants[i];
		double rooti = all_dimension_holders[0].roots[i];
		std::vector<double>
		  constantsj = all_dimension_holders[1].constants[j];
		double rootj = all_dimension_holders[1].roots[j];
		std::vector<double>
		  constantsk = all_dimension_holders[0].constants[k];
		double rootk = all_dimension_holders[0].roots[k];
		std::vector<double>
		  constantsl = all_dimension_holders[1].constants[l];
		double rootl = all_dimension_holders[1].roots[l];
		std::vector<double>
		  constantsm = all_dimension_holders[0].constants[m];
		double rootm = all_dimension_holders[0].roots[m];
		std::vector<double>
		  constantsn = all_dimension_holders[1].constants[n];
		double rootn = all_dimension_holders[1].roots[n];

		kl3D::Form_KL eigenvector_integral(_mesh, std::make_shared<EigenFunction3D>(constantsi, rooti, constantsj, rootj, constantsk, rootk), std::make_shared<EigenFunction3D>(constantsl, rootl, constantsm, rootm, constantsn, rootn));

		assemble(s, eigenvector_integral);
		integral = s.get_scalar_value();

		msg = "phi_" + std::to_string(i) + "*" + std::to_string(j)
		  + "*" + std::to_string(k)
		  + " phi_" + std::to_string(l) + "*" + std::to_string(m)
		  + "*" + std::to_string(n)
		  + ": " + std::to_string(integral);
		pods_log(0, msg);
	      }
	    }
	  }
	}
      }
    }
  }

  return;
}
//------------------------------------------------------------------------------
