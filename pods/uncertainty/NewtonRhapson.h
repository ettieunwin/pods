// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-06-21
// Last changed: 2017-06-21

#ifndef NEWTONRHAPSON_H
#define NEWTONRHAPSON_H

#include <vector>
#include <functional>
#include "FunctionHolder.h"
namespace pods
{
  class NewtonRhapson
  {
  public:
    NewtonRhapson(FunctionHolder function_holder, int num_roots,
		  std::vector<double> initial_guess,
		  double period, double error= 0.001);

    ~NewtonRhapson(){};

    std::vector<double> get_roots();
    
  private:
    double _function(std::function<double(double)> function, double x);

    double _function_prime(std::function<double(double)> function_derivative,
			   double x);

    double _num_roots, _period, _error;

    std::vector<double> _initial_guess;

    FunctionHolder _function_holder;
  };
}
#endif
