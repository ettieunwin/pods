// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// The logger is based on the FEniCS libraries logger, see
// <https://fenicsproject.org>.
//
// First added:  2017-04-06
// Last changed: 2017-04-06

#include <dolfin.h>
#include <assert.h>
#include <cmath>
#include <fstream>
#include <string>
#include <memory>
#include "kl1D.h"
#include "kl2D.h"
#include "ExponentialTruncatedKL.h"

#include "../log/PodsLog.h"
#include "FunctionHolder.h"
#include "NewtonRhapson.h"

using namespace dolfin;
using namespace pods;
//-----------------------------------------------------------------------------
// Exponential
class Exponential : public Expression
{
public:

  Exponential() : Expression() {}

  void eval(Array<double>& values, const Array<double>& x) const
  {
    Array<double> data(1);
    u->eval(data, x);
    const double u_val= data[0];

    values[0] = exp(u_val);
  }

  std::shared_ptr<const Function> u;
};
//------------------------------------------------------------------------------
ExponentialTruncatedKL::ExponentialTruncatedKL(std::shared_ptr<const dolfin::Mesh> mesh,
					       ProbabilitySpace& omega,
					       double mu, double sigma,
					       std::vector<double> lambda,
					       int truncation_number,
					       bool debug)
  :  _mesh(mesh), _omega(omega), _lambda(lambda),
     _truncation_number(truncation_number), _debug(debug)
{

  int dim = mesh->geometry().dim();
  std::shared_ptr<FunctionSpace> V;
  if (dim == 1)
    V = std::make_shared<kl1D::FunctionSpace>(mesh);
  else if (dim == 2)
    V = std::make_shared<kl2D::FunctionSpace>(mesh);

  _V = V;

  // Need to scale mean and standard deviation to new values
  double m = mu;
  double s = sigma;

  double mu_l = log(m/(double) sqrt(1 + pow(s,2)/ (double) pow(m,2)));
  double sigma_l = sqrt(log(1+ pow(s,2)/(double) pow(m,2)));

  _mu = mu_l;
  _sigma = sigma_l;
}
//------------------------------------------------------------------------------
std::shared_ptr<Function> ExponentialTruncatedKL::sample()
{
  pods_log(20, "Exponential Truncated KL: Resampling");
  Function exponential_random_field(_V);
  // Sample the TruncatedKL expansion
  TruncatedKL kl = TruncatedKL(_mesh, _omega, _mu, _sigma, _lambda,
			       _truncation_number,
			       _debug);
  std::shared_ptr<Function> random_field = kl.sample();
  pods_log(20, "Exponential Truncated KL: Computed Analytic Truncated KL ");

  auto exp = std::make_shared<Exponential>();
  exp->u = random_field;
  exponential_random_field.interpolate(*exp);

  return std::make_shared<Function>(exponential_random_field);

}
//---------------------------------------------------------------------------
