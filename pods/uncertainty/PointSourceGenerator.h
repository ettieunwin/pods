// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-03-29
// Last changed: 2017-03-29

#ifndef POINTSOURCEGENERATOR_H
#define POINTSOURCEGENERATOR_H

#include "StochasticFunction.h"
#include "ProbabilitySpace.h"
#include <vector>
#include <dolfin.h>

namespace pods
{
  // Create a point source information holder which can generate from constructor to
  // make implementation nicer.

  class PointSourceGenerator: public StochasticFunction<std::vector<std::pair<dolfin::Point, double>>>
  /// This class enables random fields to be generated using point sources
  /// which are applied at random locations and with random magnitudes.
  {
  public:
    // Constructs point source generator. Requires inputs of number of
    // point sources to apply, mean of the variable the random field is
    // added to, a scaling parameter for the maximum magnitude of the
    // point source and a probability space.
    PointSourceGenerator(ProbabilitySpace Omega,
			 MPI_Comm child_comm,
			 int num_ps, double mean,
			 double scaling,
			 std::vector<double> mesh_dimensions):
      _Omega(Omega), _child_comm(child_comm),
      _num_ps(num_ps), _mean(mean), _scaling(scaling),
      _mesh_dimensions(mesh_dimensions)
    {
    }

    // Destructs point source generator
    ~PointSourceGenerator()
    {
    }

    // Generates a sample of point sources.
    std::vector<std::pair<dolfin::Point, double>> sample();

  private:
    // Probability space
    ProbabilitySpace _Omega;

    // Child communicator
    MPI_Comm _child_comm;

    // Number of point sources
    int _num_ps;

    // Mean of random field
    double _mean;

    // Magnitude scaling
    double _scaling;

    // Mesh scaling
    std::vector<double> _mesh_dimensions;
  };
}

#endif
