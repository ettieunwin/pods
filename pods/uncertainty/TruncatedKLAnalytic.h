// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-04-20
// Last changed: 2017-04-20

#ifndef TRUNCATEDKLANALYTIC
#define TRUNCATEDKLANALYTIC

#include <dolfin.h>
#include "FunctionHolder.h"
#include "StochasticFunction.h"

using namespace dolfin;
namespace pods
{

  class EigenFunction1D : public Expression
  {
    /// This class results in an expression for the
    /// analytic 1D eigenfunctions used in the analytic
    /// truncated KL expansion.
  public:
    EigenFunction1D(){}

    EigenFunction1D(std::vector<double> constants, double root)
      : _A(constants[0]), _B(constants[1]), _root(root) {}

    void eval(Array<double>& values, const Array<double>& x) const
    {
      values[0] = _A*cos(_root*x[0]) + _B*sin(_root*x[0]);
    }

  private:
    double _A, _B, _root;
  };

  class EigenFunction2D : public Expression
  {
    /// This class results in an expression for the
    /// analytic 2D eigenfunctions used in the analytic
    /// truncated KL expansion.
  public:
    EigenFunction2D(){}

    EigenFunction2D(std::vector<double> constantsx, double rootx,
		    std::vector<double> constantsy, double rooty)
      : _Ax(constantsx[0]), _Bx(constantsx[1]), _rootx(rootx),
        _Ay(constantsy[0]), _By(constantsy[1]), _rooty(rooty){}

    void eval(Array<double>& values, const Array<double>& x) const
    {
      values[0] = (_Ax*cos(_rootx*x[0]) + _Bx*sin(_rootx*x[0]))*
	(_Ay*cos(_rooty*x[1]) + _By*sin(_rooty*x[1]));
    }

  private:
    double _Ax, _Bx, _rootx, _Ay, _By, _rooty;
  };

  class EigenFunction3D : public Expression
  {
    /// This class results in an expression for the
    /// analytic 3D eigenfunctions used in the analytic
    /// truncated KL expansion.
  public:
    EigenFunction3D(){}

    EigenFunction3D(std::vector<double> constantsx, double rootx,
		    std::vector<double> constantsy, double rooty,
		    std::vector<double> constantsz, double rootz)
      : _Ax(constantsx[0]), _Bx(constantsx[1]), _rootx(rootx),
      _Ay(constantsy[0]), _By(constantsy[1]), _rooty(rooty),
      _Az(constantsz[0]), _Bz(constantsz[1]), _rootz(rootz)
    {}

    void eval(Array<double>& values, const Array<double>& x) const
    {
      values[0] = (_Ax*cos(_rootx*x[0]) + _Bx*sin(_rootx*x[0]))*
	(_Ay*cos(_rooty*x[1]) + _By*sin(_rooty*x[1]))*
	(_Az*cos(_rootz*x[2]) + _Bz*sin(_rootz*x[2]));
    }

  private:
    double _Ax, _Bx, _rootx, _Ay, _By, _rooty, _Az, _Bz, _rootz;
  };

  struct EigenFunction
  {
    pods::EigenFunction1D one_D;
    pods::EigenFunction2D two_D;
    pods::EigenFunction3D three_D;
  };

  struct EigenPair
  {
    EigenFunction eigenfunction;
    double eigenvalue;
  };

  struct Roots
  {
    std::vector<double> odd_roots;
    std::vector<double> even_roots;
  };

  struct NRInfo
  {
    /// Structure containing all the information the Newton Rhaspen
    /// solver needs to find roots of transcendental equations.
    // Domain [-b, a]
    double b;
    double a;
    // Length scale parameter for covariance kernel c(x,y) = e^{|x-y|/lambda}
    double lambda;
    // Odd roots: f(x) = lambda^{-1} - x*tan(a*x)
    std::vector<double> initial_guesses_odd;
    // Even roots: f(x) = lambda^{-1}tan(a*x) + x
    std::vector<double> initial_guesses_even;
    // Other roots: f(x) = tan(a*x) - (2*lambda*x)/(lambda^{2}*x^{2}-1)
    std::vector<double> initial_guesses;
  };

  struct DimensionInfo
  {
    std::vector<double> roots;
    std::vector<std::vector<double>> constants;
    double lambda;
  };

  class TruncatedKLAnalytic :
    public StochasticFunction<std::shared_ptr<dolfin::Function>>
  {
    /// This class enables a random field to be sampled
    /// from a probabilty space analytically using a
    /// truncated KL expansion with a separable exponential
    /// covariance kernel.  Only possible for domains which
    /// are made from straight lines, e.g. straight interval,
    /// rectangle or cuboid.
  public:
    // Constructor
    TruncatedKLAnalytic(std::shared_ptr<const dolfin::Mesh> mesh,
 			ProbabilitySpace& omega,
			double mu, double sigma,
			int truncation_number,
 			std::vector<NRInfo> nr_info,
			bool debug=false);

    // Destructor
    ~TruncatedKLAnalytic() {};

    // Samples the probability space to generate a random field realisation
    std::shared_ptr<dolfin::Function> sample();

    // Public functions so can check found all roots and make initial guesses
    // Computes roots from the two transcendental equations for D=[-b,b]
    // Lord et al
    Roots compute_roots_two(NRInfo nr_info);

    // Computers roots for the one transcendental equation for D=[0,b]
    // Cliffe et al
    std::vector<double> compute_roots_one(NRInfo nr_info);

    // Calculated analytic eigenpairs.
    std::vector<EigenPair> calculate_eigen_pairs(MPI_Comm comm);

  private:
    // Calculate constants for eigenfunctions and roots of transcendental
    // equations
    std::vector<DimensionInfo> _calculate_constants_and_roots();

    // Calculates constants for eigenfunctions from odd roots
    std::vector<double> _calculate_As(std::vector<double> roots, double a);

    // Calculates constants for eigenfunctions from even roots.
    std::vector<double> _calculate_Bs(std::vector<double> roots, double a);

    // Calculates constants for eigenfunctions from even roots.
    std::vector<double> _calculate_Cs(std::vector<double> roots, double a,
				      double lambda);

    // Creates FunctionHolder for NewtonRhapson for odd roots
    // Lord et al
    FunctionHolder _create_function_holder_odd(double a, double lambda);

    // Creates FunctionHolder for NewtonRhapson for even roots
    // Lord et al
    FunctionHolder _create_function_holder_even(double a, double lambda);

    // Creates FunctionHolder for NewtonRhapson for all roots
    // Cliffe et al
    FunctionHolder _create_function_holder(double a, double lambda);

    // Calculates eigenvalues
    std::vector<double> _calculate_eigenvalues(std::vector<DimensionInfo> all_dim_holder);

    // Calculates eigenfunctions
    std::vector<EigenFunction>
      _calculate_eigenfunctions(std::vector<DimensionInfo> all_dimension_holders);

    // Sort eigenpairs into decending order of eigenvalue
    void _sort_eigen_pairs(std::vector<EigenPair>& eigen_pairs);
    std::vector<size_t> _sort_indexes(std::vector<double> v);

    // Checks orthonormal properties of eigenfunctions
    void _check_l2_norm(MPI_Comm comm,
			std::vector<DimensionInfo> all_dimension_holders);

    // Meshes
    std::shared_ptr<const dolfin::Mesh> _mesh;

    // Probability space
    ProbabilitySpace& _omega;

    // Constants
    int _truncation_number, _truncation_number_all, _dim,
      _rank, _size, _file_no;
    double _mu, _sigma;

    // Vectors of initial guesses
    std::vector<NRInfo> _nr_info;

    // Function space
    std::shared_ptr<dolfin::FunctionSpace> _V_all, _V;

    // Debug switch
    bool _debug;

    // Communicator
    MPI_Comm _comm;
  };

}


#endif
