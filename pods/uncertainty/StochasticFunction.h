// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-04-25
// Last changed: 2017-04-25

#ifndef STOCHASTICFUNCTION_H
#define STOCHASTICFUNCTION_H

#include <vector>
#include <dolfin.h>
#include "ProbabilitySpace.h"

namespace pods
{
  template <typename SampleClass>
  class StochasticFunction
  {    
  public:
    // Destructor
    ~StochasticFunction()
    {
    };
    
    // Sample to get a realisation
    virtual SampleClass sample() = 0;
  };
}

#endif
