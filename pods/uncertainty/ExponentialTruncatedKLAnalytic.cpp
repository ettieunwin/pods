// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// The logger is based on the FEniCS libraries logger, see
// <https://fenicsproject.org>.
//
// First added:  2017-04-06
// Last changed: 2017-04-06

#include <dolfin.h>
#include <assert.h>
#include <cmath>
#include <fstream>
#include <string>
#include <memory>
#include "kl1D.h"
#include "kl2D.h"
#include "kl3D.h"
#include "ExponentialTruncatedKLAnalytic.h"

#include "../log/PodsLog.h"
#include "FunctionHolder.h"
#include "NewtonRhapson.h"

using namespace dolfin;
using namespace pods;
//-----------------------------------------------------------------------------
// Exponential
class Exponential : public Expression
{
public:

  Exponential() : Expression() {}

  void eval(Array<double>& values, const Array<double>& x) const
  {
    Array<double> data(1);
    u->eval(data, x);
    const double u_val= data[0];
    values[0] = exp(u_val);
  }

  std::shared_ptr<const Function> u;
};
//------------------------------------------------------------------------------
ExponentialTruncatedKLAnalytic::ExponentialTruncatedKLAnalytic(std::shared_ptr<const dolfin::Mesh> mesh,
					 ProbabilitySpace& omega,
					 double mu, double sigma,
					 int truncation_number,
					 std::vector<NRInfo> nr_info,
					 bool debug)
  : _mesh_full(mesh), _omega(omega), _truncation_number(truncation_number),
     _nr_info(nr_info), _debug(debug)
{
  // Gets group communicator information
  MPI_Comm comm=mesh->mpi_comm();
  _comm = comm;
  int rank, size;
  MPI_Comm_rank(_comm, &rank);
  MPI_Comm_size(_comm, &size);
  _size = size;
  _rank = rank;

  // To avoid file writing/ reading conflicts
  int file_no;
  if (rank == 0)
    MPI_Comm_rank(MPI_COMM_WORLD, &file_no);
  MPI_Bcast(&file_no, 1, MPI_INT, 0, _comm);
  _file_no = file_no;

  int _dim = mesh->geometry().dim();

// Creates function space for all the communicators together
  std::shared_ptr<FunctionSpace> V_all;
  if (_dim == 1)
    V_all = std::make_shared<kl1D::FunctionSpace>(mesh);
  else if (_dim == 2)
    V_all = std::make_shared<kl2D::FunctionSpace>(mesh);
  else if (_dim == 3)
    V_all = std::make_shared<kl3D::FunctionSpace>(mesh);

  // If size is equal to 1, mesh and function space are already only
  // on zeroth processor
  if (_size == 1)
  {
    // Assigns objects for just zeroth processor
    _V = V_all;
    _mesh = mesh;
  }
  else
  {
    pods_log(20, "Analytic Truncated KL: Finding random field on zeroth ranked processor");

    HDF5File tmp_mesh(_comm,
		      "tmp_mesh"+std::to_string(_file_no)+".h5", "w");
    tmp_mesh.write(*mesh, "mesh");
    tmp_mesh.close();

    if (_rank == 0)
    {
      pods_log(0, "Analytic Truncated KL: Reading mesh in on zeroth ranked processor");
      // All processors write to mesh - has to be to HDF5!!
      std::shared_ptr<Mesh> new_mesh = std::make_shared<Mesh>(MPI_COMM_SELF);
      HDF5File tmp_mesh_read(MPI_COMM_SELF,
			     "tmp_mesh"+std::to_string(_file_no)+".h5",
			     "r");
      tmp_mesh_read.read(*new_mesh, "mesh", false);
      tmp_mesh_read.close();
      _mesh = new_mesh;

      if (_dim == 1)
	_V = std::make_shared<kl1D::FunctionSpace>(new_mesh);
      else if (_dim == 2)
	_V = std::make_shared<kl2D::FunctionSpace>(new_mesh);
      else if (_dim == 3)
	_V = std::make_shared<kl2D::FunctionSpace>(new_mesh);
    }
  }

  _V_all = V_all;

  // Barrier to make sure that the zeroth processor has finished
  MPI_Barrier(_comm);

  // Need to scale mean and standard deviation to new values
  double m = mu;
  double s = sigma;

  //double mu_l = 2*log(m) - 0.5*log(pow(s,2) + pow(m,2));
  //double sigma_l = -2*log(m) + log(pow(s,2) + pow(m,2));

  _mu = mu;
  _sigma = sigma;
}
//------------------------------------------------------------------------------
std::shared_ptr<Function> ExponentialTruncatedKLAnalytic::sample()
{
  pods_log(20, "Analytic Exponential Truncated KL: Resampling");
  Function exponential_random_field(_V_all);
  // Sample the TruncatedKL expansion
  TruncatedKLAnalytic kl = TruncatedKLAnalytic(_mesh_full, _omega, _mu, _sigma,
					       _truncation_number,
					       _nr_info, _debug);
  std::shared_ptr<Function> random_field = kl.sample();

  pods_log(20, "Analytic Exponential Truncated KL: Got truncated random field");

  if (_size > 1)
  {
    // If many processors, write from all
    HDF5File field(_comm, "field_" + std::to_string(_file_no) + ".h5",
		   "w");
    field.write(*random_field, "field");
    field.close();
  }

  if (_rank == 0)
  {
    pods_log(20, "Analytic Exponential Truncated KL: Computing Exponential Analytic Truncated KL on rank 0");

    std::shared_ptr<Function>
      tmp_random_field = std::make_shared<Function>(_V);
    Function tmp_exponential_random_field(_V);

    // If many processors, read in function
    if (_size > 1)
    {
      pods_log(0, "Analytic Exponential Truncated KL: More then one processor, need to read in file");
      HDF5File field_(MPI_COMM_SELF,
		      "field_" + std::to_string(_file_no) + ".h5",
		      "r");
      field_.read(*tmp_random_field, "field");
      field_.close();
    }
    else
    {
      tmp_random_field = random_field;
      pods_log(0, "Analytic Exponential Truncated KL: Only one processor");
    }

    // Calculate exponential
    auto exp = std::make_shared<Exponential>();
    exp->u = tmp_random_field;
    tmp_exponential_random_field.interpolate(*exp);

    pods_log(0, "Analytic Exponential Truncated KL: Calculated exponential");

    if (_size > 1)
    {
      pods_log(0, "Analytic Exponential Truncated KL: More than one processor, saving field");
      // If running in parallel save
      HDF5File exp_field(MPI_COMM_SELF,
			 "exp_field_" + std::to_string(_file_no) + ".h5",
			 "w");
      exp_field.write(tmp_exponential_random_field, "field");
      exp_field.close();
    }
    else
    {
      exponential_random_field = tmp_exponential_random_field;
    }
  }

  MPI_Barrier(_comm);

  if (_size > 1)
  {
    pods_log(0, "Analytic Exponential Truncated KL: Reading in file on all processors");
    // If running in parallel read
    HDF5File exp_field(_comm,
		       "exp_field_" + std::to_string(_file_no) + ".h5",
		       "r");
    exp_field.read(exponential_random_field, "field");
    exp_field.close();
  }

  return std::make_shared<Function>(exponential_random_field);
}
//---------------------------------------------------------------------------
