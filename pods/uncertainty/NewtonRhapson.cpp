// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-06-21
// Last changed: 2017-06-21

#include "NewtonRhapson.h"
#include<iostream>
#include<cmath>
#include<iomanip>
#include<functional>

using namespace pods;

//----------------------------------------------------------------------------
NewtonRhapson::NewtonRhapson(FunctionHolder function_holder,
			     int num_roots,
			     std::vector<double> initial_guess,
			     double period, double error)
  : _function_holder(function_holder),  _num_roots(num_roots),
    _initial_guess(initial_guess), _period(period), _error(error)
{
}
//----------------------------------------------------------------------------
std::vector<double> NewtonRhapson::get_roots()
{
  double x,x1,e,fx,fx1;
  std::vector<double> roots(_num_roots);

  int num_initial_guesses = _initial_guess.size();
  for (int i=0; i<_num_roots; ++i)
  {
    if (i<num_initial_guesses)
      x1 = _initial_guess[i];
    else
      x1 = x1 + _period;
            
    do            
    {
      x = x1;          
      fx = _function(_function_holder.get_function(), x);
      fx1 = _function_prime(_function_holder.get_derivative_function(), x);
      x1 = x - (fx/fx1);        
    } while (fabs(x1-x)>=_error);

    roots[i] = x1;
  }
  return roots;
}
//----------------------------------------------------------------------------- 
double NewtonRhapson::_function(std::function<double(double)> function,
				double x)  
{
  return function(x);
}
//----------------------------------------------------------------------------- 
double NewtonRhapson::_function_prime(std::function<double(double)>
				      function_derivative,
				      double x)
{
  return function_derivative(x);
}
//----------------------------------------------------------------------------
