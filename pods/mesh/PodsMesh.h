// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-06-01
// Last changed: 2017-06-02

#ifndef PODSMESH_H
#define PODSMESH_H

#include <dolfin.h>
#include "../log/PodsLog.h"

namespace pods
{
  class PodsMesh
  // This class holds the meshes required for the MLMC and MC routines. 3
  // meshes are necessary, the initial mesh if using the LP smoothing method
  // for uncertainty since this ensures that the random field can always
  // be represented on the coarsest mesh, the coarse mesh and the fine mesh.
  // Methods are also present for reading in meshes from file or generated
  // using refine from an initial mesh.
  {
  public:

    // Constructor
    PodsMesh()
    {
    };

    // Destructor
    ~PodsMesh()
    {
    };

    // Sets the intial mesh
    void set_initial_mesh(std::shared_ptr<const dolfin::Mesh> initial_mesh)
    {
      _initial_mesh = initial_mesh;
    }

    // Sets the coarse mesh
    void set_coarse_mesh(std::shared_ptr<const dolfin::Mesh> coarse_mesh)
    {
      _coarse_mesh = coarse_mesh;
    }

    // Sets the fine mesh
    void set_fine_mesh(std::shared_ptr<const dolfin::Mesh> fine_mesh)
    {
      _fine_mesh = fine_mesh;
    }

    // Gets the fine mesh
    std::shared_ptr<const dolfin::Mesh> get_fine_mesh()
    {
      return _fine_mesh;
    }

    // Gets the coarse mesh
    std::shared_ptr<const dolfin::Mesh> get_coarse_mesh()
    {
      return _coarse_mesh;
    }

    // Gets the initial mesh
    std::shared_ptr<const dolfin::Mesh> get_initial_mesh()
    {
      return _initial_mesh;
    }

    // Create a PodsMesh using refine.
    static void create_meshes(PodsMesh& pods_mesh,
			      std::shared_ptr<const dolfin::Mesh> initial_mesh,
			      MPI_Comm mesh_comm,
			      int level);

    // Creats a PodsMesh by reading in files.
    static void read_meshes(PodsMesh& pods_mesh,
			    std::vector<std::string> meshes,
			    MPI_Comm mesh_comm, int level);

  private:
    std::shared_ptr<const dolfin::Mesh> _initial_mesh;
    std::shared_ptr<const dolfin::Mesh> _coarse_mesh;
    std::shared_ptr<const dolfin::Mesh> _fine_mesh;
  };
}
#endif
