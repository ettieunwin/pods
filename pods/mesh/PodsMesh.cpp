// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-06-01
// Last changed: 2017-06-02

#include <dolfin.h>
#include "PodsMesh.h"
#include "MeshGenerator.h"
#include "../log/PodsLog.h"

using namespace pods;
//-----------------------------------------------------------------------------
void PodsMesh::create_meshes(PodsMesh& pods_mesh,
			     std::shared_ptr<const dolfin::Mesh> initial_mesh,
			     MPI_Comm mesh_comm,
			     int level)
{
  // Sets initial mesh
  pods_mesh.set_initial_mesh(initial_mesh);

  // Generate meshes by refining initial mesh
  MeshGenerator mesh_generator = MeshGenerator(initial_mesh,level);

  // Sets fine mesh of PodsMesh to be fine mesh created by generator.
  pods_mesh.set_fine_mesh(mesh_generator.get_fine_mesh());
  pods_log(20, "generated fine mesh");

  if (level > 0)
  {
    // If not on level zero, coarse mesh is coarse mesh created by generator.
    pods_mesh.set_coarse_mesh(mesh_generator.get_coarse_mesh());
    pods_log(20, "generated coarse mesh");
  }
  else
    // If level zero coasrse mesh and fine mesh are the same.
    pods_mesh.set_coarse_mesh(pods_mesh.get_fine_mesh());

  return;
 }
//-----------------------------------------------------------------------------
void PodsMesh::read_meshes(PodsMesh& pods_mesh, std::vector<std::string> meshes,
			   MPI_Comm mesh_comm, int level)
{
  int number_meshes = meshes.size();
  if (number_meshes == 0)
  {
    pods_error("PodsMesh.cpp",
	       "reading meshes from files.",
	       "not provided any meshes");
  }

  // Reads in and sets initial mesh
  std::shared_ptr<dolfin::Mesh> initial_mesh
    = std::make_shared<dolfin::Mesh>(mesh_comm);
  dolfin::XDMFFile initial(mesh_comm, meshes[0]);
  initial.read(*initial_mesh);
  pods_mesh.set_initial_mesh(initial_mesh);
  pods_log(20, "reading in initial mesh");
  if (level < number_meshes)
  {
    // Reads in other meshes if they exist
    if (level > 0)
    {
      // Read in and set fine mesh
      std::shared_ptr<dolfin::Mesh>
	fine_mesh = std::make_shared<dolfin::Mesh>(mesh_comm);
      dolfin::XDMFFile fine(mesh_comm, meshes[level]);
      fine.read(*fine_mesh);
      pods_mesh.set_fine_mesh(fine_mesh);
      pods_log(20, "read in fine mesh");

      // Read in and set coarse mesh
      std::shared_ptr<dolfin::Mesh>
	coarse_mesh = std::make_shared<dolfin::Mesh>(mesh_comm);
      dolfin::XDMFFile coarse(mesh_comm, meshes[level-1]);
      coarse.read(*coarse_mesh);
      pods_mesh.set_coarse_mesh(coarse_mesh);
      pods_log(20, "read in coarse mesh");
    }
    else
    {
      // All meshes are the same for level 0
      pods_mesh.set_fine_mesh(initial_mesh);
      pods_log(20, "read in fine mesh");
      pods_mesh.set_coarse_mesh(initial_mesh);
      pods_log(20, "read in coarse mesh");
    }
  }
  else
  {
    // Not enough meshes so have to refine.
    // Reads in finest mesh as start for refinement
    std::shared_ptr<dolfin::Mesh>
      finest_mesh = std::make_shared<dolfin::Mesh>(mesh_comm);
    dolfin::XDMFFile finest(mesh_comm, meshes[number_meshes-1]);
    finest.read(*finest_mesh);

    pods_log(20, "not enough meshes provided - using refine");
    MeshGenerator mesh_generator
      = MeshGenerator(finest_mesh, level+1-number_meshes);
    pods_mesh.set_fine_mesh(mesh_generator.get_fine_mesh());
    pods_mesh.set_coarse_mesh(mesh_generator.get_coarse_mesh());
  }
  return;
}
//-----------------------------------------------------------------------------
