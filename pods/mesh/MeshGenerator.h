// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-03-30
// Last changed: 2017-03-30

#ifndef MESHGENERATOR_H
#define MESHGENERATOR_H

#include <dolfin.h>
#include "../log/PodsLog.h"

namespace pods
{
  class MeshGenerator
  /// This class generates shared pointers to constant meshes for different
  /// levels of refinement.  The fine mesh is level 'level' and the coarse
  /// mesh is level 'level-1'.  It is constructed with a shared pointer
  /// to a constant mesh and level.
  {
  public:
    // Mesh constructor that takes in a shared pointer to a constant mesh
    // and the level of the fine mesh.
    MeshGenerator(std::shared_ptr<const dolfin::Mesh> initial_mesh, int level);

    // Destructor
    ~MeshGenerator()
    {
    };

    // Generates shared pointer to the constant fine mesh.  If level = 0, throws
    // an error since level = -1 does not exist.
    std::shared_ptr<const dolfin::Mesh> get_coarse_mesh()
    {
      if (_level == 0)
      {
        pods_error("MeshGenerator.h", "getting coarse mesh",
                   "There is no coarse mesh for level 0");
      }
      return _coarse_mesh;
    };

    // Generates a shared pointer to the constant coarse mesh.
    std::shared_ptr<const dolfin::Mesh> get_fine_mesh()
    {
      return _fine_mesh;
    };

  private:
    // Level of fine mesh
    int _level;

    // Coarse mesh
    std::shared_ptr<const dolfin::Mesh> _coarse_mesh;

    // Fine mesh
    std::shared_ptr<const dolfin::Mesh> _fine_mesh;
  };
}
#endif
