// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-03-30
// Last changed: 2017-03-30

#include <dolfin.h>
#include "MeshGenerator.h"
#include "../log/PodsLog.h"

using namespace pods;
//-----------------------------------------------------------------------------
MeshGenerator::MeshGenerator(std::shared_ptr<const dolfin::Mesh> initial_mesh,
			     int level)
  : _level(level)
{
  _coarse_mesh = initial_mesh;
  std::shared_ptr<const dolfin::Mesh> mesh = initial_mesh;
  // Gets communicator from inital mesh to ensure future meshes have
  // same communicator.
  MPI_Comm comm = initial_mesh->mpi_comm();
  // Makes temporary meshes to be used in refinement.
  std::shared_ptr<dolfin::Mesh>
    tmp_mesh_coarse= std::make_shared<dolfin::Mesh>(comm);
  std::shared_ptr<dolfin::Mesh>
    tmp_mesh_fine = std::make_shared<dolfin::Mesh>(comm);

  if (level == 0)
  {
    _fine_mesh = initial_mesh;
  }
  else
  {
    // Refinement depending on level of l
    dolfin::refine(*tmp_mesh_fine, *_coarse_mesh);
    *tmp_mesh_coarse = *tmp_mesh_fine;
    for(int l=1; l<level; ++l)
    {
      refine(*tmp_mesh_fine, *tmp_mesh_coarse);
      if (l != level-1)
      {
        *tmp_mesh_coarse = *tmp_mesh_fine;
      }
    }
    if (level>1)
    {
      // Stores coarse mesh if changed
      _coarse_mesh = tmp_mesh_coarse;
    }
    // Stores fine mesh
    _fine_mesh = tmp_mesh_fine;
  }

  // Logs mesh topology and dof information
  //std::string msg = "Fine mesh: (d, dim): ";
  //for (int d=0; d<=_fine_mesh->topology().dim(); ++d)
  //{
  //  msg += " (" + std::to_string(d) +
  //         ", " + std::to_string(_fine_mesh->num_entities_global(d)) + "), ";
  //}
  //pods_log(20, msg, {_fine_mesh->mpi_comm()});

  //msg = "Coarse mesh: (d, dim): ";
  //for (int d=0; d<=_coarse_mesh->topology().dim(); ++d)
  //{
  //  msg += " (" + std::to_string(d) + ", "
  //         + std::to_string(_coarse_mesh->num_entities_global(d)) + "), ";
  //}
  //pods_log(20, msg, {_coarse_mesh->mpi_comm()});

};
//-----------------------------------------------------------------------------
