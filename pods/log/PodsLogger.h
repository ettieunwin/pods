// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// The logger is based on the FEniCS libraries logger, see
// <https://fenicsproject.org>.
//
// First added:  2017-03-30
// Last changed: 2017-03-30

#ifndef __PODS_LOGGER_H
#define __PODS_LOGGER_H

#include <iostream>
#include <string>
#include <mpi.h>

namespace pods
{

  class PodsLogger
  /// This is the logger for the PODS code.  For log levels see
  /// included text file.
  {
  public:

    // Constructor
    PodsLogger();

    // Destructor
    ~PodsLogger();

    // Set log level
    void set_log_level(int log_level, std::string processors_to_print="one");

    // Return true iff logging is active
    inline bool is_active()
    {
      return _active;
    }

    // Logs a message
    void pods_log(std::string msg, int log_level);

    // Error message
    void pods_error(std::string location,
		    std::string task,
		    std::string reason) const;

  private:
    // Current log level
    int _log_level;

    // True iff logging is active
    bool _active;

    // MPI_Comm
    MPI_Comm _mpi_comm;

    // Which processors to print on
    std::string _processors_to_print;
  };

}

#endif
