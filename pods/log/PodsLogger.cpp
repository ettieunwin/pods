// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// The logger is based on the FEniCS libraries logger, see
// <https://fenicsproject.org>.
//
// First added:  2017-03-30
// Last changed: 2017-03-30

#include "PodsLogger.h"
#include <sstream>
#include <stdexcept>

using namespace pods;
//-----------------------------------------------------------------------------
PodsLogger::PodsLogger()
  : _active(true), _mpi_comm(MPI_COMM_WORLD)
{
}
//-----------------------------------------------------------------------------
PodsLogger::~PodsLogger()
{
}
//-----------------------------------------------------------------------------
void PodsLogger::set_log_level(int log_level, std::string processors_to_print)
{
  _log_level = log_level;
  _processors_to_print = processors_to_print;
}
//-----------------------------------------------------------------------------
void PodsLogger::pods_log(std::string msg, int log_level)
{
  int rank;
  MPI_Comm_rank(_mpi_comm, &rank);
  int num_p;
  MPI_Comm_size(_mpi_comm, &num_p);

  if (_log_level <= log_level)
  {
    if (_processors_to_print == "all")
    {
      if (num_p == 1)
	std::cout << msg << std::endl;
      else
	std::cout << "Process " << rank << ": " << msg << std::endl;
    }
    if (_processors_to_print == "one")
    {
      if (num_p == 1)
	std::cout << msg << std::endl;
      else
      {
	if (rank == 0)
	  std::cout << "Process " << rank << ": " << msg << std::endl;
      }
    }
  }
}
//-----------------------------------------------------------------------------
void PodsLogger::pods_error(std::string location,
                          std::string task,
                          std::string reason) const
{
  int rank;
  MPI_Comm_rank(_mpi_comm, &rank);

  std::stringstream s;
  s << std::endl << std::endl
    << "*** "
    << "-------------------------------------------------------------------------"
    << std::endl
    << "*** " << "Error:   Unable to " << task << "." << std::endl
    << "*** " << "Reason:  " << reason << "." << std::endl
    << "*** " << "Where:   This error was encountered inside " << location << "."
    << std::endl
    << "*** " << "Process: " << rank << std::endl
    << "*** " << std::endl
    << "-------------------------------------------------------------------------"
    << std::endl;

  throw std::runtime_error(s.str());
}
//-----------------------------------------------------------------------------
