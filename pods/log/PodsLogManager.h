// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// The logger is based on the FEniCS libraries logger, see
// <https://fenicsproject.org>.
//
// First added:  2017-03-30
// Last changed: 2017-03-30

#ifndef __PODs_LOG_MANAGER_H
#define __PODs_LOG_MANAGER_H

#include "PodsLogger.h"

namespace pods
{

  class PodsLogManager
  /// This is the class that manages the PODS logger and ensures only
  /// a single instance is generated.
  {
  public:

    // Singleton instance of logger
    static PodsLogger& pods_logger();

  };

}

#endif
