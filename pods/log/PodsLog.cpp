// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// The logger is based on the FEniCS libraries logger, see
// <https://fenicsproject.org>.
//
// First added:  2017-03-30
// Last changed: 2017-03-30

#include "PodsLog.h"
#include "PodsLogManager.h"
#include "../solvers/GenericSolver.h"
#include <fstream>
#include <cassert>

using namespace pods;
//-----------------------------------------------------------------------------
void pods::set_pods_log_level(int log_level)
{
  PodsLogManager::pods_logger().set_log_level(log_level);
}
//-----------------------------------------------------------------------------
void pods::set_pods_log_level(int log_level, std::string processors_to_print)
{
  PodsLogManager::pods_logger().set_log_level(log_level, processors_to_print);
}
//-----------------------------------------------------------------------------
void pods::pods_log(int log_level, std::string msg)
{
  if (!PodsLogManager::pods_logger().is_active())
    return; // optimization
  PodsLogManager::pods_logger().pods_log(msg, log_level);
}
//-----------------------------------------------------------------------------
void pods::pods_log(int log_level, std::string msg,
              const std::vector<MPI_Comm>& comms)
{
  int comm_rank;
  std::string comm_msg = "Split (";
  for (std::size_t j=0; j<comms.size(); ++j)
  {
    MPI_Comm_rank(comms[j], &comm_rank);
    comm_msg += std::to_string(comm_rank) +
      (j == comms.size()-1 ? "): " : ", ");
  }
  std::string whole_message = comm_msg + msg;
  pods_log(log_level, whole_message);
}
//-----------------------------------------------------------------------------
void pods::pods_error(std::string location,
                std::string task,
                std::string reason)
{
  PodsLogManager::pods_logger().pods_error(location, task, reason);
}
//-----------------------------------------------------------------------------
void pods::pods_save_mlmc(double seed_value,
                          std::shared_ptr<GenericSolver> solver_class,
                          std::vector<QOI>& QOI_holder,
                          double tol,
                          std::string save_path)
{
  pods_log(80, "saving");

  // Get information about solver
  solver_info info = solver_class->get_solver_info();
  std::ofstream f;
  std::time_t time = std::time(NULL);

  // Get number of processors
  int rank;
  int num;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &num);

  int num_levels = QOI_holder[0].num_samples.size();
  int num_qois = QOI_holder.size();

  if (rank == 0)
  {
    f.open(save_path + "mlmc_data_" +
	   std::to_string(int(std::abs(std::log10(tol)))) + "_" +
	   std::to_string(time) + ".csv");
    f << "solver," << info.problem << std::endl;
    f << "type,mlmc" << std::endl;
    f << std::fixed << std::setprecision(8) << "seed," << seed_value << std::endl;
    f << "random field," << info.random_field << std::endl;
    f << "initial mesh," << info.mesh << std::endl;
    f << "tolerance," <<  tol << std::endl;
    f << "number of processors," << num << std::endl;
    f << "total time," << QOI_holder[0].total_time << std::endl;

    assert(num_qois == info.qoi.size()
           && "Pre-defined quantites of interests don't match calculated ones");
    f << "QOIs,";
    for (int q=0; q<num_qois; ++q)
      f << info.qoi[q] << ",";
    f << std::endl;

    f << "E[Q],";
    for (int q=0; q<num_qois; ++q)
      f<< std::fixed << std::setprecision(20) << QOI_holder[q].expectation << ",";
    f << std::endl;

    f << "V[Q],";
    for (int q=0; q<num_qois; ++q)
      f << std::fixed << std::setprecision(20) << QOI_holder[q].variance << ",";
    f << std::endl;

    f << "alpha,";
    for (int q=0; q<num_qois; ++q)
      f << std::fixed << std::setprecision(8) << QOI_holder[q].alpha << ",";
    f << std::endl;
    f << "beta,";
    for (int q=0; q<num_qois; ++q)
      f << std::fixed << std::setprecision(8) << QOI_holder[q].beta << ",";
    f << std::endl;
    f << "gamma,";
    for (int q=0; q<num_qois; ++q)
      f << std::fixed << std::setprecision(8) << QOI_holder[q].gamma << ",";
    f << std::endl;

    f << std::endl;
    f << "number of samples,";
    for(std::vector<int>::const_iterator i = QOI_holder[0].num_samples.begin();
	i != QOI_holder[0].num_samples.end(); ++i)
      f << *i << ",";
    f << std::endl;

    for (int q=0; q<num_qois; ++q)
    {
      f << "Ql " << info.qoi[q] << ",";
      for (int l=0; l<num_levels; ++l)
	f << std::fixed << std::setprecision(20) << QOI_holder[q].expectation_per_level[l] << ",";
      f << std::endl;

      f << "Vl " << info.qoi[q] << ",";
      for (int l=0; l<num_levels; ++l)
	f << std::fixed << std::setprecision(30)  << QOI_holder[q].variance_per_level[l] << ",";
      f << std::endl;
    }
    f << "Cl, ";
    for (int i=0; i< num_levels; ++i)
      f << std::fixed << std::setprecision(8) << QOI_holder[0].cost_per_level[i] << ",";
    f << std::endl;

    f << std::endl;
  }
  return;
}
//-----------------------------------------------------------------------------
void pods::pods_save_mc(double seed_value,
                        std::shared_ptr<GenericSolver> solver_class,
                        std::vector<QOI>& QOI_holder, double tol,
                        std::string save_path)
{
  pods_log(80, "Saving");

  // Get information about solver
  solver_info info = solver_class->get_solver_info();
  std::ofstream f;
  std::time_t time = std::time(NULL);

  // Get number of processors
  int total_cores;
  int rank;
  MPI_Comm_size(MPI_COMM_WORLD, &total_cores);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  int num_qois = QOI_holder.size();
  int level = QOI_holder[0].num_samples.size()-1;
  if (rank == 0)
  {
    // FIXME - if not using eigs need to save differently
    f.open(save_path + "mc_data_" +
           std::to_string(int(std::abs(std::log10(tol)))) + "_" +
           std::to_string(time) + ".csv");
    f << "solver," << info.problem << std::endl;
    f << "type,mc" << std::endl;
    f << std::fixed << std::setprecision(8) << "seed," << seed_value << std::endl;
    f << "random field," << info.random_field << std::endl;
    f << "initial mesh," << info.mesh << std::endl;
    f << "tolerance," <<  tol << std::endl;
    f << "number of processors," << total_cores << std::endl;
    f << "total time," << QOI_holder[0].total_time << std::endl;

    assert(num_qois == info.qoi.size()
           && "Pre-defined quantites of interests don't match calculated ones");
    f << "QOIs,";
    for (int q=0; q<num_qois; ++q)
      f << info.qoi[q] << ",";
    f << std::endl;

    f << "E[Q],";
    for (int q=0; q<num_qois; ++q)
      f << std::fixed << std::setprecision(8) << QOI_holder[q].expectation << ",";
    f << std::endl;

    f << "V[Q],";
    for (int q=0; q<num_qois; ++q)
      f << std::fixed << std::setprecision(8) << QOI_holder[q].variance << ",";
    f << std::endl;

    f << std::endl;
    f << "number of samples, " << QOI_holder[0].num_samples[level]
      << std::endl;
    for (int q=0; q<num_qois; ++q)
    {
      f << "Es " << info.qoi[0] << ",";
      for (int n=0; n<QOI_holder[0].num_samples[level]; ++n)
        f << std::fixed << std::setprecision(8) << QOI_holder[q].all_estimations[n] << ",";
      f << std::endl;
    }
  }
  return;
}
//-----------------------------------------------------------------------------
