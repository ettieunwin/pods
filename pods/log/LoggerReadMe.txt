Logger levels:

100 - Critical Print Statements
80 - What I like to have
50 - Some extra information
20 - Sensible debug messages
0 - Anything.

Can't use the logger in the main file since haven't started MPI there.  Maybe
want to look at that in the future.
