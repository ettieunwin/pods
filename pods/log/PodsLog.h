// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// The logger is based on the FEniCS libraries logger, see
// <https://fenicsproject.org>.
//
// First added:  2017-03-30
// Last changed: 2017-03-30

#ifndef __PODSLOG_H
#define __PODSLOG_H

#include <string>
#include <vector>
#include <mpi.h>
#include "../pod/ParallelController.h"
#include "../solvers/GenericSolver.h"

namespace pods
{
  // Set pod log level
  void set_pods_log_level(int log_level);

  // Set pod log level
  void set_pods_log_level(int log_level, std::string processors_to_print);
  
  // Print message at given debug level
  void pods_log(int log_level, std::string msg);

  // Print message at given debug level and any sub communicators used
  void pods_log(int log_level, std::string msg,
                const std::vector<MPI_Comm>& comms);

  // Prints dolfin error
  void pods_error(std::string location,
		  std::string task,
		  std::string reason);

  // Saves the mlmc QOI_holder
  void pods_save_mlmc(double seed_value,
                      std::shared_ptr<GenericSolver> solver_class,
                      std::vector<QOI>& QOI_holder, double tol,
                      std::string save_path);
  // Saves the mc QOI_holder
  void pods_save_mc(double seed_value,
		    std::shared_ptr<GenericSolver> solver_class,
		    std::vector<QOI>& QOI_holder, double tol,
		    std::string save_path);
}

#endif
