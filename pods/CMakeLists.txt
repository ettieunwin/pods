# Require CMake 3.5
cmake_minimum_required(VERSION 3.5)

set(PODS_H pods.h)
install(FILES ${PODS_H} DESTINATION ${PODS_INCLUDE_DIR}
  COMPONENT Development)

#------------------------------------------------------------------------------
# Include directories required to build
include_directories(SYSTEM ${DOLFIN_INCLUDE_DIRS}
  ${DOLFIN_3RD_PARTY_INCLUDE_DIRS})

#------------------------------------------------------------------------------
# PODS source directories

# All files and directories in this directory
file(GLOB PODS_CONTENT RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} *)

# Initialize list
set(PODS_DIRS)

# List of excluded files and directories
set(EXCLUDES ${PODS_H} CMakeFiles cmake_install.cmake CMakeLists.txt Makefile libPODS.so)

# Iterate over all directories and files and append to DOLFIN_DIRS
foreach(_DIR_NAME ${PODS_CONTENT})
  list(FIND EXCLUDES ${_DIR_NAME} INDEX_EXCLUDES)
  if (${INDEX_EXCLUDES} LESS 0)
    list(APPEND PODS_DIRS ${_DIR_NAME})
  endif()
endforeach()


#------------------------------------------------------------------------------
# Install header files

# Initialize lists
set(PODS_HEADERS)
set(PODS_SOURCES)

foreach(DIR ${PODS_DIRS})
  # Each subdirectory defines HEADERS and SOURCES
  add_subdirectory(${DIR})
  set(HEADERS_FULL_PATH)
  foreach(HEADER_FILE ${HEADERS})
    list(APPEND HEADERS_FULL_PATH ${CMAKE_CURRENT_SOURCE_DIR}/${DIR}/${HEADER_FILE})
    #MESSAGE( STATUS "filepath :         " ${CMAKE_CURRENT_SOURCE_DIR}/${DIR}/${HEADER_FILE} )
  endforeach()
  install(FILES ${HEADERS_FULL_PATH} DESTINATION ${PODS_INCLUDE_DIR}/PODS/${DIR}
    COMPONENT Development)
  list(APPEND PODS_HEADERS ${HEADERS_FULL_PATH})
  foreach(SOURCE_FILE ${SOURCES})
    list(APPEND PODS_SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/${DIR}/${SOURCE_FILE})
    #MESSAGE( STATUS "filepath :         " ${CMAKE_CURRENT_SOURCE_DIR}/${DIR}/${SOURCE_FILE} )
  endforeach()
endforeach()

# Source files to build
add_library(PODS SHARED ${PODS_HEADERS} ${PODS_SOURCES} )

# Set compile flag
target_compile_features(PODS PRIVATE cxx_range_for)

# Link to DOLFIN library
target_link_libraries(PODS PUBLIC ${DOLFIN_LIBRARIES})

#------------------------------------------------------------------------------
# Boost
target_link_libraries(PODS PUBLIC Boost::boost)
foreach (BOOST_PACKAGE ${PODS_BOOST_COMPONENTS_PUBLIC})
  target_link_libraries(PODS PUBLIC "Boost::${BOOST_PACKAGE}")
endforeach()
foreach (BOOST_PACKAGE ${PODS_BOOST_COMPONENTS_PRIVATE})
  target_link_libraries(PODS PRIVATE "Boost::${BOOST_PACKAGE}")
endforeach()


#------------------------------------------------------------------------------
# Install it all

install(TARGETS PODS EXPORT PODSTargets
  RUNTIME DESTINATION ${PODS_LIB_DIR} COMPONENT RuntimeExecutables
  LIBRARY DESTINATION ${PODS_LIB_DIR} COMPONENT RuntimeLibraries
  ARCHIVE DESTINATION ${PODS_LIB_DIR} COMPONENT Development)

install(EXPORT PODSTargets
  DESTINATION ${PODS_SHARE_DIR}/cmake
  COMPONENT Development)
