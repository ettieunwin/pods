// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-03-29
// Last changed: 2017-03-29

#ifndef GENERICSOLVER_H
#define GENERICSOLVER_H

#include "../mesh/PodsMesh.h"
#include "../uncertainty/ProbabilitySpace.h"
#include <functional>

namespace pods
{
  // Structure for storing information about the solver
  struct solver_info
  {
    std::string problem;
    std::string random_field;
    std::vector<std::string> qoi;
    std::string mesh;
  };

  class PodsMesh;

  class GenericSolver
  /// This class is the generic pods solver class that every solver used
  /// with this code has to adhear to.  The constructor takes in the number
  /// of cells in the coarse level and information about the random field.
  {
  public:
    //struct pods_meshes;
    //Destructor
    ~GenericSolver()
    {
    }

    // Solves the problem with the random field data.
    virtual std::vector<double>
      pods_solve(PodsMesh& pods_mesh, double seed, bool fine_mesh) = 0;

    // Generates a shared pointer to a mesh of a given level that conforms
    // to the parameters set out in constructor and defined by the user.
    virtual void get_meshes(MPI_Comm comm, PodsMesh& pods_mesh,
			    int level) = 0;

    // Returns number of quantities of interest that is defined by the user.
    // If not pre-defined then there is only one.
    virtual int get_num_qoi()
    {
      return 1;
    }

    // Gets information about the solver that is pre determined by the user
    // for use in saving.
    virtual solver_info get_solver_info() = 0;

    // Sets number of cores per level.
    virtual std::vector<int> get_min_num_cores_per_level(int num_levels)
    {
        std::vector<int> min_cores_at_level;
	for (int i =0; i<num_levels; i++)
	  min_cores_at_level.push_back(1);

	return min_cores_at_level;
    }
  };
}
#endif
