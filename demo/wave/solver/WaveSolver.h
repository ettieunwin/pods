// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-03-31
// Last changed: 2017-03-31

#ifndef WAVESOLVER_H
#define WAVESOLVER_H

#include <pods.h>
#include <dolfin.h>

using namespace pods;

class PointSourceGenerator;

class WaveSolver: public GenericSolver
{
public:
  // Constructor
  WaveSolver(int n_cells_L0, int eig_start, int eig_stop,
	     int num_ps,
	     double mean, double scaling);

  // Destructor
  ~WaveSolver()
  {
  };

  // Solves the wave equation eigenvalue problem
  std::vector<double> pods_solve(PodsMesh& pods_mesh,
				 double seed,
				 bool fine_mesh) override;

  // Generates meshes
  void get_meshes(MPI_Comm comm, PodsMesh& pods_mesh,
		  int level) override;

  // Returns number of qois
  int get_num_qoi() override
  {
    return _eig_stop-_eig_start;
  }

  // Returns solver info
  solver_info get_solver_info() override;

private:

  // Mesh dimensions
  std::vector<double> _mesh_dimensions;

  // Generates initial mesh
  std::shared_ptr<dolfin::Mesh> _get_initial_mesh(MPI_Comm comm);

  // Number of cells in first level of mesh
  int _n_cells_L0;

  // First eigen value
  int _eig_start;

  // Last eigen value
  int _eig_stop;

  // Number of point sources
  int _num_ps;

  // Mean
  double _mean;

  // Scaling
  double _scaling;

};

#endif
