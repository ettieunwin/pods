// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-03-31
// Last changed: 2017-03-31

#include "StringVib.h"
#include "WaveSolver.h"
#include <pods.h>
#include <dolfin.h>
#include <math.h>

using namespace pods;
using namespace dolfin;
//-----------------------------------------------------------------------------
WaveSolver::WaveSolver(int n_cells_L0, int eig_start, int eig_stop,
		       int num_ps,
		       double mean, double scaling)
  : _n_cells_L0(n_cells_L0), _eig_start(eig_start), _eig_stop(eig_stop),
     _num_ps(num_ps), _mean(mean), _scaling(scaling)
{
  std::vector<double> mesh_dimensions = {1.0};
  _mesh_dimensions = mesh_dimensions;
  parameters["mesh_partitioner"] = "ParMETIS";
}
//-----------------------------------------------------------------------------
std::vector<double> WaveSolver::pods_solve(PodsMesh& pods_mesh,
					   double seed,
					   bool fine_mesh)
{
  // Chooses the correct mesh from pods_mesh depending
  std::shared_ptr<const dolfin::Mesh> mesh;
  if (fine_mesh == true)
    mesh = pods_mesh.get_fine_mesh();
  else
    mesh = pods_mesh.get_coarse_mesh();

  // Ensure mesh communicator is right
  MPI_Comm mesh_mpi_comm = mesh->mpi_comm();

  // Create function space
  auto V = std::make_shared<StringVib::FunctionSpace>(mesh);

  // Dummy rhs
  auto b_rhs = std::make_shared<PETScVector>(mesh_mpi_comm);
  StringVib::Form_L L(V);
  L.c = std::make_shared<Constant>(0.0);

  // Build stiffness matrix
  auto K = std::make_shared<PETScMatrix>(mesh_mpi_comm);
  StringVib::Form_k k(V, V);
  k.T = std::make_shared<Constant>(200.0);
  assemble_system(*K, *b_rhs, k, L, {});

  // Build mass matrix
  auto M = std::make_shared<PETScMatrix>(mesh_mpi_comm);
  StringVib::Form_m m(V, V);
  double rho_mu = _mean;
  m.rho = std::make_shared<Constant>(rho_mu);
  assemble_system(*M, *b_rhs, m, L, {});

  // Create a probability space
  ProbabilitySpace Omega = ProbabilitySpace("uniform", seed);
  pods_log(20, "created probability space");

  // Scaling size of random number to ensure isn't larger than matrix entry.
  double scaling_wave = _scaling*_mean*_mesh_dimensions[0];

  pods::PointSourceGenerator psg = pods::PointSourceGenerator(Omega,
							      mesh_mpi_comm,
							      _num_ps,
							      _mean,
							      scaling_wave,
							      _mesh_dimensions);
  std::vector<std::pair<dolfin::Point, double>> sources = psg.sample();

  // Convert random field information to be compatible with pointers
  // in FEniCS
  std::vector<std::pair<const Point*, double>> _sources;
  for (std::size_t i = 0; i < sources.size(); ++i)
    _sources.push_back({&(sources[i].first), sources[i].second});

  PointSource ps = PointSource(V, V, _sources);
  ps.apply(*M);

  const std::size_t nevs = _eig_stop;
  dolfin::SLEPcEigenSolver esolver(mesh_mpi_comm, K, M);

  esolver.set_options_prefix("wave_");
  PETScOptions::set("wave_st_ksp_type", "preonly");
  PETScOptions::set("wave_st_pc_type", "lu");
  PETScOptions::set("wave_st_pc_factor_mat_solver_package", "mumps");
  PETScOptions::set("wave_eps_gen_hermitian");
  PETScOptions::set("wave_eps_nev", nevs);
  PETScOptions::set("wave_eps_tol", 1e-8);
  PETScOptions::set("wave_eps_target", 1.0);
  PETScOptions::set("wave_st_type", "sinvert");
  PETScOptions::set("wave_eps_max_it", 100);
  PETScOptions::set("wave_st_pc_type", "cholesky");
  PETScOptions::set("wave_eps_smallest_real");

  esolver.solve(nevs);

  double r, c;
  PETScVector rx(mesh_mpi_comm);
  PETScVector cx(mesh_mpi_comm);
  std::vector<double> lam;
  for (std::size_t j=0; j<nevs; ++j)
  {
    esolver.get_eigenpair(r, c, rx, cx, j);
    lam.push_back(r);
  }

  std::vector<double> nf;
  std::string msg;
  int count = 0;
  for (int i = _eig_start; i < _eig_stop; ++i)
    {
      if (lam[i] < 0)
	nf.push_back(0);
      else
	nf.push_back(lam[i]);
      msg+= std::to_string(lam[i]) + " ";
      count += 1;
    }

  pods_log(50, msg);
  return nf;
}
//-----------------------------------------------------------------------------
std::shared_ptr<dolfin::Mesh> WaveSolver::_get_initial_mesh(MPI_Comm comm)
{
  return std::make_shared<dolfin::UnitIntervalMesh>(comm, _n_cells_L0);
}
//-----------------------------------------------------------------------------
// Generates mesh of a given level
void WaveSolver::get_meshes(MPI_Comm comm, PodsMesh& pods_mesh,
				  int level)
{
  // Creates the other meshes as necessary using dolfin::refine.
  PodsMesh::create_meshes(pods_mesh, _get_initial_mesh(comm),
			  comm, level);

  return;
}
//-----------------------------------------------------------------------------
solver_info WaveSolver::get_solver_info()
{
  solver_info info;
  info.problem = "wave";
  info.random_field = std::to_string(_num_ps)
    + " point sources / max magnitude " + std::to_string(_scaling) + "mass";

  std::vector<std::string> eigs;
  for (int q=_eig_start; q<_eig_stop; ++q)
    eigs.push_back(std::to_string(q));
  info.qoi = eigs;

  info.mesh = std::to_string(_n_cells_L0);

  return info;
}
//-----------------------------------------------------------------------------
