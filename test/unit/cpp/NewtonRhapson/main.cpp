// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// The logger is based on the FEniCS libraries logger, see
// <https://fenicsproject.org>.
//
// First added:  2017-04-06
// Last changed: 2017-04-06

#include <pods.h>
#include <dolfin.h>
#include <assert.h>
#include <cmath>
#include "kl.h"
#include "KL.cpp"
#include <fstream>
#include <sstream>

using namespace dolfin;
using namespace pods;

class Kernel : public Expression
{
 public:
  Kernel(double lambda): _lambda(lambda) {}
  
  void eval(Array<double>& values, const Array<double>& x) const
  {
    double x_i = 0.0;
    values[0] = exp(-std::abs(x_i - x[0])/_lambda);
  }

 private:
  double _lambda;
};

std::vector<double> calculate_As(std::vector<double> roots, double a)
{
  std::vector<double> As(roots.size());
  for (int i=0; i<roots.size(); i++)
    As[i] = 1.0/pow(a + sin(2.0*roots[i]*a)/(2.0*roots[i]), 0.5);
  return As;
}

std::vector<double> calculate_Bs(std::vector<double> roots, double a)
{
  std::vector<double> Bs(roots.size());
  for (int i=0; i<roots.size(); i++)
    Bs[i] = 1.0/pow(a - sin(2.0*roots[i]*a)/(2.0*roots[i]), 0.5);
  return Bs;
}

std::vector<double> calculate_eigs(std::vector<double> roots,
				   double lambda)
{
  std::vector<double> eigenvalues(roots.size());
  
  for (int i=0; i< roots.size(); i++)
  {
    eigenvalues[i] = (2.0/lambda)/(pow(roots[i], 2) + pow(lambda, -2));
  }
  return eigenvalues;
}

class EigenFunctionOdd : public Expression
{
 public:
  EigenFunctionOdd(double A, double root_odd)
    : _A(A), _root_odd(root_odd) {}

  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = _A*cos(_root_odd*x[0]);   
  }
  
 private:
  double _A, _root_odd;
};

class EigenFunctionEven : public Expression
{
 public:
  EigenFunctionEven(double B, double root_even)
    : _B(B), _root_even(root_even) {}

  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = _B*sin(_root_even*x[0]);   
  }
  
 private:
  double _B, _root_even;
};


int main()
{
  double lambda = 0.1;
  double a = 1.0;
  //investigate this...
  int n_cells = 200;
  int num_roots = 500;

  std::ifstream file("roots.txt");
  std::string str; 
  while (std::getline(file, str))
  {
    std::cout      // Process str
  }
    // // Even roots
  // std::vector<double> initial_guesses_even = {2.02, 4.9};
  // double period = M_PI;
  // std::function<double(double)> function_even = [nr_function = FunctionEven(a, lambda)](double x) mutable {return nr_function.function(x);};
  // std::function<double(double)> derivative_function_even = [nr_derivative_function = DerivativeFunctionEven(a, lambda)](double x) mutable {return nr_derivative_function.function(x);};
  // FunctionHolder fh_even = FunctionHolder(function_even, derivative_function_even);
  
  // NewtonRhapson nr_even = NewtonRhapson(fh_even, num_roots, initial_guesses_even,
  // 					period);
  // std::vector<double> even_roots = nr_even.get_roots();

  // // Calculate As and Bs
  // std::vector<double> As_odd = calculate_As(odd_roots, a);
  // std::vector<double> Bs_even = calculate_Bs(even_roots, a);
  
  // std::vector<double> eigenvalues_odd = calculate_eigs(odd_roots, lambda);
  // std::vector<double> eigenvalues_even = calculate_eigs(even_roots, lambda);

  // std::cout << "Odd roots: " << std::endl;
  // for (int i=0; i<odd_roots.size(); ++i)
  //   std::cout << eigenvalues_odd[i] << " " <<  odd_roots[i] << " " << As_odd[i] << std::endl;
  // std::cout << "Even roots: " << std::endl;
  // for (int i=0; i<even_roots.size(); ++i)
  //   std::cout << eigenvalues_even[i] << " " << even_roots[i] << " " << Bs_even[i] << std::endl;

  // std::shared_ptr<Mesh> mesh = std::make_shared<IntervalMesh>(n_cells, -a, a);
  // auto V = std::make_shared<kl::FunctionSpace>(mesh);

  // ProbabilitySpace ps = ProbabilitySpace("normal", 80);
  // std::mt19937 gen;
  // std::vector<double> stats = {0.0, 1.0};
  // std::function<double()> random_number_generator
  //   = ps.generate_random_number_generator(gen,stats);


  // std::vector<double> sample_numbers(num_roots*2);
  // std::generate(sample_numbers.begin(),
  // 		sample_numbers.end(),
  // 		random_number_generator);

  // Function eig_function_odd = Function(V);
  // Function eig_function_even = Function(V);
  // Function data = Function(V);
  
  // XDMFFile file("data.xdmf");
  // XDMFFile file2("data2.xdmf");
  // XDMFFile file3("data3.xdmf");
  // for (int j=0; j<odd_roots.size(); ++j)
  // {
  //   auto phi_odd = EigenFunctionOdd(As_odd[j], odd_roots[j]);
  //   eig_function_odd.interpolate(phi_odd);
  //   data.vector()->axpy(pow(eigenvalues_odd[j], 0.5)*sample_numbers[2*j],
  // 			*eig_function_odd.vector());
  //   file.write(data,2*j);
  //   file2.write(eig_function_odd,j);
    
  //   auto phi_even = EigenFunctionEven(Bs_even[j], even_roots[j]);
  //   eig_function_even.interpolate(phi_even);
  //   data.vector()->axpy(pow(eigenvalues_even[j], 0.5)*sample_numbers[2*j+1],
  // 			*eig_function_even.vector());
  //   file.write(data,2*j+1);
  //   file3.write(eig_function_even,j);
  // }



  // auto c = Kernel(lambda);
  // //auto coords = mesh->coordinates();
}
