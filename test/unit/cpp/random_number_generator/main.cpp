// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// The logger is based on the FEniCS libraries logger, see
// <https://fenicsproject.org>.
//
// First added:  2017-04-06
// Last changed: 2017-04-06

#include <pods.h>
#include <dolfin.h>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <memory>
#include <functional>
#include <random>
#include <numeric>
#include <algorithm>
#include <mpi.h>
#include <assert.h>
using namespace dolfin;
using namespace pods;

int main()
{
  MPI_Init(NULL, NULL);
  MPI_Comm comm = MPI_COMM_WORLD;

  int size;
  MPI_Comm_size(comm, &size);

  int rank;
  MPI_Comm_rank(comm, &rank);

  // Generate random number generator
  std::vector<double> seeds;
  for(int i=1; i<size+1; ++i)
    seeds.push_back(i);
  std::vector<double> stats = {-1, 1};

  std::mt19937 gen;
  RandomNumberGenerator rng = RandomNumberGenerator(seeds[rank], stats);

  std::function<double()> ung = rng.uniform_distribution(gen);

  std::vector<double> rank0 = {0.99437, 0.865115, -0.743751, 0.998081,
                               -0.527822, -0.206839, -0.224179, 0.339492,
                               0.871078, 0.692622};
  std::vector<double> rank1 = {-0.629836, 0.863082, 0.895461, -0.0305018,
                               -0.358927, -0.691147, 0.397725, -0.760099,
                               -0.0296482, 0.265475};

  for (int i=0; i<rank0.size(); ++i)
  {
    double number = ung();
    if (rank == 0)
    {
      //std::cout << number << std::endl;
      assert(round(number - rank0[i]) == 0
             && "Wrong random number on process 0");
    }
    if (rank == 1)
    {
      //std::cout << number << std::endl;
      assert(round(number - rank1[i]) == 0
             && "Wrong random number on process 1");
    }
  }
  MPI_Finalize();
}
