# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.5

# compile CXX with /usr/bin/c++
CXX_FLAGS = -Wno-comment     -fopenmp -g   -std=c++11

CXX_DEFINES = -DDEBUG -DDOLFIN_LA_INDEX_SIZE=4 -DDOLFIN_SIZE_T=8 -DDOLFIN_VERSION=\"2017.1.0.dev0\" -DHAS_CHOLMOD -DHAS_HDF5 -DHAS_MPI -DHAS_OPENMP -DHAS_PARMETIS -DHAS_PETSC -DHAS_SCOTCH -DHAS_SLEPC -DHAS_UMFPACK -DHAS_ZLIB -D_FORTIFY_SOURCE=2 -D_LARGEFILE64_SOURCE -D_LARGEFILE_SOURCE

CXX_INCLUDES = -I/home/hjtu2/Documents/fenics/dolfin-coding-days/local.ettie.add_matrix/include -isystem /home/hjtu2/local/fenics/lib/python2.7/site-packages/FFC-2017.1.0.dev0-py2.7.egg/ffc/backends/ufc -isystem /home/hjtu2/local/packages/petsc-dev/include -isystem /usr/lib/openmpi/include/openmpi/opal/mca/event/libevent2021/libevent -isystem /usr/lib/openmpi/include/openmpi/opal/mca/event/libevent2021/libevent/include -isystem /usr/lib/openmpi/include -isystem /usr/lib/openmpi/include/openmpi -isystem /usr/include/eigen3 -isystem /usr/include/hdf5/openmpi -I/home/hjtu2/local/include -isystem /home/hjtu2/local/packages/slepc-dev/include 

