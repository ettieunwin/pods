// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// The logger is based on the FEniCS libraries logger, see
// <https://fenicsproject.org>.
//
// First added:  2017-04-06
// Last changed: 2017-04-06

#include <pods.h>
#include <dolfin.h>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <mpi.h>
#include <memory>

using namespace dolfin;
using namespace pods;

int main()
{
  MPI_Init(NULL, NULL);
  MPI_Comm comm = MPI_COMM_WORLD;

  int num_ps = 10;
  double mean = 1000;
  double scaling = 0.1;
  std::vector<double> max_mesh_dims = {1};

  int size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  // Random magnitudes to check the random number generator.
  std::vector<double> random_mags = {86.5115, 99.8081, -20.6839, 33.9492,
                                     69.2622, 4.90963, -54.0846, 82.7924,
                                     -13.8603, 55.6778};

  // Generate a seeded random number generator on each processor
  std::vector<double> seeds;
  for(int i=1; i<size+1; ++i)
    seeds.push_back(i);

  std::vector<double> stats = {-1, 1};

  RandomNumberGenerator rng= RandomNumberGenerator(MPI_COMM_WORLD,
						   seeds, stats);
  std::mt19937 gen;
  std::function<double()> ung = rng.uniform_distribution(gen);

  std::vector<std::pair<Point, double>> sources;
  sources = PointSourceGenerator::point_source_info(comm, ung, num_ps,
						    mean, scaling,
						    max_mesh_dims);

  // Checking that number of point sources on each process is as expected.
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  
  if (rank == 0)
    assert(num_ps == sources.size()
	   && "Wrong number of point sources on rank 0 process");
  else
    assert(0 == sources.size()
	   && "Wrong number of point sources on non rank 0 processes");


  // Checking that random number generation is correct on rank 0.
  if (rank == 0)
  {
    for (int i=0; i<num_ps ; ++i)
    {
      std::pair<Point, double> source = sources[i];

      Point point = std::get<0>(source);
      double magnitude = std::get<1>(source);

      // Checks magnitude of points is what expected so random number
      // generator is working.
      assert (round(magnitude-random_mags[i])==0  && "magnitude is wrong");
    }
  }

  MPI_Finalize();
}
