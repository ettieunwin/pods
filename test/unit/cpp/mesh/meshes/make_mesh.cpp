#include <dolfin.h>
#include <pods.h>
#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>

using namespace dolfin;
using namespace pods;

int main()
{
  std::string save_path = "../meshes";
  boost::filesystem::path dir(save_path);
  boost::filesystem::create_directory(dir);

  for (int i=0; i<2; ++i)
  {
    // Set variables for MC/MLMC
    int n_cells = 10*pow(2,i);
    std::cout << "Number of cells in each direction:" << n_cells << std::endl;
       
    Mesh mesh = UnitSquareMesh(n_cells, n_cells);
    
    XDMFFile xdmf(save_path+ "/mesh"+ std::to_string(i)+".xdmf");
    xdmf.write(mesh);
  }
}
