// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// The logger is based on the FEniCS libraries logger, see
// <https://fenicsproject.org>.
//
// First added:  2017-04-06
// Last changed: 2017-04-06

#include <pods.h>
#include <dolfin.h>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <mpi.h>
#include <memory>

using namespace dolfin;
using namespace pods;

int main()
{
  //----------------------------------------------------------------------------
  // MeshGenerator test.
  // Should also add a test that checks the error on mesh if level 0 and
  // generate coarse mesh.
  // Test that checks the mesh refine function is working.  Runs tests
  // for interval, square and box mesh.
  MPI_Init(NULL, NULL);
  MPI_Comm comm = MPI_COMM_WORLD;

  int size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  int n = 10;

  // Generate mesh hierarchy
  std::shared_ptr<const Mesh> mesh
    = std::make_shared<UnitSquareMesh>(n, n);

  for(int level=0; level<4; ++level)
  {
    MeshGenerator mg = MeshGenerator(mesh, level);
    if (level > 0)
    {
      std::shared_ptr<const Mesh> mesh_coarse = mg.get_coarse_mesh();
      int sum_coarse;
      int total_coarse=mesh_coarse->num_cells();
      MPI_Allreduce(&total_coarse, &sum_coarse, 1, MPI_INT, MPI_SUM,
                    MPI_COMM_WORLD);
      assert(sum_coarse == pow(2, ((level-1)*2)+1)*n*n);
    }

    std::shared_ptr<const Mesh> mesh_fine = mg.get_fine_mesh();
    int sum_fine;
    int total_fine=mesh_fine->num_cells();
    MPI_Allreduce(&total_fine, &sum_fine, 1, MPI_INT, MPI_SUM,
		  MPI_COMM_WORLD);
    assert(sum_fine == pow(2, (level*2)+1)*n*n);
  }

  mesh = std::make_shared<UnitIntervalMesh>(n);

  for(int level=0; level<4; ++level)
  {
    MeshGenerator mg = MeshGenerator(mesh, level);
    if (level > 0)
    {
      std::shared_ptr<const Mesh> mesh_coarse = mg.get_coarse_mesh();
      int sum_coarse;
      int total_coarse=mesh_coarse->num_cells();
      MPI_Allreduce(&total_coarse, &sum_coarse, 1, MPI_INT, MPI_SUM,
                    MPI_COMM_WORLD);
      assert(sum_coarse == pow(2, (level-1))*n);
    }

    std::shared_ptr<const Mesh> mesh_fine = mg.get_fine_mesh();
    int sum_fine;
    int total_fine=mesh_fine->num_cells();
    MPI_Allreduce(&total_fine, &sum_fine, 1, MPI_INT, MPI_SUM,
		  MPI_COMM_WORLD);
    assert(sum_fine == pow(2, level)*n);
  }

  mesh = std::make_shared<UnitCubeMesh>(n, n, n);
  for(int level=0; level<4; ++level)
  {
    MeshGenerator mg = MeshGenerator(mesh, level);
    if (level > 0)
    {
      std::shared_ptr<const Mesh> mesh_coarse = mg.get_coarse_mesh();
      int sum_coarse;
      int total_coarse=mesh_coarse->num_cells();
      MPI_Allreduce(&total_coarse, &sum_coarse, 1, MPI_INT, MPI_SUM,
                    MPI_COMM_WORLD);
      assert(sum_coarse == 6*n*n*n*pow(2, (level-1)*3));
    }

    std::shared_ptr<const Mesh> mesh_fine = mg.get_fine_mesh();
    int sum_fine;
    int total_fine=mesh_fine->num_cells();
    MPI_Allreduce(&total_fine, &sum_fine, 1, MPI_INT, MPI_SUM,
		  MPI_COMM_WORLD);
    assert(sum_fine == 6*n*n*n*pow(2, level*3));
  }
  
  std::cout << "*****MeshGenerator tests passed*****" << std::endl;
  //---------------------------------------------------------------------------
  // PodsMesh tests
  PodsMesh pods_mesh;
  std::shared_ptr<Mesh> mesh_pm = std::make_shared<Mesh>(UnitSquareMesh(10,10));

  int num_cells;
 
  pods_mesh.set_initial_mesh(mesh_pm);
  int mesh_cells = static_cast<int>(pods_mesh.get_initial_mesh()->num_cells());
  MPI_Allreduce(&mesh_cells, &num_cells, 1,
		MPI_INT, MPI_SUM, MPI_COMM_WORLD); 
  assert(num_cells==200);
  
  pods_mesh.set_fine_mesh(mesh_pm);
  mesh_cells = static_cast<int>(pods_mesh.get_fine_mesh()->num_cells());
  MPI_Allreduce(&mesh_cells, &num_cells, 1,
		MPI_INT, MPI_SUM, MPI_COMM_WORLD); 
  assert(num_cells==200);
  
  pods_mesh.set_coarse_mesh(mesh_pm);
  mesh_cells = static_cast<int>(pods_mesh.get_coarse_mesh()->num_cells());
  MPI_Allreduce(&mesh_cells, &num_cells, 1,
		MPI_INT, MPI_SUM, MPI_COMM_WORLD); 
  assert(num_cells==200);
  
  std::cout << "*****PodsMesh tests passed*****" << std::endl;
  //----------------------------------------------------------------------------
  // CreateMeshes test
  PodsMesh pods_mesh_cm;
  int cells;

  std::shared_ptr<Mesh>
    initial_mesh = std::make_shared<Mesh>(UnitSquareMesh(10,10));

  // Test for level=0
  PodsMesh::CreateMeshes(pods_mesh_cm, initial_mesh, MPI_COMM_WORLD, 0);
  cells = static_cast<int>(pods_mesh_cm.get_coarse_mesh()->num_cells());
  MPI_Allreduce(&cells, &num_cells, 1,
		MPI_INT, MPI_SUM, MPI_COMM_WORLD); 
  assert(num_cells==200);
  cells = static_cast<int>(pods_mesh_cm.get_fine_mesh()->num_cells());
  MPI_Allreduce(&cells, &num_cells, 1,
		MPI_INT, MPI_SUM, MPI_COMM_WORLD); 
  assert(num_cells==200);

  // Test for level>0
  PodsMesh::CreateMeshes(pods_mesh_cm, initial_mesh, MPI_COMM_WORLD, 2);
  cells = static_cast<int>(pods_mesh_cm.get_coarse_mesh()->num_cells());
  MPI_Allreduce(&cells, &num_cells, 1,
		MPI_INT, MPI_SUM, MPI_COMM_WORLD); 
  assert(num_cells==800);
  cells = static_cast<int>(pods_mesh_cm.get_fine_mesh()->num_cells());
  MPI_Allreduce(&cells, &num_cells, 1,
		MPI_INT, MPI_SUM, MPI_COMM_WORLD); 
  assert(num_cells==3200);
  
  std::cout << "*****PodsMesh::CreateMesh tests passed*****" << std::endl;
  //----------------------------------------------------------------------------
  // ReadMeshes test

  std::vector<std::string> meshes = {"../meshes/meshes/mesh0.xdmf",
				     "../meshes/meshes/mesh1.xdmf"};
  PodsMesh pods_mesh_rm;
  
  // Test for level 0
  PodsMesh::ReadMeshes(pods_mesh_rm, meshes, MPI_COMM_WORLD, 0);
  cells = static_cast<int>(pods_mesh_rm.get_initial_mesh()->num_cells());
  MPI_Allreduce(&cells, &num_cells, 1,
		MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  assert(num_cells==200);
  cells = static_cast<int>(pods_mesh_rm.get_coarse_mesh()->num_cells());
  MPI_Allreduce(&cells, &num_cells, 1,
		MPI_INT, MPI_SUM, MPI_COMM_WORLD); 
  assert(num_cells==200);
  cells = static_cast<int>(pods_mesh_rm.get_fine_mesh()->num_cells());
  MPI_Allreduce(&cells, &num_cells, 1,
		MPI_INT, MPI_SUM, MPI_COMM_WORLD); 
  assert(num_cells==200);

  // Test for level 1 (mesh provided)
  PodsMesh::ReadMeshes(pods_mesh_rm, meshes, MPI_COMM_WORLD, 1);
  cells = static_cast<int>(pods_mesh_rm.get_initial_mesh()->num_cells());
  MPI_Allreduce(&cells, &num_cells, 1,
		MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  assert(num_cells==200);
  cells = static_cast<int>(pods_mesh_rm.get_coarse_mesh()->num_cells());
  MPI_Allreduce(&cells, &num_cells, 1,
		MPI_INT, MPI_SUM, MPI_COMM_WORLD); 
  assert(num_cells==200); 
  cells = static_cast<int>(pods_mesh_rm.get_fine_mesh()->num_cells());
  MPI_Allreduce(&cells, &num_cells, 1,
		MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  assert(num_cells==800);

  // Test for level 2 (mesh not provided)
  PodsMesh::ReadMeshes(pods_mesh_rm, meshes, MPI_COMM_WORLD, 2);
  cells = static_cast<int>(pods_mesh_rm.get_initial_mesh()->num_cells());
  MPI_Allreduce(&cells, &num_cells, 1,
		MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  assert(num_cells==200);
  cells = static_cast<int>(pods_mesh_rm.get_coarse_mesh()->num_cells());
  MPI_Allreduce(&cells, &num_cells, 1,
		MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  assert(num_cells==800);
  cells = static_cast<int>(pods_mesh_rm.get_fine_mesh()->num_cells());
  MPI_Allreduce(&cells, &num_cells, 1,
		MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  assert(num_cells==3200);

  std::cout << "*****PodsMesh::ReadMesh tests passed*****" << std::endl;
  //---------------------------------------------------------------------------
  MPI_Finalize();
}
