#podS

podS is a uncertainty quantification library for Monte Carlo and Multi-level Monte Carlo approximations of stochastic partial differential equations. 
podS is designed to automatically schedule and run batch jobs seeking optimal work distribution across parallel processes. podS is dependent on the FEniCS
libraries (https://fenicsproject.org/). 

#Installation
1) Install the FEniCS libraries according to their installation procedure found here: https://fenicsproject.org/download/

2) Clone this repository

3) Install pods

```
ffc -l dolfin pods/uncertainty/Matern_1d.ufl
ffc -l dolfin pods/uncertainty/Matern_2d.ufl
ffc -l dolfin pods/uncertainty/Matern_3d.ufl
ffc -l dolfin pods/uncertainty/kl1D.ufl
ffc -l dolfin pods/uncertainty/kl2D.ufl
ffc -l dolfin pods/uncertainty/kl3D.ufl
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$HOME/local ..
make 
make install

```
or use the install file
```
bash install_pods.sh
```

#Docker container
A docker container with FEniCS and podS pre-installed is available at https://quay.io/pods/pods

Example usage
```
docker run -it quay.io/pods/pods
```

#Example usage
The UQ problem is specified as a class adhering to GenericSolver.h. Examples are given in the ```pod_stochastic/demo``` folder.

#Contributors
* H. Juliette. T. Unwin <hjtu2@cam.ac.uk>
* Hugo Hadfield <hh409@cam.ac.uk>
* Nate J. C. Sime <njcs4@cam.ac.uk>


#License
GNU LGPL, version 3.